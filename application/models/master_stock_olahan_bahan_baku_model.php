<?php
class master_stock_olahan_bahan_baku_model extends Core_master_model{	    	
	public $table_name = "master_stock_olahan_bahan_baku";
	var $default_value_list = Array(
		'id_bahan_baku' => NULL ,
		'jumlah' => 0 ,
		'import_flag' => '' 
	);
	var $value_list  = array();
	var $array_condition  = array();
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }
	
	
	
	function saldoAwal($bahan_baku, $gudang = 0){								
		$sql = "SELECT jumlah FROM ".$this->table_name." WHERE id_bahan_baku = ".$bahan_baku." ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == NULL){
			return NULL;
		} else {
			return $row['jumlah'];
		}
	}

}

?>