<?php
class Master_user_model extends Core_master_model{	    	
	public $table_name = "master_user";
	var $default_value_list = Array(
		'id' => NULL ,
		'user_name' => '' ,
		'password' => '' ,
		'role' => NULL ,
		'ip' => '' ,
		'token_cookie' => '' ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'user_status' => STATUS_ACTIVE,
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'id' ,
		1 => 'first_name' ,
		2 => 'last_name' ,
		3 => 'phone_number' ,
		4 => 'email' ,
		5 => 'city' 
	);
	var $fillable_value_list = array(
		'id',
		'first_name',
		'last_name',
		'phone_number',
		'email',
		'city',
	);

	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

}

?>