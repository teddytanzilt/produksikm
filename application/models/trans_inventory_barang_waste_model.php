<?php
class Trans_inventory_barang_waste_model extends Core_master_model{	    	
	public $table_name = "trans_inventory_barang_waste";
	var $default_value_list = Array(
		'id_barang_waste' => NULL ,
		'id_gudang' => NULL ,
		'id_user' => NULL ,
		'trans_date' => NULL ,
		'comment' => '' ,
		'saldo_awal' => 0 ,
		'jumlah_masuk' => 0 ,
		'jumlah_keluar' => 0 ,
		'saldo_akhir' => 0 ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE
	);
	var $value_list  = array();
	var $array_condition  = array();
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

    function getQuerySaldo($barang)
	{
		$query = "SELECT ".
				"s.id, s.id_barang_waste, s.id_gudang, s.id_user, ".
				"s.comment, ".
				"s.trans_date, s.jumlah_masuk, s.jumlah_keluar, ".
				"@b AS saldo_awal, ".
				"@b := @b + s.jumlah_masuk - s.jumlah_keluar AS saldo_akhir ".
				"FROM ".
				"(SELECT @b := 0.0) AS dummy ".
				"CROSS JOIN ".
				"trans_inventory_barang_waste AS s ".
				"WHERE s.id_barang_waste = ".$barang." ".
				"ORDER BY trans_date ";
		return $query;
	}
	
	function getMutasi($start_date, $end_date){
		$sql = "SELECT ".
				"master_barang_waste.kode AS kode_barang, master_barang_waste.nama AS nama_barang, ".
				"master_satuan.kode AS kode_satuan, master_gudang.kode AS kode_gudang, ".
				"sum_total.id_barang_waste, sum_total.id_gudang, sum_total.total_masuk, sum_total.total_keluar, ".
				"sum_awal.saldo_awal, sum_akhir.saldo_akhir ".
				"FROM ".
				"( ".
					"SELECT id_barang_waste, id_gudang, SUM(jumlah_masuk) AS total_masuk, SUM(jumlah_keluar) AS total_keluar, MIN(trans_date) AS min_date, MAX(trans_date) AS max_date  ".
					"FROM trans_inventory_barang_waste ".
					"WHERE trans_date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' ".
					"GROUP BY id_barang_waste, id_gudang ".
				") sum_total ".
				"LEFT JOIN master_barang_waste ON (sum_total.id_barang_waste = master_barang_waste.id) ".
				"LEFT JOIN master_satuan ON (master_barang_waste.satuan = master_satuan.id) ".
				"LEFT JOIN master_gudang ON (sum_total.id_gudang = master_gudang.id) ".
				"LEFT JOIN trans_inventory_barang_waste sum_awal ON (sum_awal.id_barang_waste = sum_total.id_barang_waste AND sum_awal.id_gudang = sum_total.id_gudang AND sum_awal.trans_date = sum_total.min_date) ".
				"LEFT JOIN trans_inventory_barang_waste sum_akhir ON (sum_akhir.id_barang_waste = sum_total.id_barang_waste AND sum_akhir.id_gudang = sum_total.id_gudang AND sum_akhir.trans_date = sum_total.max_date) ".
				"WHERE sum_awal.trans_date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' ".
				"AND sum_akhir.trans_date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
	
	function getMutasiDetail($start_date, $end_date){
		$sql = "SELECT ".
				"master_barang_waste.kode AS kode_barang, master_barang_waste.nama AS nama_barang, master_gudang.kode AS kode_gudang, ".
				"trans_inventory_barang_waste.comment, trans_inventory_barang_waste.trans_date, ".
				"trans_inventory_barang_waste.saldo_awal, trans_inventory_barang_waste.saldo_akhir, ".
				"trans_inventory_barang_waste.jumlah_masuk, trans_inventory_barang_waste.jumlah_keluar ".
				"FROM trans_inventory_barang_waste  ".
				"LEFT JOIN master_barang_waste ON (trans_inventory_barang_waste.id_barang_waste = master_barang_waste.id) ".
				"LEFT JOIN master_gudang ON (trans_inventory_barang_waste.id_gudang = master_gudang.id) ".
				"WHERE trans_inventory_barang_waste.trans_date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
	
	function getJumlah($sum_field, $start_date, $end_date){
		$sql = "SELECT SUM(".$sum_field.") AS field_name ".
				"FROM ".$this->table_name." ".
				"WHERE trans_date BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}

	function getCountReffId($reff_id , $type = 1){
		if($type == 1) {
			$sql = "SELECT COUNT(*) AS field_name ".
				"FROM ".$this->table_name." ".
				"WHERE reff_id = '".$reff_id."' AND (comment LIKE 'I%' OR comment LIKE '-I%') ";
		} else if($type == 2) {
			$sql = "SELECT COUNT(*) AS field_name ".
				"FROM ".$this->table_name." ".
				"WHERE reff_id = '".$reff_id."' AND (comment LIKE 'O%' OR comment LIKE '-O%') ";
		}
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}

	function cancelReffId($reff_id , $type = 1){
		if($type == 1) {
			$sql = "UPDATE ".$this->table_name." SET trans_status = '".STATUS_CANCEL."' ".
				"WHERE reff_id = '".$reff_id."' AND (comment LIKE 'I%' OR comment LIKE '-I%') ";
		} else if($type == 2) {
			$sql = "UPDATE ".$this->table_name." SET trans_status = '".STATUS_CANCEL."' ".
				"WHERE reff_id = '".$reff_id."' AND (comment LIKE 'O%' OR comment LIKE '-O%') ";
		}
		$query = $this->db->query($sql);
		return $query;
	}

	/*belong to ancient method*/
	function getSaldoPerTanggal($tanggal, $bahan_baku){
		$sql = "SELECT saldo_akhir ".
				"FROM ".$this->table_name." ".
				"WHERE trans_date < '".$tanggal."' AND id_bahan_baku = '".$bahan_baku."' ".
				"ORDER BY trans_date DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['saldo_akhir'];
		}
	}

	/*belong to ancient method*/
	function updateSaldoPerTanggal($jumlah, $tanggal, $exclude_id, $bahan_baku){
		$sql = "UPDATE ".$this->table_name." SET saldo_awal = (saldo_awal+".intval($jumlah)."), saldo_akhir = (saldo_akhir+".intval($jumlah).") ".
				"WHERE trans_date >= '".$tanggal."' AND id_bahan_baku = '".$bahan_baku."' AND id != ".$exclude_id." ";
		$query = $this->db->query($sql);
		return $query;
	}
}

?>