<?php
class Trans_pemasukan_bahan_baku_model extends Core_master_model{	    	
	public $table_name = "trans_pemasukan_bahan_baku";
	var $default_value_list = Array(
		'id' => NULL ,
		'no_transaksi' => '' ,
		'tanggal' => NULL ,
		'jenis_dokumen' => '' ,
		'no_dokumen_pabean' => '' ,
		'tanggal_dokumen_pabean' => '' ,
		'no_seri_barang' => '' ,
		'no_bukti_penerimaan_barang' => '' ,
		'tanggal_bukti_penerimaan_barang' => '',
		'bahan_baku' => NULL ,
		'jumlah' => 0 ,
		'mata_uang' => NULL ,
		'nilai_barang' => '' ,
		'gudang' => NULL ,
		'negara_asal_barang' => NULL ,
		'supplier' => NULL ,
		'harga_satuan' => '',
		'no_pib' => '',
		'tanggal_pib' => NULL,
		'negara_asal_pib' => NULL,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'trans_status' => STATUS_PENDING
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'trans_pemasukan_bahan_baku.id' ,
		1 => 'trans_pemasukan_bahan_baku.trans_status',
		2 => 'trans_pemasukan_bahan_baku.no_transaksi' ,
		3 => 'trans_pemasukan_bahan_baku.tanggal' ,
		4 => 'master_bahan_baku.kode AS kode_bahan_baku' ,
		5 => 'master_gudang.kode AS kode_gudang' ,
		6 => 'trans_pemasukan_bahan_baku.nilai_barang' 
		
	);
	var $datatable_show_value_list = Array(
		0 => 'id' ,
		1 => 'trans_status',
		2 => 'no_transaksi' ,
		3 => 'tanggal' ,
		4 => 'kode_bahan_baku' ,
		5 => 'kode_gudang' ,
		6 => 'nilai_barang'
		
	);
	var $datatable_search_value_list = Array(
		0 => 'trans_pemasukan_bahan_baku.id' ,
		1 => 'trans_pemasukan_bahan_baku.trans_status',
		2 => 'trans_pemasukan_bahan_baku.no_transaksi' ,
		3 => 'trans_pemasukan_bahan_baku.tanggal' ,
		4 => 'master_bahan_baku.kode' ,
		5 => 'master_gudang.kode' ,
		6 => 'trans_pemasukan_bahan_baku.nilai_barang'
		
	);
	var $fillable_value_list = array(
		'jenis_dokumen',
		'no_dokumen_pabean',
		'tanggal_dokumen_pabean',
		'no_seri_barang',
		'no_bukti_penerimaan_barang',
		'tanggal_bukti_penerimaan_barang',
		'bahan_baku',
		'jumlah',
		'mata_uang',
		'nilai_barang',
		'gudang',
		'negara_asal_barang',
		'supplier',
		'harga_satuan',
		'no_pib',
		'tanggal_pib',
		'negara_asal_pib'
	);
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

	function getDataTableCountActive($datatable_request){								
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM ".$this->table_name." ".
			"LEFT JOIN master_bahan_baku ON (master_bahan_baku.id = ".$this->table_name.".bahan_baku) ".
			"LEFT JOIN master_gudang ON (master_gudang.id = ".$this->table_name.".gudang) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." ".
			"LEFT JOIN master_bahan_baku ON (master_bahan_baku.id = ".$this->table_name.".bahan_baku) ".
			"LEFT JOIN master_gudang ON (master_gudang.id = ".$this->table_name.".gudang) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
	
	function getDailyReport($start_date, $end_date){
		$sql = "SELECT ".
				"trans_pemasukan_bahan_baku.jenis_dokumen, ".
				"trans_pemasukan_bahan_baku.no_dokumen_pabean, ".
				"trans_pemasukan_bahan_baku.tanggal_dokumen_pabean, ".
				"trans_pemasukan_bahan_baku.no_seri_barang, ".
				"trans_pemasukan_bahan_baku.no_bukti_penerimaan_barang, ".
				"trans_pemasukan_bahan_baku.tanggal_bukti_penerimaan_barang, ".
				"master_bahan_baku.kode AS kode_barang, ".
				"master_bahan_baku.nama AS nama_barang, ".
				"master_satuan.kode AS kode_satuan, ".
				"trans_pemasukan_bahan_baku.jumlah, ".
				"master_mata_uang.kode AS kode_mata_uang, ".
				"trans_pemasukan_bahan_baku.nilai_barang, ".
				"master_gudang.kode AS kode_gudang, ".
				"master_negara.kode AS kode_negara ".
				"FROM ".
				"trans_pemasukan_bahan_baku ".
				"LEFT JOIN master_bahan_baku ON (trans_pemasukan_bahan_baku.bahan_baku = master_bahan_baku.id)  ".
				"LEFT JOIN master_satuan ON (master_bahan_baku.satuan = master_satuan.id)  ".
				"LEFT JOIN master_mata_uang ON (trans_pemasukan_bahan_baku.mata_uang = master_mata_uang.id)  ".
				"LEFT JOIN master_gudang ON (trans_pemasukan_bahan_baku.gudang = master_gudang.id)  ".
				"LEFT JOIN master_negara ON (trans_pemasukan_bahan_baku.negara_asal_barang = master_negara.id)  ".
				"WHERE trans_pemasukan_bahan_baku.tanggal BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' AND trans_pemasukan_bahan_baku.record_status = '".STATUS_ACTIVE."'";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
}

?>