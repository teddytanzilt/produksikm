<?php
class Trans_pengeluaran_bahan_baku_model extends Core_master_model{	    	
	public $table_name = "trans_pengeluaran_bahan_baku";
	var $default_value_list = Array(
		'id' => NULL ,
		'no_transaksi' => '' ,
		'tanggal' => NULL ,
		'no_bukti_pengeluaran' => '' ,
		'tanggal_bukti_pengeluaran' => '' ,
		'bahan_baku' => NULL ,
		'jumlah' => 0 ,
		'gudang' => NULL ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'trans_status' => STATUS_PENDING
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'trans_pengeluaran_bahan_baku.id' ,
		1 => 'trans_pengeluaran_bahan_baku.trans_status',
		2 => 'trans_pengeluaran_bahan_baku.no_transaksi' ,
		3 => 'trans_pengeluaran_bahan_baku.tanggal' ,
		4 => 'master_bahan_baku.kode AS kode_bahan_baku' ,
		5 => 'master_gudang.kode AS kode_gudang' ,
		6 => 'trans_pengeluaran_bahan_baku.jumlah' ,
	);
	var $datatable_show_value_list = Array(
		0 => 'id' ,
		1 => 'trans_status',
		2 => 'no_transaksi' ,
		3 => 'tanggal' ,
		4 => 'kode_bahan_baku' ,
		5 => 'kode_gudang' ,
		6 => 'jumlah' 
	);
	var $datatable_search_value_list = Array(
		0 => 'trans_pengeluaran_bahan_baku.id' ,
		1 => 'trans_pengeluaran_bahan_baku.trans_status',
		2 => 'trans_pengeluaran_bahan_baku.no_transaksi' ,
		3 => 'trans_pengeluaran_bahan_baku.tanggal' ,
		4 => 'master_bahan_baku.kode' ,
		5 => 'master_gudang.kode' ,
		6 => 'trans_pengeluaran_bahan_baku.jumlah'
	);
	var $fillable_value_list = array(
		'no_bukti_pengeluaran',
		'tanggal_bukti_pengeluaran',
		'bahan_baku',
		'jumlah',
		'gudang',
	);
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

	function getDataTableCountActive($datatable_request){								
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM ".$this->table_name." ".
			"LEFT JOIN master_bahan_baku ON (master_bahan_baku.id = ".$this->table_name.".bahan_baku) ".
			"LEFT JOIN master_gudang ON (master_gudang.id = ".$this->table_name.".gudang) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." ".
			"LEFT JOIN master_bahan_baku ON (master_bahan_baku.id = ".$this->table_name.".bahan_baku) ".
			"LEFT JOIN master_gudang ON (master_gudang.id = ".$this->table_name.".gudang) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
	
	function getDailyReport($start_date, $end_date){
		$sql = "SELECT ".
				"trans_pengeluaran_bahan_baku.no_bukti_pengeluaran, ".
				"trans_pengeluaran_bahan_baku.tanggal_bukti_pengeluaran, ".
				"master_bahan_baku.kode AS kode_barang, ".
				"master_bahan_baku.nama AS nama_barang, ".
				"master_satuan.kode AS kode_satuan, ".
				"trans_pengeluaran_bahan_baku.jumlah, ".
				"master_gudang.kode AS kode_gudang ".
				"FROM ".
				"trans_pengeluaran_bahan_baku ".
				"LEFT JOIN master_bahan_baku ON (trans_pengeluaran_bahan_baku.bahan_baku = master_bahan_baku.id)  ".
				"LEFT JOIN master_satuan ON (master_bahan_baku.satuan = master_satuan.id)  ".
				"LEFT JOIN master_gudang ON (trans_pengeluaran_bahan_baku.gudang = master_gudang.id)  ".
				"WHERE trans_pengeluaran_bahan_baku.tanggal BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' AND trans_pengeluaran_bahan_baku.record_status = '".STATUS_ACTIVE."'";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
}

?>