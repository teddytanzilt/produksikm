<?php
class master_stock_barang_jadi_model extends Core_master_model{	    	
	public $table_name = "master_stock_barang_jadi";
	var $default_value_list = Array(
		'id_barang_jadi' => NULL ,
		'id_gudang' => NULL ,
		'jumlah' => 0 ,
		'import_flag' => '' 
	);
	var $value_list  = array();
	var $array_condition  = array();
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }
	
	
	
	function saldoAwal($barang_jadi, $gudang){								
		$sql = "SELECT jumlah FROM ".$this->table_name." WHERE id_barang_jadi = ".$barang_jadi." AND id_gudang = ".$gudang." ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == NULL){
			return NULL;
		} else {
			return $row['jumlah'];
		}
	}

}

?>