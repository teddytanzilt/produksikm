<?php
class Master_negara_model extends Core_master_model{	    	
	public $table_name = "master_negara";
	var $default_value_list = Array(
		'id' => NULL ,
		'kode' => '' ,
		'nama' => '' ,
		'import_flag' => '' ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'id' ,
		1 => 'kode' ,
		2 => 'nama' 
	);
	var $fillable_value_list = array(
		'kode',
		'nama'
	);

	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

}

?>