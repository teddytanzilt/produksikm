<?php
class Trans_pengeluaran_barang_jadi_model extends Core_master_model{	    	
	public $table_name = "trans_pengeluaran_barang_jadi";
	var $default_value_list = Array(
		'id' => NULL ,
		'no_transaksi' => '' ,
		'tanggal' => NULL ,
		'no_feb' => '' ,
		'tanggal_feb' => NULL ,
		'no_bukti_pengeluaran' => '' ,
		'tanggal_bukti_pengeluaran' => NULL ,
		'pembeli' => NULL ,
		'negara_tujuan' => NULL ,
		'barang_jadi' => NULL ,
		'jumlah' => 0 ,
		'mata_uang' => NULL ,
		'nilai_barang' => '' ,
		'gudang' => NULL ,
		'harga_satuan' => '',
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'trans_status' => STATUS_PENDING
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'trans_pengeluaran_barang_jadi.id' ,
		1 => 'trans_pengeluaran_barang_jadi.trans_status',
		2 => 'trans_pengeluaran_barang_jadi.no_transaksi' ,
		3 => 'trans_pengeluaran_barang_jadi.tanggal' ,
		4 => 'master_barang_jadi.kode AS kode_barang_jadi' ,
		5 => 'master_gudang.kode AS kode_gudang' ,
		6 => 'trans_pengeluaran_barang_jadi.nilai_barang' ,
	);
	var $datatable_show_value_list = Array(
		0 => 'id' ,
		1 => 'trans_status',
		2 => 'no_transaksi' ,
		3 => 'tanggal' ,
		4 => 'kode_barang_jadi' ,
		5 => 'kode_gudang' ,
		6 => 'nilai_barang' 
	);
	var $datatable_search_value_list = Array(
		0 => 'trans_pengeluaran_barang_jadi.id' ,
		1 => 'trans_pemasukan_barang_jadi.trans_status',
		2 => 'trans_pengeluaran_barang_jadi.no_transaksi' ,
		3 => 'trans_pengeluaran_barang_jadi.tanggal' ,
		4 => 'master_barang_jadi.kode' ,
		5 => 'master_gudang.kode' ,
		6 => 'trans_pengeluaran_barang_jadi.nilai_barang'
	);
	var $fillable_value_list = array(
		'no_feb',
		'tanggal_feb',
		'no_bukti_pengeluaran',
		'tanggal_bukti_pengeluaran',
		'pembeli',
		'negara_tujuan',
		'barang_jadi',
		'jumlah',
		'mata_uang',
		'nilai_barang',
		'gudang',
		'harga_satuan',
	);
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

	function getDataTableCountActive($datatable_request){								
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM ".$this->table_name." ".
			"LEFT JOIN master_barang_jadi ON (master_barang_jadi.id = ".$this->table_name.".barang_jadi) ".
			"LEFT JOIN master_gudang ON (master_gudang.id = ".$this->table_name.".gudang) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." ".
			"LEFT JOIN master_barang_jadi ON (master_barang_jadi.id = ".$this->table_name.".barang_jadi) ".
			"LEFT JOIN master_gudang ON (master_gudang.id = ".$this->table_name.".gudang) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
	
	function getDailyReport($start_date, $end_date){
		$sql = "SELECT ".
				"trans_pengeluaran_barang_jadi.no_feb, ".
				"trans_pengeluaran_barang_jadi.tanggal_feb, ".
				"trans_pengeluaran_barang_jadi.no_bukti_pengeluaran, ".
				"trans_pengeluaran_barang_jadi.tanggal AS tanggal_bukti_pengeluaran, ".
				"master_customer.kode AS kode_pembeli, ".
				"master_barang_jadi.kode AS kode_barang, ".
				"master_barang_jadi.nama AS nama_barang, ".
				"master_satuan.kode AS kode_satuan, ".
				"trans_pengeluaran_barang_jadi.jumlah, ".
				"master_mata_uang.kode AS kode_mata_uang, ".
				"trans_pengeluaran_barang_jadi.nilai_barang, ".
				"master_gudang.kode AS kode_gudang, ".
				"master_negara.kode AS kode_negara ".
				"FROM ".
				"trans_pengeluaran_barang_jadi ".
				"LEFT JOIN master_barang_jadi ON (trans_pengeluaran_barang_jadi.barang_jadi = master_barang_jadi.id)  ".
				"LEFT JOIN master_satuan ON (master_barang_jadi.satuan = master_satuan.id)  ".
				"LEFT JOIN master_mata_uang ON (trans_pengeluaran_barang_jadi.mata_uang = master_mata_uang.id)  ".
				"LEFT JOIN master_gudang ON (trans_pengeluaran_barang_jadi.gudang = master_gudang.id)  ".
				"LEFT JOIN master_customer ON (trans_pengeluaran_barang_jadi.pembeli = master_customer.id)  ".
				"LEFT JOIN master_negara ON (trans_pengeluaran_barang_jadi.negara_tujuan = master_negara.id)  ".
				"WHERE trans_pengeluaran_barang_jadi.tanggal BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' AND trans_pengeluaran_barang_jadi.record_status = '".STATUS_ACTIVE."'";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
}

?>