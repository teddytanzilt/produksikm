<?php
class Trans_pemasukan_barang_jadi_model extends Core_master_model{	    	
	public $table_name = "trans_pemasukan_barang_jadi";
	var $default_value_list = Array(
		'id' => NULL ,
		'no_transaksi' => '' ,
		'tanggal' => NULL ,
		'no_bukti_penerimaan' => '' ,
		'tanggal_bukti_penerimaan' => NULL ,
		'barang_jadi' => NULL ,
		'jumlah' => 0 ,
		'gudang' => NULL ,
		'no_pemakaian_bahan_baku' => '',
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'trans_status' => STATUS_PENDING
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'trans_pemasukan_barang_jadi.id' ,
		1 => 'trans_pemasukan_barang_jadi.trans_status',
		2 => 'trans_pemasukan_barang_jadi.no_transaksi' ,
		3 => 'trans_pemasukan_barang_jadi.tanggal' ,
		4 => 'master_barang_jadi.kode AS kode_barang_jadi' ,
		5 => 'master_gudang.kode AS kode_gudang' ,
		6 => 'trans_pemasukan_barang_jadi.jumlah' ,
	);
	var $datatable_show_value_list = Array(
		0 => 'id' ,
		1 => 'trans_status',
		2 => 'no_transaksi' ,
		3 => 'tanggal' ,
		4 => 'kode_barang_jadi' ,
		5 => 'kode_gudang' ,
		6 => 'jumlah' 
	);
	var $datatable_search_value_list = Array(
		0 => 'trans_pemasukan_barang_jadi.id' ,
		1 => 'trans_pemasukan_barang_jadi.trans_status',
		2 => 'trans_pemasukan_barang_jadi.no_transaksi' ,
		3 => 'trans_pemasukan_barang_jadi.tanggal' ,
		4 => 'master_barang_jadi.kode' ,
		5 => 'master_gudang.kode' ,
		6 => 'trans_pemasukan_barang_jadi.jumlah'
	);
	var $fillable_value_list = array(
		'no_bukti_penerimaan',
		'tanggal_bukti_penerimaan',
		'barang_jadi',
		'jumlah',
		'gudang',
		'no_pemakaian_bahan_baku',
	);
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

	function getDataTableCountActive($datatable_request){								
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM ".$this->table_name." ".
			"LEFT JOIN master_barang_jadi ON (master_barang_jadi.id = ".$this->table_name.".barang_jadi) ".
			"LEFT JOIN master_gudang ON (master_gudang.id = ".$this->table_name.".gudang) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." ".
			"LEFT JOIN master_barang_jadi ON (master_barang_jadi.id = ".$this->table_name.".barang_jadi) ".
			"LEFT JOIN master_gudang ON (master_gudang.id = ".$this->table_name.".gudang) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
	
	function getDailyReport($start_date, $end_date){
		$sql = "SELECT ".
				"trans_pemasukan_barang_jadi.no_bukti_penerimaan, ".
				"trans_pemasukan_barang_jadi.tanggal_bukti_penerimaan, ".
				"master_barang_jadi.kode AS kode_barang, ".
				"master_barang_jadi.nama AS nama_barang, ".
				"master_satuan.kode AS kode_satuan, ".
				"trans_pemasukan_barang_jadi.jumlah, ".
				"master_gudang.kode AS kode_gudang ".
				"FROM ".
				"trans_pemasukan_barang_jadi ".
				"LEFT JOIN master_barang_jadi ON (trans_pemasukan_barang_jadi.barang_jadi = master_barang_jadi.id)  ".
				"LEFT JOIN master_satuan ON (master_barang_jadi.satuan = master_satuan.id)  ".
				"LEFT JOIN master_gudang ON (trans_pemasukan_barang_jadi.gudang = master_gudang.id)  ".
				"WHERE trans_pemasukan_barang_jadi.tanggal BETWEEN '".$start_date." 00:00:00' AND '".$end_date." 23:59:59' AND trans_pemasukan_barang_jadi.record_status = '".STATUS_ACTIVE."'";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
}

?>