<?php
class master_stock_barang_waste_model extends Core_master_model{	    	
	public $table_name = "master_stock_barang_waste";
	var $default_value_list = Array(
		'id_barang_waste' => NULL ,
		'id_gudang' => NULL ,
		'jumlah' => 0 ,
		'import_flag' => '' 
	);
	var $value_list  = array();
	var $array_condition  = array();
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }
	
	
	
	function saldoAwal($barang_waste, $gudang){								
		$sql = "SELECT jumlah FROM ".$this->table_name." WHERE id_barang_waste = ".$barang_waste." AND id_gudang = ".$gudang." ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == NULL){
			return NULL;
		} else {
			return $row['jumlah'];
		}
	}

}

?>