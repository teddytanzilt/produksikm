<?php
class Core_master_model extends CI_Model{	    	
	public $table_name = "";
	var $default_value_list = array(
		'id' => NULL ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE
	);
	var $fillable_value_list = array();
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = array(0 => 'id');
	var $datatable_search_value_list = array();
	var $datatable_show_value_list = array();
	
	function __construct()
    {        
        parent::__construct();		
		$this->value_list = $this->default_value_list;
    }

	function getValueList() {
	   return $this->value_list;
	}
	function setValueList($value_list) {
	   return $this->value_list = $value_list;
	}
	function refreshValueList(){
		$this->value_list = $this->default_value_list;
	}
	function getCond() {
	   return $this->array_condition;
	}
	function setCond($array_condition) {
	   return $this->array_condition = $array_condition;
	}
	function getFillableValueList() {
	   return $this->fillable_value_list;
	}
	function getDatatableValueList() {
	   return $this->datatable_value_list;
	}
	function getDatatableShowValueList() {
	   return $this->datatable_show_value_list;
	}
	function getDatatableSearchValueList() {
	   return $this->datatable_search_value_list;
	}
	
	function insertHeader(){
		$query = $this->db->insert($this->table_name, $this->value_list);
	}
	
	function updateHeader(){
		$this->db->where($this->array_condition);
		$query = $this->db->update($this->table_name, $this->value_list);
	}
	
	function deleteHeader(){
		$query = $this->db->delete($this->table_name, $this->array_condition);
	}
	
	function getHeaderField($field_name){	
		$this->db->select($field_name);
		$this->db->where($this->array_condition);
		$query = $this->db->get($this->table_name);
		$res = $query->row_array();										
		return $res[$field_name];
	}
	
	function getHeaderDetail(){								
		$this->db->select('*');
		$this->db->where($this->array_condition);
		$this->db->from($this->table_name);
		$res = $this->db->get()->result();
		return $res;
	}
	
	function getHeaderArray(){								
		$this->db->select('*');
		$this->db->where($this->array_condition);
		$this->db->from($this->table_name);		
		$res = $this->db->get()->row_array(0);
		return $res;
	}
	
	function checkExist(){							
		$this->db->where($this->array_condition);
		$this->db->from($this->table_name);
		$counted = $this->db->count_all_results();
		if($counted > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	function getList(){
		$this->db->select('*');
		$this->db->where($this->array_condition); 
		$this->db->from($this->table_name);	
		$res = $this->db->get()->result();
		return $res;			
	}
	
	function getListArray(){								
		$this->db->select('*');
		$this->db->where($this->array_condition);
		$this->db->from($this->table_name);		
		$res = $this->db->get()->result_array();
		return $res;
	}
	
	function getCountData(){								
		$this->db->where($this->array_condition);
		$this->db->from($this->table_name);
		$counted = $this->db->count_all_results();
		return $counted;
	}
	
	function sortBy($field_name, $order){								
		$this->db->order_by($field_name, $order);
	}
	
	function limitBy($limit, $start = 0){								
		$this->db->limit($limit, $start);
	}
	
	function randomize(){								
		$this->db->order_by($this->table_name.'.id', 'RANDOM');
	}
	
	function maxCol($col_name){								
		$sql = "SELECT MAX(".$col_name.") AS field_name FROM ".$this->table_name;
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}

	function getHeaderArrayById($id){								
		$this->db->where('id', $id);
		$res = $this->db->get($this->table_name)->row_array(0);
		return $res;
	}

	function getDataTableCount($datatable_request){								
		$sql = "SELECT COUNT(id) AS field_name FROM ".$this->table_name." WHERE 1=1 ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}

	function getDataTableData($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." WHERE 1=1 ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getCountDataActive(){		
		$this->array_condition = array('record_status' => STATUS_ACTIVE);
		$this->db->where($this->array_condition);
		$this->db->from($this->table_name);
		$counted = $this->db->count_all_results();
		return $counted;
	}
	
	function getDataTableCountActive($datatable_request){								
		$sql = "SELECT COUNT(id) AS field_name FROM ".$this->table_name." WHERE record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." WHERE record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
	
	function getDataById($id){								
		$this->db->where('id', $id);
		$this->db->where('record_status', STATUS_ACTIVE);
		$res = $this->db->get($this->table_name)->row_array(0);
		return $res;
	}
	
	function getActiveList($limit=null){
		$this->array_condition = array('record_status' => STATUS_ACTIVE);
		if($limit!=null){
			$this->limitBy($limit);
		}
		$res = $this->getList();
		return $res;
	}
	
	function nextTrans(){								
		$sql = "SELECT COUNT(*)+1 AS field_name FROM ".$this->table_name." WHERE YEAR(tanggal) = YEAR(NOW()) AND MONTH(tanggal) = MONTH(NOW())";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
}

?>