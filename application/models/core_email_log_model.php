<?php
class Core_email_log_model extends Core_master_model{	    	
	public $table_name = "core_email_log";
	var $default_value_list = Array(
		'id' => NULL ,
		'mail_from' => '' ,
		'mail_from_name' => '' ,
		'mail_to' => '' ,
		'mail_cc' => '' ,
		'mail_bcc' => '' ,
		'mail_subject' => '' ,
		'mail_body' => '' ,
		'mail_error' => '' ,
		'mail_error_info' => '' ,
		'created_on' => NULL
	);
	
	var $value_list  = array();
	var $array_condition  = array();
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }
}

?>