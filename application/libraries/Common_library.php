<?php
class Common_library
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();		
	}
	
	public function getData($data = null)
	{
		try{
			foreach($_POST as $arr_name => $arr_value){
				if($arr_name == "content" || $arr_name == "description" || $arr_name == "product_content" || $arr_name == "social_media_link"){
					$data[$arr_name] = $arr_value;
				} else {
					$data[$arr_name] = clean_value($arr_value);
				}
			}	
		} catch (Exception $e) {

		}
		return $data;
	}

	public function splitArray($arr, $splitter = ",") {
		$return_val = "";
		foreach($arr as $val){
			if($return_val == ""){
				$return_val = $val;
			} else {
				$return_val = $return_val.$splitter.$val;
			}
		}
		return $return_val;
	}
}
?>