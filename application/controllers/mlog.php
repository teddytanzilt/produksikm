<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mlog extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('core_app_log_model');
		
		$this->module_name = "mlog";
		$this->module_title = "Log";
		$this->table_name = "core_app_log";
		$this->model_object = $this->core_app_log_model;
		
		$this->link_back = $this->module_name;
		$this->link_view = $this->module_name."/view/";
		
		$this->view_list = $this->module_name."/list";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
	}

	public function index($data = null)
	{
		$this->module_subtitle = "List";
		$data[$this->module_name.'_list'] = null;
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->view_list;		
		$this->load->view('parts/template',$data);
	}

	public function get_data_list_json(){	
		$request_data= $_REQUEST;
		$column = $this->model_object->getDatatableValueList();
		$total_data = $this->model_object->getCountData();
		$total_filtered = $this->model_object->getDataTableCount($request_data);
		$data_filtered = $this->model_object->getDataTableData($request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}
	
}