<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rmutasibbaku extends Report_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('trans_inventory_bahan_baku_model');
		
		$this->module_name = "rmutasibbaku";
		$this->module_title = "Report Mutasi Bahan Baku";
		
		$this->view_report = $this->module_name."/report";
		
		$this->report_title = "Laporan Mutasi Bahan Baku";
		
		$core_app_config_cond = array('config_key' => 'COMPANY_NAME');
		$this->core_app_config_model->setCond($core_app_config_cond);
		$this->company_name =  $this->core_app_config_model->getHeaderField("config_value");
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
	}
	
	public function generate_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			if($data['type'] == "1"){
				$report_data = $this->trans_inventory_bahan_baku_model->getMutasi($data['from_date'],$data['to_date']);
				$this->generate_report($report_data,$data['from_date'],$data['to_date']);	
			} else {
				$report_data = $this->trans_inventory_bahan_baku_model->getMutasiDetail($data['from_date'],$data['to_date']);
				$this->generate_detail_report($report_data,$data['from_date'],$data['to_date']);
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			
		} else {
			$this->index($data);
		}
	}
	
	public function generate_report($report_data, $start_date, $end_date)
	{
		$this->load->library('datetime_library');
		$this->load->library('pdf');	
		
		$start_date = $this->datetime_library->indonesian_date($this->datetime_library->date_format($start_date.' 00:00:00', 'l jS F Y'), 'l, j F Y', '');
		$end_date = $this->datetime_library->indonesian_date($this->datetime_library->date_format($end_date.' 00:00:00', 'l jS F Y'), 'l, j F Y', '');
		
		$marginX = 12;
		$marginY = 12;
		$paperW = 210; 
		$paperH = 297; 
		
		$this->pdf->fontpath = 'assets/fonts/pdf/'; 
		$this->pdf->AddFont('Calibri');
		$this->pdf->AddFont('Calibri-Bold','','calibrib.php');
		$this->pdf->AliasNbPages();
		$this->pdf->Open();
		$this->pdf->SetAutoPageBreak(true, '10');
		
		$this->generate_report_header($this->pdf, $start_date, $end_date);
		
		$no = 1;
		$fontSize = 10;
		$titleFontSize = 9;
		foreach($report_data as $rd){
			if($no != 0 && $no % 49 == 0){
				$this->generate_report_header($this->pdf, $start_date, $end_date);
			}
			$this->pdf->SetFont('Calibri','',$fontSize);
			
			$this->pdf->Cell(10, 5, $no, 1, 0, 'C', true);
			$this->pdf->Cell(25, 5, $rd['kode_barang'], 1, 0, 'C', true);
			$this->pdf->Cell(25, 5, ((strlen($rd['nama_barang']) > 17)?substr($rd['nama_barang'],0,17):$rd['nama_barang']), 1, 0, 'C', true);
			$this->pdf->SetFont('Calibri-Bold','',$titleFontSize);
			$this->pdf->Cell(20, 5, number_format($rd['saldo_awal'],0,'.',','), 1, 0, 'C', true);
			$this->pdf->Cell(20, 5, number_format($rd['total_masuk'],0,'.',','), 1, 0, 'C', true);
			$this->pdf->Cell(20, 5, number_format($rd['total_keluar'],0,'.',','), 1, 0, 'C', true);
			$this->pdf->Cell(20, 5, number_format($rd['saldo_akhir'],0,'.',','), 1, 0, 'C', true);
			$this->pdf->SetFont('Calibri','',$fontSize);
			$this->pdf->Cell(25, 5, $rd['kode_satuan'], 1, 0, 'C', true);
			$this->pdf->SetFont('Calibri','',$titleFontSize);
			$this->pdf->Cell(25, 5, $rd['kode_gudang'], 1, 1, 'C', true);
			
			
			$no++;
		}

		$this->pdf->Ln(5);
		$this->pdf->Output();
	}
	
	public function generate_detail_report($report_data, $start_date, $end_date)
	{
		$this->load->library('datetime_library');
		$this->load->library('pdf');	
		
		$start_date = $this->datetime_library->indonesian_date($this->datetime_library->date_format($start_date.' 00:00:00', 'l jS F Y'), 'l, j F Y', '');
		$end_date = $this->datetime_library->indonesian_date($this->datetime_library->date_format($end_date.' 00:00:00', 'l jS F Y'), 'l, j F Y', '');
		
		$marginX = 12;
		$marginY = 12;
		$paperW = 210; 
		$paperH = 297; 
		
		$this->pdf->fontpath = 'assets/fonts/pdf/'; 
		$this->pdf->AddFont('Calibri');
		$this->pdf->AddFont('Calibri-Bold','','calibrib.php');
		$this->pdf->AliasNbPages();
		$this->pdf->Open();
		$this->pdf->SetAutoPageBreak(true, '10');
		
		$this->generate_report_detail_header($this->pdf, $start_date, $end_date);
		
		$no = 1;
		$fontSize = 10;
		$titleFontSize = 9;
		foreach($report_data as $rd){
			if($no != 0 && $no % 49 == 0){
				$this->generate_report_detail_header($this->pdf, $start_date, $end_date);
			}
			$this->pdf->SetFont('Calibri','',$fontSize);
			
			$this->pdf->Cell(10, 5, $no, 1, 0, 'C', true);
			$this->pdf->Cell(20, 5, $rd['kode_barang'], 1, 0, 'C', true);
			$this->pdf->Cell(30, 5, $rd['comment'], 1, 0, 'C', true);
			$this->pdf->Cell(35, 5, $rd['trans_date'], 1, 0, 'C', true);
			$this->pdf->SetFont('Calibri-Bold','',$titleFontSize);
			$this->pdf->Cell(20, 5, number_format($rd['saldo_awal'],0,'.',','), 1, 0, 'C', true);
			$this->pdf->Cell(20, 5, number_format($rd['jumlah_masuk'],0,'.',','), 1, 0, 'C', true);
			$this->pdf->Cell(20, 5, number_format($rd['jumlah_keluar'],0,'.',','), 1, 0, 'C', true);
			$this->pdf->Cell(20, 5, number_format($rd['saldo_akhir'],0,'.',','), 1, 0, 'C', true);
			$this->pdf->SetFont('Calibri','',$titleFontSize);
			$this->pdf->Cell(15, 5, $rd['kode_gudang'], 1, 1, 'C', true);
			
			
			$no++;
		}
		
		$this->pdf->Ln(5);
		$this->pdf->Output();
	}
	
	
	public function generate_report_header($obj, $start_date, $end_date)
	{
		$obj->AddPage();		
		
		$title = $this->report_title;
		$titleFontSize = 18;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(0, 0,strtoupper($title), 0, 0, 'C');
		$obj->Ln(8);
		
		$company_name = $this->company_name;
		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'PT', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$company_name, 0, 1, 'L');	

		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'Periode', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$start_date. ' - '.$end_date, 0, 1, 'L');	
		
		$obj->Ln(5);

		$titleFontSize = 9;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->SetFillColor(200,200,200);
		$obj->Cell(10, 5, 'No', 1, 0, 'C', true);
		$obj->Cell(25, 5, 'Kode Barang', 1, 0, 'C', true);
		$obj->Cell(25, 5, 'Nama Barang', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Saldo Awal', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Pemasukan', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Pengeluaran', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Saldo Akhir', 1, 0, 'C', true);
		$obj->Cell(25, 5, 'Satuan', 1, 0, 'C', true);
		$obj->Cell(25, 5, 'Gudang', 1, 1, 'C', true);

		$obj->SetFillColor(255,255,255);
		$obj->SetFont('Calibri','',$fontSize);
	}
	
	public function generate_report_detail_header($obj, $start_date, $end_date)
	{
		$obj->AddPage();		
		
		$title = $this->report_title;
		$titleFontSize = 18;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(0, 0,strtoupper($title), 0, 0, 'C');
		$obj->Ln(8);

		$company_name = $this->company_name;
		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'PT', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$company_name, 0, 1, 'L');	

		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'Periode', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$start_date. ' - '.$end_date, 0, 1, 'L');	
		
		$obj->Ln(5);

		$titleFontSize = 9;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->SetFillColor(200,200,200);
		$obj->Cell(10, 5, 'No', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Kode Barang', 1, 0, 'C', true);
		$obj->Cell(30, 5, 'Keterangan', 1, 0, 'C', true);
		$obj->Cell(35, 5, 'Tanggal', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Saldo Awal', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Pemasukan', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Pengeluaran', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Saldo Akhir', 1, 0, 'C', true);
		$obj->Cell(15, 5, 'Gudang', 1, 1, 'C', true);
		
		$obj->SetFillColor(255,255,255);
		$obj->SetFont('Calibri','',$fontSize);
	}
}