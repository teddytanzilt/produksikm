<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mbjadi extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('master_barang_jadi_model');
		$this->load->model('master_satuan_model');
		
		$this->module_name = "mbjadi";
		$this->module_title = "Master Barang Jadi";
		$this->table_name = "master_barang_jadi";
		$this->model_object = $this->master_barang_jadi_model;
		
		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";
		
		$this->msg_add_success = "Create Barang Jadi Success.";
		$this->msg_edit_success = "Edit Barang Jadi Success.";
		$this->msg_delete_success = "Delete Barang Jadi Success.";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
	}
	
	public function create_load_data($data = null) {
		$data['satuan_list'] = $this->master_satuan_model->getActiveList();
		return $data;
	}
	
	public function edit_load_data($data = null) {
		$data['satuan_list'] = $this->master_satuan_model->getActiveList();
		return $data;
	}
	
	public function get_data_list_active_json(){	
		$request_data= $_REQUEST;
		$column = $this->model_object->getDatatableShowValueList();
		$total_data = $this->model_object->getCountDataActive();
		$total_filtered = $this->model_object->getDataTableCountActive($request_data);
		$data_filtered = $this->model_object->getDataTableDataActive($request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}
}