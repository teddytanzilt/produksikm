<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends Common_Controller {
	function __construct(){
		parent::__construct();

		$this->load->model('master_stock_bahan_baku_model');
		$this->load->model('trans_inventory_bahan_baku_model');
		$this->load->model('trans_pemasukan_bahan_baku_model');
		$this->load->model('trans_pemakaian_bahan_baku_model');


		$this->load->model('master_stock_barang_jadi_model');
		$this->load->model('trans_inventory_barang_jadi_model');
		$this->load->model('trans_pemasukan_barang_jadi_model');
		$this->load->model('trans_pengeluaran_barang_jadi_model');
	}
	
	
	public function undo_pemasukan_bbaku()
	{
		$data = $this->common_library->getData();
		//key
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		if($result['validation']) {
			if($data['api_key'] != md5(date("Ymd"))){
				$result['validation'] = false;
				$result['message'] = "Api Key Wrong";
			}
		}
		if($result['validation']) {
			$trans_cond = array('no_transaksi' =>  $data['no_transaksi'], 'record_status' => STATUS_ACTIVE, 'trans_status' => STATUS_CONFIRM);
			$this->trans_pemasukan_bahan_baku_model->setCond($trans_cond);
			if(!$this->trans_pemasukan_bahan_baku_model->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Confirmed Transaction Not Exist";
			}
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$item = 0;
					$gudang = 0;
					$jumlah = 0;

					$trans_cond = array('no_transaksi' =>  $data['no_transaksi'], 'record_status' => STATUS_ACTIVE);
					$this->trans_pemasukan_bahan_baku_model->setCond($trans_cond);
					$header_value = $this->trans_pemasukan_bahan_baku_model->getHeaderArray();
					$header_value['trans_status'] = STATUS_PENDING;
					$this->trans_pemasukan_bahan_baku_model->setValueList($header_value);
					$this->trans_pemasukan_bahan_baku_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$item = $header_value['bahan_baku'];
					$gudang = $header_value['gudang'];
					$jumlah = $header_value['jumlah'];

					$saldo_awal = $this->master_stock_bahan_baku_model->saldoAwal($item, $gudang);
					$jumlah_masuk = 0;
					$jumlah_keluar = $jumlah;
					$saldo_akhir = intval($saldo_awal) + intval($jumlah_masuk) - intval($jumlah_keluar);

					$content_value = $this->trans_inventory_bahan_baku_model->getValueList();
					$content_value['id_bahan_baku'] = $item;
					$content_value['id_gudang'] = $gudang;
					$content_value['id_user'] = 0;
					$content_value['trans_date'] = date('Y-m-d H:i:s');
					$content_value['comment'] = '-'.$data['no_transaksi'];
					$content_value['saldo_awal'] = $saldo_awal;
					$content_value['jumlah_masuk'] = $jumlah_masuk;
					$content_value['jumlah_keluar'] = $jumlah_keluar;
					$content_value['saldo_akhir'] = $saldo_akhir;
					$content_value["created_by"] = 0;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->trans_inventory_bahan_baku_model->setValueList($content_value);		
					$this->trans_inventory_bahan_baku_model->insertHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$stock_cond = array('id_bahan_baku' => $item, 'id_gudang' => $gudang);
					$this->master_stock_bahan_baku_model->setCond($stock_cond);
					$stock_value = $this->master_stock_bahan_baku_model->getHeaderArray();
					$adjustment = intval($jumlah) * -1;
					$stock_value['jumlah'] = intval($stock_value['jumlah']) + intval($adjustment);
					$this->master_stock_bahan_baku_model->setValueList($stock_value);
					$this->master_stock_bahan_baku_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		echo json_encode($result);
	}
	
	public function undo_pemakaian_bbaku()
	{
		$data = $this->common_library->getData();
		//key
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		if($result['validation']) {
			if($data['api_key'] != md5(date("Ymd"))){
				$result['validation'] = false;
				$result['message'] = "Api Key Wrong";
			}
		}
		if($result['validation']) {
			$trans_cond = array('no_transaksi' =>  $data['no_transaksi'], 'record_status' => STATUS_ACTIVE, 'trans_status' => STATUS_CONFIRM);
			$this->trans_pemakaian_bahan_baku_model->setCond($trans_cond);
			if(!$this->trans_pemakaian_bahan_baku_model->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Confirmed Transaction Not Exist";
			}
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$item = 0;
					$gudang = 0;
					$jumlah = 0;

					$trans_cond = array('no_transaksi' =>  $data['no_transaksi'], 'record_status' => STATUS_ACTIVE);
					$this->trans_pemakaian_bahan_baku_model->setCond($trans_cond);
					$header_value = $this->trans_pemakaian_bahan_baku_model->getHeaderArray();
					$header_value['trans_status'] = STATUS_PENDING;
					$this->trans_pemakaian_bahan_baku_model->setValueList($header_value);
					$this->trans_pemakaian_bahan_baku_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$item = $header_value['bahan_baku'];
					$gudang = $header_value['gudang'];
					$jumlah = $header_value['jumlah'];

					$saldo_awal = $this->master_stock_bahan_baku_model->saldoAwal($item, $gudang);
					$jumlah_masuk = $jumlah;
					$jumlah_keluar = 0;
					$saldo_akhir = intval($saldo_awal) + intval($jumlah_masuk) - intval($jumlah_keluar);

					$content_value = $this->trans_inventory_bahan_baku_model->getValueList();
					$content_value['id_bahan_baku'] = $item;
					$content_value['id_gudang'] = $gudang;
					$content_value['id_user'] = 0;
					$content_value['trans_date'] = date('Y-m-d H:i:s');
					$content_value['comment'] = '-'.$data['no_transaksi'];
					$content_value['saldo_awal'] = $saldo_awal;
					$content_value['jumlah_masuk'] = $jumlah_masuk;
					$content_value['jumlah_keluar'] = $jumlah_keluar;
					$content_value['saldo_akhir'] = $saldo_akhir;
					$content_value["created_by"] = 0;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->trans_inventory_bahan_baku_model->setValueList($content_value);		
					$this->trans_inventory_bahan_baku_model->insertHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$stock_cond = array('id_bahan_baku' => $item, 'id_gudang' => $gudang);
					$this->master_stock_bahan_baku_model->setCond($stock_cond);
					$stock_value = $this->master_stock_bahan_baku_model->getHeaderArray();
					$adjustment = intval($jumlah) * 1;
					$stock_value['jumlah'] = intval($stock_value['jumlah']) + intval($adjustment);
					$this->master_stock_bahan_baku_model->setValueList($stock_value);
					$this->master_stock_bahan_baku_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		echo json_encode($result);
	}

	public function undo_pemasukan_bjadi()
	{
		$data = $this->common_library->getData();
		//key
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		if($result['validation']) {
			if($data['api_key'] != md5(date("Ymd"))){
				$result['validation'] = false;
				$result['message'] = "Api Key Wrong";
			}
		}
		if($result['validation']) {
			$trans_cond = array('no_transaksi' =>  $data['no_transaksi'], 'record_status' => STATUS_ACTIVE, 'trans_status' => STATUS_CONFIRM);
			$this->trans_pemasukan_barang_jadi_model->setCond($trans_cond);
			if(!$this->trans_pemasukan_barang_jadi_model->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Confirmed Transaction Not Exist";
			}
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$item = 0;
					$gudang = 0;
					$jumlah = 0;

					$trans_cond = array('no_transaksi' =>  $data['no_transaksi'], 'record_status' => STATUS_ACTIVE);
					$this->trans_pemasukan_barang_jadi_model->setCond($trans_cond);
					$header_value = $this->trans_pemasukan_barang_jadi_model->getHeaderArray();
					$header_value['trans_status'] = STATUS_PENDING;
					$this->trans_pemasukan_barang_jadi_model->setValueList($header_value);
					$this->trans_pemasukan_barang_jadi_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$item = $header_value['barang_jadi'];
					$gudang = $header_value['gudang'];
					$jumlah = $header_value['jumlah'];

					$saldo_awal = $this->master_stock_barang_jadi_model->saldoAwal($item, $gudang);
					$jumlah_masuk = 0;
					$jumlah_keluar = $jumlah;
					$saldo_akhir = intval($saldo_awal) + intval($jumlah_masuk) - intval($jumlah_keluar);

					$content_value = $this->trans_inventory_barang_jadi_model->getValueList();
					$content_value['id_barang_jadi'] = $item;
					$content_value['id_gudang'] = $gudang;
					$content_value['id_user'] = 0;
					$content_value['trans_date'] = date('Y-m-d H:i:s');
					$content_value['comment'] = '-'.$data['no_transaksi'];
					$content_value['saldo_awal'] = $saldo_awal;
					$content_value['jumlah_masuk'] = $jumlah_masuk;
					$content_value['jumlah_keluar'] = $jumlah_keluar;
					$content_value['saldo_akhir'] = $saldo_akhir;
					$content_value["created_by"] = 0;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->trans_inventory_barang_jadi_model->setValueList($content_value);		
					$this->trans_inventory_barang_jadi_model->insertHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$stock_cond = array('id_barang_jadi' => $item, 'id_gudang' => $gudang);
					$this->master_stock_barang_jadi_model->setCond($stock_cond);
					$stock_value = $this->master_stock_barang_jadi_model->getHeaderArray();
					$adjustment = intval($jumlah) * -1;
					$stock_value['jumlah'] = intval($stock_value['jumlah']) + intval($adjustment);
					$this->master_stock_barang_jadi_model->setValueList($stock_value);
					$this->master_stock_barang_jadi_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		echo json_encode($result);
	}

	public function undo_pengeluaran_bjadi()
	{
		$data = $this->common_library->getData();
		//key
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		if($result['validation']) {
			if($data['api_key'] != md5(date("Ymd"))){
				$result['validation'] = false;
				$result['message'] = "Api Key Wrong";
			}
		}
		if($result['validation']) {
			$trans_cond = array('no_transaksi' =>  $data['no_transaksi'], 'record_status' => STATUS_ACTIVE, 'trans_status' => STATUS_CONFIRM);
			$this->trans_pengeluaran_barang_jadi_model->setCond($trans_cond);
			if(!$this->trans_pengeluaran_barang_jadi_model->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Confirmed Transaction Not Exist";
			}
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$item = 0;
					$gudang = 0;
					$jumlah = 0;

					$trans_cond = array('no_transaksi' =>  $data['no_transaksi'], 'record_status' => STATUS_ACTIVE);
					$this->trans_pengeluaran_barang_jadi_model->setCond($trans_cond);
					$header_value = $this->trans_pengeluaran_barang_jadi_model->getHeaderArray();
					$header_value['trans_status'] = STATUS_PENDING;
					$this->trans_pengeluaran_barang_jadi_model->setValueList($header_value);
					$this->trans_pengeluaran_barang_jadi_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$item = $header_value['barang_jadi'];
					$gudang = $header_value['gudang'];
					$jumlah = $header_value['jumlah'];

					$saldo_awal = $this->master_stock_barang_jadi_model->saldoAwal($item, $gudang);
					$jumlah_masuk = $jumlah;
					$jumlah_keluar = 0;
					$saldo_akhir = intval($saldo_awal) + intval($jumlah_masuk) - intval($jumlah_keluar);

					$content_value = $this->trans_inventory_barang_jadi_model->getValueList();
					$content_value['id_barang_jadi'] = $item;
					$content_value['id_gudang'] = $gudang;
					$content_value['id_user'] = 0;
					$content_value['trans_date'] = date('Y-m-d H:i:s');
					$content_value['comment'] = '-'.$data['no_transaksi'];
					$content_value['saldo_awal'] = $saldo_awal;
					$content_value['jumlah_masuk'] = $jumlah_masuk;
					$content_value['jumlah_keluar'] = $jumlah_keluar;
					$content_value['saldo_akhir'] = $saldo_akhir;
					$content_value["created_by"] = 0;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->trans_inventory_barang_jadi_model->setValueList($content_value);		
					$this->trans_inventory_barang_jadi_model->insertHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$stock_cond = array('id_barang_jadi' => $item, 'id_gudang' => $gudang);
					$this->master_stock_barang_jadi_model->setCond($stock_cond);
					$stock_value = $this->master_stock_barang_jadi_model->getHeaderArray();
					$adjustment = intval($jumlah) * 1;
					$stock_value['jumlah'] = intval($stock_value['jumlah']) + intval($adjustment);
					$this->master_stock_barang_jadi_model->setValueList($stock_value);
					$this->master_stock_barang_jadi_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		echo json_encode($result);
	}


}