<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpemasukanbjadi extends Report_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('trans_pemasukan_barang_jadi_model');
		
		$this->module_name = "rpemasukanbjadi";
		$this->module_title = "Report Pemasukan Hasil Produksi";
		
		$this->model_object = $this->trans_pemasukan_barang_jadi_model;
		
		$this->view_report = $this->module_name."/report";
		
		$this->report_title = "Laporan Pemasukan Hasil Produksi";
		
		$core_app_config_cond = array('config_key' => 'COMPANY_NAME');
		$this->core_app_config_model->setCond($core_app_config_cond);
		$this->company_name =  $this->core_app_config_model->getHeaderField("config_value");
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
	}
	
	public function generate_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			$report_data = $this->model_object->getDailyReport($data['from_date'],$data['to_date']);
			$this->generate_report($report_data,$data['from_date'],$data['to_date']);	
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			
		} else {
			$this->index($data);
		}
	}
	
	public function generate_report($report_data, $start_date, $end_date)
	{
		$this->load->library('datetime_library');
		$this->load->library('pdf');	
		
		$start_date = $this->datetime_library->indonesian_date($this->datetime_library->date_format($start_date.' 00:00:00', 'l jS F Y'), 'l, j F Y', '');
		$end_date = $this->datetime_library->indonesian_date($this->datetime_library->date_format($end_date.' 00:00:00', 'l jS F Y'), 'l, j F Y', '');
		
		$marginX = 12;
		$marginY = 12;
		$paperW = 210; 
		$paperH = 297; 
		
		$this->pdf->fontpath = 'assets/fonts/pdf/'; 
		$this->pdf->AddFont('Calibri');
		$this->pdf->AddFont('Calibri-Bold','','calibrib.php');
		$this->pdf->AliasNbPages();
		$this->pdf->Open();
		$this->pdf->SetAutoPageBreak(true, '10');
		
		$this->generate_report_header($this->pdf, $start_date, $end_date);
		
		$no = 1;
		$fontSize = 9;
		foreach($report_data as $rd){
			if($no != 0 && $no % 48 == 0){
				$this->generate_report_header($this->pdf, $start_date, $end_date);
			}
			
			$this->pdf->SetFont('Calibri','',$fontSize);
			
			$this->pdf->Cell(10,5,$no,'LBR',0,'C',1);
			$this->pdf->Cell(20,5,$rd['no_bukti_penerimaan'],'LBR',0,'C',1);
			$this->pdf->Cell(25,5,substr($rd['tanggal_bukti_penerimaan'], 0, 10),'LTBR',0,'C',1);
			$this->pdf->Cell(20,5,$rd['kode_barang'],'LBR',0,'C',1);
			$this->pdf->Cell(30,5,((strlen($rd['nama_barang']) > 20)?substr($rd['nama_barang'],0,20):$rd['nama_barang']),'LBR',0,'L',1);
			$this->pdf->Cell(25,5,number_format($rd['jumlah'],0,'.',','),'LTBR',0,'R',1);
			$this->pdf->Cell(25,5,' - ','LTBR',0,'R',1);
			$this->pdf->Cell(15,5,$rd['kode_satuan'],'LBR',0,'C',1);
			$this->pdf->Cell(17,5,$rd['kode_gudang'],'LBR',0,'C',1);
			
			$no++;
			
			$this->pdf->Ln();
		}
		
		$this->pdf->Ln(5);
		$this->pdf->Output();
	}

	public function generate_report_header($obj, $start_date, $end_date)
	{
		$obj->AddPage();		
		
		$title = $this->report_title;
		$titleFontSize = 18;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(0, 0,strtoupper($title), 0, 0, 'C');
		$obj->Ln(8);

		$company_name = $this->company_name;
		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'PT', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$company_name, 0, 1, 'L');	

		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'Periode', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$start_date. ' - '.$end_date, 0, 1, 'L');	
		
		$obj->Ln(5);

		$titleFontSize = 9;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->SetFillColor(200,200,200);
		
		$obj->Cell(10,5,'No','LTR',0,'C',1);
		$obj->Cell(45,5,'Bukti Penerimaan','LTBR',0,'C',1);
		$obj->Cell(20,5,'Kode','LTR',0,'C',1);
		$obj->Cell(30,5,'Nama','LTR',0,'C',1);
		$obj->Cell(50,5,'Jumlah','LTBR',0,'C',1);
		$obj->Cell(15,5,'Satuan','LTR',0,'C',1);
		$obj->Cell(17,5,'Gudang','LTR',0,'C',1);
		
		$obj->Ln();
		/*******************************************/
		$obj->Cell(10,5,' ','LBR',0,'C',1);
		$obj->Cell(20,5,'Nomor','LBR',0,'C',1);
		$obj->Cell(25,5,'Tanggal','LTBR',0,'C',1);
		$obj->Cell(20,5,'Barang','LBR',0,'C',1);
		$obj->Cell(30,5,'Barang','LBR',0,'C',1);
		$obj->Cell(25,5,'Dari Produksi','LTBR',0,'C',1);
		$obj->Cell(25,5,'Dari subkontrak','LTBR',0,'C',1);
		$obj->Cell(15,5,' ','LBR',0,'C',1);
		$obj->Cell(17,5,' ','LBR',0,'C',1);
		
		$obj->Ln();
		/*******************************************/

		$obj->SetFillColor(255,255,255);
		$obj->SetFont('Calibri','',$fontSize);

	}
}