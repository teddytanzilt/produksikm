<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Msetting extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('core_app_config_model');
		
		$this->module_name = "msetting";
		$this->module_title = "Master Setting";
		$this->table_name = "core_app_config";
		$this->model_object = $this->core_app_config_model;
		
		$this->link_back = $this->module_name;
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		
		$this->view_edit = $this->module_name."/edit";
		
		$this->msg_edit_success = "Edit Setting Success.";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
	}
	
	public function index($data = null)
	{
		$this->edit($data);
	}
	
	public function edit($data = null)
	{
		foreach($this->core_app_config_model->get_all()->result() as $app_config)
		{
			$data[$app_config->config_key] = $app_config->config_value;
		}
		$data = $this->edit_load_data($data);
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->view_edit;		
		$this->load->view('parts/template',$data);
	}
	
	public function edit_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}		
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$batch_save_data=array(
					'WEB_NAME'=>$data['WEB_NAME'],
					'COMPANY_NAME'=>$data['COMPANY_NAME']
				);
				$this->core_app_config_model->batch_save($batch_save_data);
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_edit_success);
			redirect($this->link_back);
		} else {
			$this->edit($data);
		}
	}
}