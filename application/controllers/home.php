<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Common_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('trans_inventory_bahan_baku_model');
		$this->load->model('trans_inventory_barang_jadi_model');
	}
	
	public function index()
	{
		$this->session_library->check_session_auth();
	}
	
	public function dashboard()
	{
		$this->module_name = "home";
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		$data['total_jumlah_masuk_bbaku'] = $this->trans_inventory_bahan_baku_model->getJumlah("jumlah_masuk", date("Y-m-")."01", date("Y-m-d"));
		$data['total_jumlah_keluar_bbaku'] = $this->trans_inventory_bahan_baku_model->getJumlah("jumlah_keluar", date("Y-m-")."01", date("Y-m-d"));
		$data['total_jumlah_masuk_bjadi'] = $this->trans_inventory_barang_jadi_model->getJumlah("jumlah_masuk", date("Y-m-")."01", date("Y-m-d"));
		$data['total_jumlah_keluar_bjadi'] = $this->trans_inventory_barang_jadi_model->getJumlah("jumlah_keluar", date("Y-m-")."01", date("Y-m-d"));
		$data['title'] = $this->web_name.' | Dashboard';		
		$data['content'] = 'dashboard';		
		$this->load->view('parts/template',$data);
	}
	
	public function login($data = null)
	{
		if($this->session_library->check_session_auth_exist(TRUE)){
			redirect('home/dashboard');
			exit;
		}
		$data['title'] = $this->web_name.' | Login';		
		$data['content'] = 'login';		
		$this->load->view('parts/template',$data);
	}
	
	public function login_process()
	{
		if($this->session_library->check_session_auth_exist(TRUE)){
			redirect('home/dashboard');
			exit;
		}
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/	
		if($result['validation']){
			$user_cond = array('record_status' => STATUS_ACTIVE, 'user_status' => STATUS_SUSPEND, 'user_name' => $data['user_name'], 'password' =>  md5($data['password']));
			$this->master_user_model->setCond($user_cond);	
			if($this->master_user_model->checkExist()){
				$result['validation'] = false;
				$result['message'] = "The user is suspended.";
			}	
		}
		if($result['validation']){
			$user_cond = array('record_status' => STATUS_ACTIVE, 'user_status' => STATUS_ACTIVE, 'user_name' => $data['user_name'], 'password' =>  md5($data['password']));
			$this->master_user_model->setCond($user_cond);	
			if(!$this->master_user_model->checkExist()){
				$result['validation'] = false;
				$result['message'] = "The password does not match.";
			}
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			$user_cond = array('record_status' => STATUS_ACTIVE, 'user_name' => $data['user_name'], 'password' =>  md5($data['password']));
			$this->master_user_model->setCond($user_cond);	
			$user_value =  $this->master_user_model->getHeaderArray();
			$this->session->set_userdata(SESSION_CHECK, 1);
			$this->session->set_userdata('session_user_id', $user_value["id"]);
			$this->session->set_userdata('session_user_name', $user_value["user_name"]);
			$this->session->set_userdata('session_user_role', $user_value["role"]);
			$this->log_library->writeLog($result);
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/
		if($result['validation']) {
			redirect('home/dashboard');		
		} else {
			$this->login($data);
		}
	}
	
	public function logout_process()
	{
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}	
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		$result['validation'] = true;
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/	
		if($result['validation']) {
			$this->session->sess_destroy();
			$this->log_library->writeLog($result);
		}
		/*********Process ends here ***********/	
		$this->index();
	}
	
	public function getSearchResult()
	{
		$this->load->model('trans_mix_model');
		$match = clean_value($this->input->post('match'));
		$list = $this->trans_mix_model->searchTrans($match);
		header('Content-type: application/json');
		echo json_encode($list);
	}
	
}