<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tpemasukanbjadi extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('master_barang_jadi_model');
		$this->load->model('master_gudang_model');
		$this->load->model('trans_pemakaian_bahan_baku_model');
		$this->load->model('master_stock_barang_jadi_model');
		$this->load->model('trans_inventory_barang_jadi_model');
		$this->load->model('trans_pemasukan_barang_jadi_model');
		
		$this->module_name = "tpemasukanbjadi";
		$this->module_title = "Pemasukan Hasil Produksi";
		$this->table_name = "trans_pemasukan_barang_jadi";
		$this->model_object = $this->trans_pemasukan_barang_jadi_model;
		
		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";
		
		$this->msg_add_success = "Create Data Success.";
		$this->msg_edit_success = "Edit Data Success.";
		$this->msg_delete_success = "Delete Data Success.";
		
		$this->trans_prefix = "IBJ";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
	}
	
	public function create_load_data($data = null) {
		$data['barang_jadi_list'] = $this->master_barang_jadi_model->getActiveList();
		$data['gudang_list'] = $this->master_gudang_model->getActiveList();
		$data['pemakaian_bahan_baku_list'] = $this->trans_pemakaian_bahan_baku_model->getActiveList();
		return $data;
	}
	
	public function edit_load_data($data = null) {
		$data['barang_jadi_list'] = $this->master_barang_jadi_model->getActiveList();
		$data['gudang_list'] = $this->master_gudang_model->getActiveList();
		$data['pemakaian_bahan_baku_list'] = $this->trans_pemakaian_bahan_baku_model->getActiveList();
		return $data;
	}
	
	public function view_load_data($data = null) {
		$data['barang_jadi'] = $this->master_barang_jadi_model->getDataById($data['barang_jadi']);
		$data['gudang'] = $this->master_gudang_model->getDataById($data['gudang']);
		return $data;
	}
	
	public function get_data_list_active_json(){	
		$request_data= $_REQUEST;
		$column = $this->model_object->getDatatableShowValueList();
		$total_data = $this->model_object->getCountDataActive();
		$total_filtered = $this->model_object->getDataTableCountActive($request_data);
		$data_filtered = $this->model_object->getDataTableDataActive($request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}
	
	public function create_process()
	{
		$data = $this->common_library->getData();
		$data['no_pemakaian_bahan_baku'] = $this->input->post('no_pemakaian_bahan_baku');
		if($data['no_pemakaian_bahan_baku'] != ""){
			$data['no_pemakaian_bahan_baku'] = $this->common_library->splitArray($data['no_pemakaian_bahan_baku'], ",");
		}
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$next_id = $this->model_object->nextTrans();
					$fillable_value = $this->model_object->getFillableValueList();
					$content_value = $this->model_object->getValueList();
					$content_value['no_transaksi'] = '';
					//$content_value['tanggal'] = date('Y-m-d H:i:s');
					$content_value['tanggal'] = $data['tanggal'];
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["created_by"] = $this->session_user_id;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->insertHeader();
					$content_id = $this->db->insert_id();
					$no_transaksi = $this->trans_prefix.date('Ym').sprintf("%05d", $next_id);
					$content_value['id'] = $content_id;
					$content_value['no_transaksi'] = $no_transaksi;
					$this->model_object->setValueList($content_value);		
					$value_condition = array('id' => $content_id);
					$this->model_object->setCond($value_condition);
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_add_success);
			redirect($this->link_back);
		} else {
			$this->create($data);
		}
	}
	
	public function confirm($id, $data = null)
	{
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		$content_detail = $this->model_object->getHeaderArray();	
		foreach($content_detail as $arr_name => $arr_value){
			$data[$arr_name] = $arr_value;
		}
		$data = $this->edit_load_data($data);
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->view_edit;		
		$this->load->view('parts/template',$data);
	}
	
	public function confirm_process()
	{
		$data = $this->common_library->getData();
		$data['no_pemakaian_bahan_baku'] = $this->input->post('no_pemakaian_bahan_baku');
		if($data['no_pemakaian_bahan_baku'] != ""){
			$data['no_pemakaian_bahan_baku'] = $this->common_library->splitArray($data['no_pemakaian_bahan_baku'], ",");
		}
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$fillable_value = $this->model_object->getFillableValueList();
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
					$this->model_object->setCond($content_cond);	
					$content_value = $this->model_object->getHeaderArray();
					$content_value['tanggal'] = substr($data['tanggal'],0,10).' '.date('H:i:s');
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["trans_status"] = STATUS_CONFIRM;
					$content_value["modified_by"] = $this->session_user_id;
					$content_value["modified_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$saldo_awal = $this->master_stock_barang_jadi_model->saldoAwal($data['barang_jadi'], $data['gudang']);
					$initial_saldo = FALSE;
					if($saldo_awal == NULL){
						$initial_saldo = TRUE;
						$saldo_awal = 0;
					}
					$jumlah_masuk = $data['jumlah'];
					$jumlah_keluar = 0;
					$saldo_akhir = intval($saldo_awal) + intval($jumlah_masuk) - intval($jumlah_keluar);
					$content_value = $this->master_stock_barang_jadi_model->getValueList();
					$content_value['id_barang_jadi'] = $data['barang_jadi'];
					$content_value['id_gudang'] = $data['gudang'];
					$content_value['jumlah'] = $saldo_akhir;
					$this->master_stock_barang_jadi_model->setValueList($content_value);		
					if($initial_saldo){
						$this->master_stock_barang_jadi_model->insertHeader();
					} else {
						$value_condition = array('id_barang_jadi' => $data['barang_jadi'] , 'id_gudang' => $data['gudang']);
						$this->master_stock_barang_jadi_model->setCond($value_condition);
						$this->master_stock_barang_jadi_model->updateHeader();
					}
					if ($this->db->trans_status() === FALSE){ break; }
					
					$saldo_awal_inventory = $this->trans_inventory_barang_jadi_model->getSaldoPerTanggalByGudang(substr($data['tanggal'],0,10).' '.date('H:i:s'), $data['barang_jadi'], $data['gudang']);
					$saldo_akhir_inventory = intval($saldo_awal_inventory) + intval($jumlah_masuk) - intval($jumlah_keluar);
					$content_value = $this->trans_inventory_barang_jadi_model->getValueList();
					$content_value['id_barang_jadi'] = $data['barang_jadi'];
					$content_value['id_gudang'] = $data['gudang'];
					$content_value['id_user'] = $this->session_user_id;
					//$content_value['trans_date'] = date('Y-m-d H:i:s');
					$content_value['trans_date'] = substr($data['tanggal'],0,10).' '.date('H:i:s');
					$content_value['comment'] = $data['no_transaksi'];
					$content_value['saldo_awal'] = $saldo_awal_inventory;
					$content_value['jumlah_masuk'] = $jumlah_masuk;
					$content_value['jumlah_keluar'] = $jumlah_keluar;
					$content_value['saldo_akhir'] = $saldo_akhir_inventory;
					$content_value['reff_id'] = $data['id'];
					$content_value["created_by"] = $this->session_user_id;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->trans_inventory_barang_jadi_model->setValueList($content_value);		
					$this->trans_inventory_barang_jadi_model->insertHeader();
					$inventory_id = $this->db->insert_id();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$updateAmount = intval($jumlah_masuk) - intval($jumlah_keluar);
					$this->trans_inventory_barang_jadi_model->updateSaldoPerTanggalByGudang($updateAmount, substr($data['tanggal'],0,10).' '.date('H:i:s'), $inventory_id, $data['barang_jadi'], $data['gudang']);
					if ($this->db->trans_status() === FALSE){ break; }
					
					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_add_success);
			redirect($this->link_back);
		} else {
			$this->confirm($data['id'], $data);
		}
	}
}