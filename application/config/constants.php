<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


define('STATUS_ACTIVE', "ACTIVE");
define('STATUS_INACTIVE', "INACTIVE");
define('STATUS_PROCESS', "IN PROCESS");
define('STATUS_CANCEL', "CANCELLED");
define('STATUS_PENDING', "PENDING");
define('STATUS_COMPLETE', "COMPLETED");
define('STATUS_DELETE', "DELETED");
define('STATUS_DELIVER', "DELIVERED");
define('STATUS_SUSPEND', "SUSPEND");
define('STATUS_CONFIRM', "CONFIRMED");

define('SESSION_CHECK', "CLIENTSESSIONENABLE");
define('ADMIN_FOLDER', "");
define('COPYRIGHT_YEAR', "2018");
define('PROGRAM_VERSION', "1.0.1");

define('CAPTCHA_SITE_KEY', "6LexHTEUAAAAAA5Lfs_4u1aPM8DdMbf-2a7g-MWT");
define('CAPTCHA_SECRET_KEY', "6LexHTEUAAAAANOUEo0OjAy6iH0-7JPzATXPn2UH");

/* End of file constants.php */
/* Location: ./application/config/constants.php */