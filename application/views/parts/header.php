<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?PHP echo $title; ?>">
	<meta name="author" content="Teddy">
	<meta name="keyword" content="<?PHP echo $title; ?>">
	<link rel="shortcut icon" href="<?PHP echo IMAGEPATH; ?>/favicon.ico"/>		
	<title><?PHP echo $title; ?></title>
	<base href="<?PHP echo base_url(); ?>" />

	<!-- Bootstrap CSS -->
	<link href="<?PHP echo CSSPATH; ?>/bootstrap.min.css" rel="stylesheet">
	<!-- bootstrap theme -->
	<link href="<?PHP echo CSSPATH; ?>/bootstrap-theme.css" rel="stylesheet">
	<!--external css-->
	<!-- font icon -->
	<link href="<?PHP echo CSSPATH; ?>/elegant-icons-style.css" rel="stylesheet" />
	<link href="<?PHP echo CSSPATH; ?>/font-awesome.min.css" rel="stylesheet" />
	<!-- Custom styles -->
	<link href="<?PHP echo CSSPATH; ?>/style.css" rel="stylesheet">
	<link href="<?PHP echo CSSPATH; ?>/style-responsive.css" rel="stylesheet" />
	<style>
		.wrapper { min-height:530px; }
	</style>
</head>
