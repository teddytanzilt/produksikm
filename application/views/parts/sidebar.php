	<!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="<?PHP echo ($this->module_name=="home")?'active':''; ?>">
			<a class="" href="home/dashboard">
				<i class="icon_house_alt"></i>
				<span>Dashboard</span>
			</a>
          </li>

      <?PHP if($this->session->userdata('session_user_role') == 1): ?>

		  <?PHP $master_array = array("msatuan", "mmuang", "mnegara", "mgudang", "msupplier", "mcustomer", "mbbaku", "mbjadi", "mbwaste"); ?>
		  <li class="sub-menu <?PHP echo (in_array($this->module_name, $master_array))?'active':''; ?>">
            <a href="javascript:;" class="">
				<i class="icon_table"></i>
				<span>Master</span>
				<span class="menu-arrow arrow_carrot-right"></span>
			</a>
            <ul class="sub">
			  <li class="<?PHP echo ($this->module_name=="msatuan")?'active':''; ?>"><a class="" href="msatuan">Satuan</a></li>
              <li class="<?PHP echo ($this->module_name=="mmuang")?'active':''; ?>"><a class="" href="mmuang">Mata Uang</a></li>
              <li class="<?PHP echo ($this->module_name=="mnegara")?'active':''; ?>"><a class="" href="mnegara">Negara</a></li>
			  <li class="<?PHP echo ($this->module_name=="mgudang")?'active':''; ?>"><a class="" href="mgudang">Gudang</a></li>
			  <li class="<?PHP echo ($this->module_name=="msupplier")?'active':''; ?>"><a class="" href="msupplier">Supplier</a></li>
			  <li class="<?PHP echo ($this->module_name=="mcustomer")?'active':''; ?>"><a class="" href="mcustomer">Customer</a></li>
			  <li class="<?PHP echo ($this->module_name=="mbbaku")?'active':''; ?>"><a class="" href="mbbaku">Bahan Baku</a></li>
			  <li class="<?PHP echo ($this->module_name=="mbjadi")?'active':''; ?>"><a class="" href="mbjadi">Bahan Jadi</a></li>
			  <li class="<?PHP echo ($this->module_name=="mbwaste")?'active':''; ?>"><a class="" href="mbwaste">Barang Waste</a></li>
            </ul>
          </li>
		  
		  <?PHP $trans_array = array("tpemasukanbbaku", "tpengeluaranbbaku", "tpemakaianbbaku", "tpemasukanbjadi", "tpengeluaranbjadi", "tpemakaianbjadi", "tpenyelesaianbwaste"); ?>
		  <li class="sub-menu <?PHP echo (in_array($this->module_name, $trans_array))?'active':''; ?>">
            <a href="javascript:;" class="">
				<i class="icon_documents_alt"></i>
				<span>Transaction</span>
				<span class="menu-arrow arrow_carrot-right"></span>
			</a>
            <ul class="sub">
			       <li class = "<?PHP echo ($this->module_name=="tpemasukanbbaku")?'active':''; ?>"><a class="" href="tpemasukanbbaku">Pemasukan B.Baku</a></li>
              <li class = "<?PHP echo ($this->module_name=="tpengeluaranbbaku")?'active':''; ?>"><a class="" href="tpengeluaranbbaku">Pengeluaran B.Baku</a></li>
              <li class = "<?PHP echo ($this->module_name=="tpemakaianbbaku")?'active':''; ?>"><a class="" href="tpemakaianbbaku">Pemakaian B.Baku</a></li>
              


              <li class = "<?PHP echo ($this->module_name=="tpemasukanbjadi")?'active':''; ?>"><a class="" href="tpemasukanbjadi">Pemasukan H.Produksi</a></li>
              <li class = "<?PHP echo ($this->module_name=="tpengeluaranbjadi")?'active':''; ?>"><a class="" href="tpengeluaranbjadi">Pengeluaran H.Produksi</a></li>
              <li class = "<?PHP echo ($this->module_name=="tpemakaianbjadi")?'active':''; ?>"><a class="" href="tpemakaianbjadi">Pemakaian H.Produksi</a></li>



			  <li class = "<?PHP echo ($this->module_name=="tpenyelesaianbwaste")?'active':''; ?>"><a class="" href="tpenyelesaianbwaste"> Penyelesaian Waste</a></li>
            </ul>
          </li>
		  
      <?PHP endif; ?>

		  <?PHP $report_array = array("rpemasukanbbaku", "rpemakaianbbaku", "rpemasukanbjadi", "rpengeluaranbjadi", "rmutasibbaku", "rmutasibjadi", "rpenyelesaianbwaste"); ?>
		  <li class="sub-menu <?PHP echo (in_array($this->module_name, $report_array))?'active':''; ?>">
            <a href="javascript:;" class="">
				<i class="icon_piechart"></i>
				<span>Report</span>
				<span class="menu-arrow arrow_carrot-right"></span>
			</a>
            <ul class="sub">
			  <li class = "<?PHP echo ($this->module_name=="rpemasukanbbaku")?'active':''; ?>"><a class="" href="rpemasukanbbaku">Pemasukan B.Baku</a></li>
              <li class = "<?PHP echo ($this->module_name=="rpengeluaranbbaku")?'active':''; ?>"><a class="" href="rpengeluaranbbaku">Pengeluaran B.Baku</a></li>
              <li class = "<?PHP echo ($this->module_name=="rpemakaianbbaku")?'active':''; ?>"><a class="" href="rpemakaianbbaku">Pemakaian B.Baku</a></li>

              <li class = "<?PHP echo ($this->module_name=="rpemasukanbjadi")?'active':''; ?>"><a class="" href="rpemasukanbjadi">Pemasukan H.Produksi</a></li>

			  <li class = "<?PHP echo ($this->module_name=="rpengeluaranbjadi")?'active':''; ?>"><a class="" href="rpengeluaranbjadi">Pengeluaran H.Produksi</a></li>
        <li class = "<?PHP echo ($this->module_name=="rpemakaianbjadi")?'active':''; ?>"><a class="" href="rpemakaianbjadi">Pemakaian H.Produksi</a></li>

			  <li class = "<?PHP echo ($this->module_name=="rmutasibbaku")?'active':''; ?>"><a class="" href="rmutasibbaku">Mutasi B.Baku</a></li>
			  <li class = "<?PHP echo ($this->module_name=="rmutasibjadi")?'active':''; ?>"><a class="" href="rmutasibjadi">Mutasi H.Produksi</a></li>
			  <li class = "<?PHP echo ($this->module_name=="rpenyelesaianbwaste")?'active':''; ?>"><a class="" href="rpenyelesaianbwaste"> Penyelesaian Waste</a></li>
            </ul>
          </li>


      <?PHP if($this->session->userdata('session_user_role') == 1): ?>
		  <li class="<?PHP echo ($this->module_name=="msetting")?'active':''; ?>">
			<a class="" href="msetting">
				<i class="icon_tools"></i>
				<span>Setting</span>
			</a>
          </li>
      <?PHP endif; ?>
		  
      <li class="<?PHP echo ($this->module_name=="mlog")?'active':''; ?>">
      <a class="" href="mlog">
        <i class="icon_compass"></i>
        <span>Log</span>
      </a>
          </li>
		  
		  <!--
          <li class="sub-menu">
            <a href="javascript:;" class="">
			<i class="icon_document_alt"></i>
			<span>Forms</span>
			<span class="menu-arrow arrow_carrot-right"></span>
			</a>
            <ul class="sub">
              <li><a class="" href="form_component.html">Form Elements</a></li>
              <li><a class="" href="form_validation.html">Form Validation</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_desktop"></i>
                          <span>UI Fitures</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="general.html">Elements</a></li>
              <li><a class="" href="buttons.html">Buttons</a></li>
              <li><a class="" href="grids.html">Grids</a></li>
            </ul>
          </li>
          <li>
            <a class="" href="widgets.html">
                          <i class="icon_genius"></i>
                          <span>Widgets</span>
                      </a>
          </li>
          <li>
            <a class="" href="chart-chartjs.html">
                          <i class="icon_piechart"></i>
                          <span>Charts</span>

                      </a>

          </li>

          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_table"></i>
                          <span>Tables</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="basic_table.html">Basic Table</a></li>
            </ul>
          </li>

          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_documents_alt"></i>
                          <span>Pages</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="profile.html">Profile</a></li>
              <li><a class="" href="login.html"><span>Login Page</span></a></li>
              <li><a class="" href="blank.html">Blank Page</a></li>
              <li><a class="" href="404.html">404 Error</a></li>
            </ul>
          </li>-->

        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->