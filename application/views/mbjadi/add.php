<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
<script src="<?PHP echo JSPATH; ?>/html5shiv.js"></script>
<script src="<?PHP echo JSPATH; ?>/respond.min.js"></script>
<script src="<?PHP echo JSPATH; ?>/lte-ie7.js"></script>
<![endif]-->
<link href="<?PHP echo VENDORPATH; ?>/select2/select2.min.css" rel="stylesheet">

<body>
  <!-- container section start -->
  <section id="container" class="">
   <?PHP $this->load->view('parts/header_nav'); ?>
   <?PHP $this->load->view('parts/sidebar'); ?>

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-table"></i> <?PHP echo $this->module_title; ?></h3>
            <ol class="breadcrumb">
			  <li><i class="fa fa-home"></i><a href="home/dashboard">Home</a></li>
              <li><i class="fa fa-table"></i><a href="<?PHP echo $this->module_name; ?>"><?PHP echo $this->module_title; ?></a></li>
			  <li><i class="fa fa-file-text-o"></i>Add</li>
            </ol>
          </div>
        </div>
        <!-- Basic Forms & Horizontal Forms-->
		
		<?PHP if(isset($validation) && isset($message)): ?>
			<?PHP if($validation && $message != ""): ?>
			<div class="alert alert-success fade in">
				<strong>Success!</strong> <?PHP echo $message; ?>
			</div>
			<?PHP endif; ?>
			<?PHP if(!$validation && $message != ""): ?>
			<div class="alert alert-block alert-danger fade in">
				<strong>Error!</strong> <?PHP echo $message; ?>
			</div>
			<?PHP endif; ?>
		<?PHP endif; ?>		
		
		<div class="row">
          <div class="col-lg-12">
			 <div class="row">
			  <div class="col-lg-12">
				<section class="panel">
				  <header class="panel-heading">
					Create Form
				  </header>
				  <div class="panel-body">
					<div class="form">
					  <form class="form-validate form-horizontal " id="content_form" method="post" action="<?PHP echo $this->module_name;?>/create_process">
						<div class="form-group ">
						  <label for="kode" class="control-label col-lg-2">Kode <span class="required">*</span></label>
						  <div class="col-lg-10">
							<input class=" form-control" id="kode" name="kode" type="text" value = "<?PHP echo isset($kode)?$kode:''; ?>" />
						  </div>
						</div>
						<div class="form-group ">
						  <label for="nama" class="control-label col-lg-2">Nama <span class="required">*</span></label>
						  <div class="col-lg-10">
							<input class=" form-control" id="nama" name="nama" type="text" value = "<?PHP echo isset($nama)?$nama:''; ?>" />
						  </div>
						</div>
						<div class="form-group ">
						  <label for="satuan" class="control-label col-lg-2">Satuan <span class="required">*</span></label>
						  <div class="col-lg-10">
							<select class=" form-control select2" id="satuan" name="satuan">
							<?PHP foreach($satuan_list as $dl): ?>
							<option value = "<?PHP echo $dl->id; ?>" <?PHP echo isset($satuan)?($satuan==$dl->id?'selected':''):''; ?>><?PHP echo $dl->nama; ?> (<?PHP echo $dl->kode; ?>)</option>
							<?PHP endforeach; ?>
							</select>
						  </div>
						</div>
						<div class="form-group ">
						  <label for="harga" class="control-label col-lg-2">Harga <span class="required">*</span></label>
						  <div class="col-lg-10">
							<input class=" form-control mask-money" id="harga" name="harga" type="text" value = "<?PHP echo isset($harga)?number_format($harga,0,'.',','):''; ?>" />
						  </div>
						</div>
						<div class="form-group">
						  <div class="col-lg-offset-2 col-lg-10">
							<button class="btn btn-primary" type="submit">Save</button>
							<a href="<?PHP echo $this->module_name; ?>" class="btn btn-default">Cancel</a>
						  </div>
						</div>
					  </form>
					</div>
				  </div>
				</section>
			  </div>
			</div>
          </div>
        </div>
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
    <?PHP $this->load->view('parts/inner_footer'); ?>
  </section>
  <!-- container section end -->

	<!-- javascripts -->
	<script src="<?PHP echo JSPATH; ?>/jquery.js"></script>
	<script src="<?PHP echo JSPATH; ?>/bootstrap.min.js"></script>
	<!-- nicescroll -->
	<script src="<?PHP echo JSPATH; ?>/jquery.scrollTo.min.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery.nicescroll.js" type="text/javascript"></script>

	<!-- jquery validate js -->
	<script type="text/javascript" src="<?PHP echo JSPATH; ?>/jquery.validate.min.js"></script>
  
	<!-- jquery ui -->
	<script src="<?PHP echo JSPATH; ?>/jquery-ui-1.9.2.custom.min.js"></script>

	<!-- custome script for all page -->
	<script src="<?PHP echo JSPATH; ?>/scripts.js"></script>

	<script src="<?PHP echo VENDORPATH; ?>/select2/select2.min.js" type="text/javascript"></script>
	
    <script src="<?PHP echo JSPATH; ?>/jquery.maskMoney.js"></script>

	<script>
	$().ready(function() {
		$(".mask-money").maskMoney({precision:0,thousands:',', allowZero: true});
		$("#content_form").validate({
			rules: {
				kode: {
					required: true,
					minlength: 2
				},
				nama: {
					required: true,
					minlength: 4
				},
				satuan: {
					required: true
				},
				harga: {
					required: true
				}
			}
		});
		$(".select2").select2();
	});

	
	</script>
</body>