<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
<script src="<?PHP echo JSPATH; ?>/html5shiv.js"></script>
<script src="<?PHP echo JSPATH; ?>/respond.min.js"></script>
<script src="<?PHP echo JSPATH; ?>/lte-ie7.js"></script>
<![endif]-->
<link href="<?PHP echo VENDORPATH; ?>/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
<style>
.ui-datepicker td a {
	text-align:center;
}
</style>
<body>
  <!-- container section start -->
  <section id="container" class="">
   <?PHP $this->load->view('parts/header_nav'); ?>
   <?PHP $this->load->view('parts/sidebar'); ?>

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-table"></i> <?PHP echo $this->module_title; ?></h3>
            <ol class="breadcrumb">
			  <li><i class="fa fa-home"></i><a href="home/dashboard">Home</a></li>
              <li><i class="fa fa-table"></i><a href="<?PHP echo $this->module_name; ?>"><?PHP echo $this->module_title; ?></a></li>
            </ol>
          </div>
        </div>
        <!-- Basic Forms & Horizontal Forms-->
		
		<?PHP if(isset($validation) && isset($message)): ?>
			<?PHP if($validation && $message != ""): ?>
			<div class="alert alert-success fade in">
				<strong>Success!</strong> <?PHP echo $message; ?>
			</div>
			<?PHP endif; ?>
			<?PHP if(!$validation && $message != ""): ?>
			<div class="alert alert-block alert-danger fade in">
				<strong>Error!</strong> <?PHP echo $message; ?>
			</div>
			<?PHP endif; ?>
		<?PHP endif; ?>		
		
		<div class="row">
          <div class="col-lg-6">
			 <div class="row">
			  <div class="col-lg-12">
				<section class="panel">
				  <header class="panel-heading">
					Filter
				  </header>
				  <div class="panel-body">
					<div class="form">
					  <form class="form-validate form-horizontal " id="content_form" method="post" action="<?PHP echo $this->module_name;?>/generate_process">
						<div class="form-group ">
						  <label for="from_date" class="control-label col-lg-2">From <span class="required">*</span></label>
						  <div class="col-lg-10">
							<input class=" form-control datepicker" id="from_date" name="from_date" type="text" value = "<?PHP echo isset($from_date)?$from_date:''; ?>" />
						  </div>
						</div>
						<div class="form-group ">
						  <label for="to_date" class="control-label col-lg-2">To <span class="required">*</span></label>
						  <div class="col-lg-10">
							<input class=" form-control datepicker" id="to_date" name="to_date" type="text" value = "<?PHP echo isset($to_date)?$to_date:''; ?>" />
						  </div>
						</div>
						<div class="form-group">
						  <div class="col-lg-offset-2 col-lg-10">
							<button class="btn btn-primary" type="submit">Generate</button>
							<a href="<?PHP echo $this->module_name; ?>" class="btn btn-default">Cancel</a>
						  </div>
						</div>
					  </form>
					</div>
				  </div>
				</section>
			  </div>
			</div>
          </div>
        </div>
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
    <?PHP $this->load->view('parts/inner_footer'); ?>
  </section>
  <!-- container section end -->

	<!-- javascripts -->
	<script src="<?PHP echo JSPATH; ?>/jquery.js"></script>
	<script src="<?PHP echo JSPATH; ?>/bootstrap.min.js"></script>
	<!-- nicescroll -->
	<script src="<?PHP echo JSPATH; ?>/jquery.scrollTo.min.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery.nicescroll.js" type="text/javascript"></script>

	<!-- jquery validate js -->
	<script type="text/javascript" src="<?PHP echo JSPATH; ?>/jquery.validate.min.js"></script>
  
	<!-- jquery ui -->
	<!--<script src="<?PHP echo JSPATH; ?>/jquery-ui-1.9.2.custom.min.js"></script>-->
	<script src="<?PHP echo VENDORPATH; ?>/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>

	<!-- custome script for all page -->
	<script src="<?PHP echo JSPATH; ?>/scripts.js"></script>

	<script>
	$().ready(function() {
		$("#content_form").validate({
			rules: {
				from_date: {
					required: true
				},
				to_date: {
					required: true
				}
			}
		});
		$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
	});

	
	</script>
</body>