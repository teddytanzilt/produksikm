<body class="login-img3-body">
	<div class="container">
		<form class="login-form" action="home/login_process" method = "POST">
			<div class="login-wrap">
				<p class="login-img"><i class="icon_lock_alt"></i></p>
				
				<?PHP if(isset($validation) && isset($message)): ?>
					<?PHP if($validation && $message != ""): ?>
					<div class="alert alert-success fade in">
						<strong>Success!</strong> <?PHP echo $message; ?>
					</div>
					<?PHP endif; ?>
					<?PHP if(!$validation && $message != ""): ?>
					<div class="alert alert-block alert-danger fade in">
						<strong>Error!</strong> <?PHP echo $message; ?>
					</div>
					<?PHP endif; ?>
				<?PHP endif; ?>
				
				<div class="input-group">
					<span class="input-group-addon"><i class="icon_profile"></i></span>
					<input type="text" class="form-control" placeholder="Username" name = "user_name" value = "<?PHP echo isset($user_name)?$user_name:''; ?>" autofocus>
				</div>
				<div class="input-group">
					<span class="input-group-addon"><i class="icon_key_alt"></i></span>
					<input type="password" class="form-control" placeholder="Password" name = "password">
				</div>
				<!--<label class="checkbox">
					<input type="checkbox" value="remember-me"> Remember me
					<span class="pull-right"> <a href="#"> Forgot Password?</a></span>
				</label>-->
				<button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
			</div>
		</form>
		<div class="text-right">
		</div>
	</div>
</body>
