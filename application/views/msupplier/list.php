<!-- Datatables -->
<link href="<?PHP echo VENDORPATH; ?>/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?PHP echo VENDORPATH; ?>/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?PHP echo VENDORPATH; ?>/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?PHP echo VENDORPATH; ?>/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?PHP echo VENDORPATH; ?>/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<style>
.dt-button-container{
	padding:10px;
}
.dt-table-container{
	padding-left:10px;
	padding-right:10px;
}
.dt-paging-container{
	padding:10px;
}
table.dataTable thead .sorting { background: url('<?PHP echo VENDORPATH; ?>/datatables.net/images/sort_both.png') no-repeat center right; }
table.dataTable thead .sorting_asc { background: url('<?PHP echo VENDORPATH; ?>/datatables.net/images/sort_asc.png') no-repeat center right; }
table.dataTable thead .sorting_desc { background: url('<?PHP echo VENDORPATH; ?>/datatables.net/images/sort_desc.png') no-repeat center right; }

table.dataTable thead .sorting_asc_disabled { background: url('<?PHP echo VENDORPATH; ?>/datatables.net/images/sort_asc_disabled.png') no-repeat center right; }
table.dataTable thead .sorting_desc_disabled { background: url('<?PHP echo VENDORPATH; ?>/datatables.net/images/sort_desc_disabled.png') no-repeat center right; }

table.dataTable thead .sorting:after,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_desc_disabled:after
{
  content: none;
}
</style>
<body>
  <!-- container section start -->
  <section id="container" class="">
   <?PHP $this->load->view('parts/header_nav'); ?>
   <?PHP $this->load->view('parts/sidebar'); ?>
	
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-table"></i> <?PHP echo $this->module_title; ?></h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="home/dashboard">Home</a></li>
              <li><i class="fa fa-table"></i><?PHP echo $this->module_title; ?></li>
            </ol>
          </div>
        </div>
        <!-- page start-->
        <div class="row">
          <div class="col-sm-12">
            <section class="panel">
              <header class="panel-heading">
                Active List
              </header>
			  
			  <?PHP if(isset($validation) && isset($message)): ?>
				<?PHP if(!$validation && $message != ""): ?>
				<div class="alert alert-block alert-danger fade in">
					<strong>Error!</strong> <?PHP echo $message; ?>
				</div>
				<?PHP endif; ?>
			<?PHP endif; ?>
			
			<?PHP if($this->session->flashdata('success_message')!=""): ?>
			<div class="alert alert-success fade in">
				<strong>Success!</strong> <?PHP echo $this->session->flashdata('success_message'); ?>
			</div>
			<?PHP endif; ?>
			
			  <table id="datatable-content" class="table table-striped table-bordered">
				<thead>
					<tr>
					<th>Id</th>
					<th>Kode</th>
					<th>Nama</th>
					<th>Alamat</th>
					<th>Action</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			  </table>
            </section>
          </div>
        </div>
       
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
    <?PHP $this->load->view('parts/inner_footer'); ?>
  </section>
  <!-- container section end -->

   <!-- Modal -->
	<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Modal Tittle</h4>
		  </div>
		  <div class="modal-body">
			Body goes here...
		  </div>
		  <div class="modal-footer">
			<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
			<button class="btn btn-warning" type="button" id = "btn-confirm" data-id = ""> Confirm</button>
		  </div>
		</div>
	  </div>
	</div>
	<!-- modal -->
	
	
	<!-- javascripts -->
	<script src="<?PHP echo JSPATH; ?>/jquery.js"></script>
	<script src="<?PHP echo JSPATH; ?>/bootstrap.min.js"></script>
	<!-- nicescroll -->
	<script src="<?PHP echo JSPATH; ?>/jquery.scrollTo.min.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery.nicescroll.js" type="text/javascript"></script>
	<!--custome script for all page-->
	<script src="<?PHP echo JSPATH; ?>/scripts.js"></script>
	<!-- Datatables -->
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/jszip/dist/jszip.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/pdfmake/build/vfs_fonts.js"></script>
	
	<script type="text/javascript">
    var handleDataTableButtons = function() {
          if ($("#datatable-content").length) {
          $("#datatable-content").DataTable({
            processing: true,
            serverSide: true,
            deferRender: true,
            keys: true,
            order: [[ 0, 'asc' ]],
            /*columnDefs: [
              { orderable: false, targets: [0] }
            ],*/
			"columnDefs": [ {
				"targets": -1,
				"data": null,
				/*"defaultContent": "<a href=''>View</a>"*/
				"render": function ( data, type, row ) {
					return "<a href='<?PHP echo $this->module_name; ?>/edit/" + row[0] + "'>Edit</a>&nbsp;&nbsp;&nbsp;<a href = '#' data-id = '" + row[0] + "' data-name = '" + row[1] + "' class = 'dt_remove'>Delete</a>";
                },
			},
			{ "orderable": false, "targets": -1 }					
			],
            ajax : ( {
              url: "<?PHP echo $this->module_name; ?>/get_data_list_active_json",
              type: "POST",
            }),

            //dom: '<lfr<B><t>ip>',
            dom: '<<lfr><"dt-button-container"B><"dt-table-container"t>i<"dt-paging-container"p>>',
            buttons: [
            {
              extend: "copy",
              className: "btn-sm",
			  exportOptions: {
				columns: ':not(:last-child)', /*columns: [0, 1, 2], */
			  }
            },
            {
              extend: "csv",
              className: "btn-sm",
			  exportOptions: {
				columns: ':not(:last-child)', /*columns: [0, 1, 2], */
			  }
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm",
			  exportOptions: {
				columns: ':not(:last-child)', /*columns: [0, 1, 2], */
			  }
            },
            {
              extend: "print",
              className: "btn-sm",
			  exportOptions: {
				columns: ':not(:last-child)', /*columns: [0, 1, 2], */
			  }
            },
            {
                text: 'Add',
                className: "btn-sm",
                action: function ( e, dt, node, config ) {
                    window.location = "<?PHP echo base_url(); ?><?PHP echo $this->module_name; ?>/create"
                }
            }
            ],
            responsive: true,
          });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
          init: function() {
            handleDataTableButtons();
          }
          };
        }();
        TableManageButtons.init();
		
		$('#datatable-content').on('click', 'a.dt_remove', function (e) {
			e.preventDefault();
			$("#confirmModal .modal-title").html("Confirmation");
			$("#confirmModal .modal-body").html("Are you sure you want to delete " + $(this).attr("data-name") + "?");
			$("#confirmModal #btn-confirm").attr('data-id', $(this).attr("data-id"));
			$('#confirmModal').modal({
				show: 'true'
			}); 
		} );
		
		$('#confirmModal').on('click', '#btn-confirm', function (e) {
			var id = $(this).attr("data-id");
			window.location = "<?PHP echo base_url(); ?><?PHP echo $this->module_name; ?>/delete_process/" + id;
		});
    </script>
</body>