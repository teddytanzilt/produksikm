<!-- full calendar css-->
<link href="<?PHP echo VENDORPATH; ?>/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
<link href="<?PHP echo VENDORPATH; ?>/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
<!-- easy pie chart-->
<link href="<?PHP echo VENDORPATH; ?>/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
<!-- owl carousel -->
<link rel="stylesheet" href="<?PHP echo CSSPATH; ?>/owl.carousel.css" type="text/css">
<link href="<?PHP echo CSSPATH; ?>/jquery-jvectormap-1.2.2.css" rel="stylesheet">
<!-- Custom styles -->
<link rel="stylesheet" href="<?PHP echo CSSPATH; ?>/fullcalendar.css">
<link href="<?PHP echo CSSPATH; ?>/widgets.css" rel="stylesheet">
<link href="<?PHP echo CSSPATH; ?>/xcharts.min.css" rel=" stylesheet">
<link href="<?PHP echo CSSPATH; ?>/jquery-ui-1.10.4.min.css" rel="stylesheet">
<body>
  <!-- container section start -->
  <section id="container" class="">
    <?PHP $this->load->view('parts/header_nav'); ?>
    <?PHP $this->load->view('parts/sidebar'); ?>

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
              <li><i class="fa fa-laptop"></i>Dashboard</li>
            </ol>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box blue-bg">
              <i class="fa fa-plus"></i>
              <div class="count"><?PHP echo ($total_jumlah_masuk_bbaku!="")?$total_jumlah_masuk_bbaku:"0"; ?></div>
              <div class="title">Bahan Baku</div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box brown-bg">
              <i class="fa fa-minus"></i>
              <div class="count"><?PHP echo ($total_jumlah_keluar_bbaku!="")?$total_jumlah_keluar_bbaku:"0"; ?></div>
              <div class="title">Bahan Baku</div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box dark-bg">
              <i class="fa fa-plus"></i>
              <div class="count"><?PHP echo ($total_jumlah_masuk_bjadi!="")?$total_jumlah_masuk_bjadi:"0"; ?></div>
              <div class="title">Barang Jadi</div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box green-bg">
              <i class="fa fa-minus"></i>
              <div class="count"><?PHP echo ($total_jumlah_keluar_bjadi!="")?$total_jumlah_keluar_bjadi:"0"; ?></div>
              <div class="title">Barang Jadi</div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

        </div>
        <!--/.row-->


      </section>
      <?PHP $this->load->view('parts/inner_footer'); ?>
    </section>
    <!--main content end-->
  </section>
  <!-- container section start -->
	
	<!-- javascripts -->
	<script src="<?PHP echo JSPATH; ?>/jquery.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery-ui-1.10.4.min.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
	<!-- bootstrap -->
	<script src="<?PHP echo JSPATH; ?>/bootstrap.min.js"></script>
	<!-- nice scroll -->
	<script src="<?PHP echo JSPATH; ?>/jquery.scrollTo.min.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery.nicescroll.js" type="text/javascript"></script>
	<!-- charts scripts -->
	<script src="<?PHP echo VENDORPATH; ?>/jquery-knob/js/jquery.knob.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery.sparkline.js" type="text/javascript"></script>
	<script src="<?PHP echo VENDORPATH; ?>/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
	<script src="<?PHP echo JSPATH; ?>/owl.carousel.js"></script>
	<!-- jQuery full calendar -->
	<<script src="<?PHP echo JSPATH; ?>/fullcalendar.min.js"></script>
	<!-- Full Google Calendar - Calendar -->
	<script src="<?PHP echo VENDORPATH; ?>/fullcalendar/fullcalendar/fullcalendar.js"></script>
	<!--script for this page only-->
	<script src="<?PHP echo JSPATH; ?>/calendar-custom.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery.rateit.min.js"></script>
	<!-- custom select -->
	<script src="<?PHP echo JSPATH; ?>/jquery.customSelect.min.js"></script>
	<script src="<?PHP echo VENDORPATH; ?>/chart-master/Chart.js"></script>

	<!--custome script for all page-->
	<script src="<?PHP echo JSPATH; ?>/scripts.js"></script>
	<!-- custom script for this page-->
	<script src="<?PHP echo JSPATH; ?>/sparkline-chart.js"></script>
	<script src="<?PHP echo JSPATH; ?>/easy-pie-chart.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery-jvectormap-world-mill-en.js"></script>
	<script src="<?PHP echo JSPATH; ?>/xcharts.min.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery.autosize.min.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery.placeholder.min.js"></script>
	<script src="<?PHP echo JSPATH; ?>/gdp-data.js"></script>
	<script src="<?PHP echo JSPATH; ?>/morris.min.js"></script>
	<script src="<?PHP echo JSPATH; ?>/sparklines.js"></script>
	<script src="<?PHP echo JSPATH; ?>/charts.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery.slimscroll.min.js"></script>
	
	
	
	<style>
	.ui-menu{
		z-index:1003;
	}
	.ui-menu .ui-menu-item a
	{
		display: block;
		padding: 3px 3px 3px 3px;
		text-decoration: none;
		cursor: pointer;
		/*background-color: #FFFFFF;*/
	}
	</style>
	
	
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script>
	$( function() {
		$( "#global-search" ).autocomplete({
			source : function(request, response) {
				$.ajax({
					url : "<?PHP echo base_url(); ?>home/getSearchResult",
					dataType : "json",
					method : 'POST',
					data : {
						match : $( "#global-search" ).val()
					},
					success : function(data) {
						response($.map(data, function(item) {
							return {
								label : item.no_transaksi,
								value : item.id
							}
						}));
					}
				});
			},
			minLength : 4,
			open: function( event, ui ) {
				//$( "#global-search").val("");
			},
			select : function(event, ui) {
				 event.preventDefault();
				//var location_value = ui.item.value;
				//var location_array = location_value.split("|"); 
				//$("#SearchCityGoId").val(location_array[0]);
				//$("#SearchAreaGoId").val(location_array[1]);
				//$("#SearchHotelGoId").val(location_array[2]);
				
				$( "#global-search").val(ui.item.label);
				//$(this).val(ui.item.label);
			},
			close: function( event, ui ) {
				 event.preventDefault();
				 $( "#global-search").blur();
			},
			change: function( event, ui ) {
				//event.preventDefault();
			},
			focus: function( event, ui ) {
				event.preventDefault();
				$( "#global-search").val(ui.item.label);
			}
		});
	});
  </script>
    <script>
      //knob
      $(function() {
        $(".knob").knob({
          'draw': function() {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
        $("#owl-slider").owlCarousel({
          navigation: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true

        });
      });

      //custom select box

      $(function() {
        $('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
        $('#map').vectorMap({
          map: 'world_mill_en',
          series: {
            regions: [{
              values: gdpData,
              scale: ['#000', '#000'],
              normalizeFunction: 'polynomial'
            }]
          },
          backgroundColor: '#eef3f7',
          onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });
    </script>

</body>