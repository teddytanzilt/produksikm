<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
<script src="<?PHP echo JSPATH; ?>/html5shiv.js"></script>
<script src="<?PHP echo JSPATH; ?>/respond.min.js"></script>
<script src="<?PHP echo JSPATH; ?>/lte-ie7.js"></script>
<![endif]-->
<link href="<?PHP echo VENDORPATH; ?>/select2/select2.min.css" rel="stylesheet">
<link href="<?PHP echo VENDORPATH; ?>/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
<style>
.ui-datepicker td a {
	text-align:center;
}
</style>
<body>
  <!-- container section start -->
  <section id="container" class="">
   <?PHP $this->load->view('parts/header_nav'); ?>
   <?PHP $this->load->view('parts/sidebar'); ?>

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-table"></i> <?PHP echo $this->module_title; ?></h3>
            <ol class="breadcrumb">
			  <li><i class="fa fa-home"></i><a href="home/dashboard">Home</a></li>
              <li><i class="fa fa-table"></i><a href="<?PHP echo $this->module_name; ?>"><?PHP echo $this->module_title; ?></a></li>
			  <li><i class="fa fa-file-text-o"></i>Add</li>
            </ol>
          </div>
        </div>
        <!-- Basic Forms & Horizontal Forms-->
		
		<?PHP if(isset($validation) && isset($message)): ?>
			<?PHP if($validation && $message != ""): ?>
			<div class="alert alert-success fade in">
				<strong>Success!</strong> <?PHP echo $message; ?>
			</div>
			<?PHP endif; ?>
			<?PHP if(!$validation && $message != ""): ?>
			<div class="alert alert-block alert-danger fade in">
				<strong>Error!</strong> <?PHP echo $message; ?>
			</div>
			<?PHP endif; ?>
		<?PHP endif; ?>		
		
		<div class="row">
          <div class="col-lg-12">
			 <div class="row">
			  <div class="col-lg-12">
				<section class="panel">
				  <header class="panel-heading">
					Create Form
				  </header>
				  <div class="panel-body">
					<div class="form">
					  <form class="form-validate form-horizontal " id="content_form" method="post" action="<?PHP echo $this->module_name;?>/create_process">
					  	<div class="form-group ">
						  <label for="tanggal" class="control-label col-lg-2">Tanggal Transaksi<span class="required">*</span></label>
						  <div class="col-lg-10">
							<input class=" form-control datepicker" id="tanggal" name="tanggal" type="text" value = "<?PHP echo isset($tanggal)?$tanggal:''; ?>" />
						  </div>
						</div>
						<div class="form-group ">
						  <label for="no_bukti_penerimaan" class="control-label col-lg-2">No Bukti Penerimaan <span class="required">*</span></label>
						  <div class="col-lg-10">
							<input class=" form-control" id="no_bukti_penerimaan" name="no_bukti_penerimaan" type="text" value = "<?PHP echo isset($no_bukti_penerimaan)?$no_bukti_penerimaan:''; ?>" />
						  </div>
						</div>
						<div class="form-group ">
						  <label for="tanggal_bukti_penerimaan" class="control-label col-lg-2">Tanggal Bukti Penerimaan <span class="required">*</span></label>
						  <div class="col-lg-10">
							<input class=" form-control datepicker" id="tanggal_bukti_penerimaan" name="tanggal_bukti_penerimaan" type="text" value = "<?PHP echo isset($tanggal_bukti_penerimaan)?$tanggal_bukti_penerimaan:''; ?>" />
						  </div>
						</div>
						<div class="form-group ">
						  <label for="barang_jadi" class="control-label col-lg-2">Barang Jadi <span class="required">*</span></label>
						  <div class="col-lg-10">
							<select class=" form-control select2" id="barang_jadi" name="barang_jadi">
							<?PHP foreach($barang_jadi_list as $dl): ?>
							<option value = "<?PHP echo $dl->id; ?>" <?PHP echo isset($barang_jadi)?($barang_jadi==$dl->id?'selected':''):''; ?>><?PHP echo $dl->nama; ?> (<?PHP echo $dl->kode; ?>)</option>
							<?PHP endforeach; ?>
							</select>
						  </div>
						</div>
						<div class="form-group ">
						  <label for="jumlah" class="control-label col-lg-2">Jumlah <span class="required">*</span></label>
						  <div class="col-lg-10">
							<input class=" form-control" id="jumlah" name="jumlah" type="number" value = "<?PHP echo isset($jumlah)?$jumlah:''; ?>" />
						  </div>
						</div>
						<div class="form-group ">
						  <label for="gudang" class="control-label col-lg-2">Gudang <span class="required">*</span></label>
						  <div class="col-lg-10">
							<select class=" form-control select2" id="gudang" name="gudang">
							<?PHP foreach($gudang_list as $dl): ?>
							<option value = "<?PHP echo $dl->id; ?>" <?PHP echo isset($gudang)?($gudang==$dl->id?'selected':''):''; ?>><?PHP echo $dl->nama; ?> (<?PHP echo $dl->kode; ?>)</option>
							<?PHP endforeach; ?>
							</select>
						  </div>
						</div>
						<div class="form-group ">
						  <label for="no_pemakaian_bahan_baku" class="control-label col-lg-2">No Pemakaian Bahan Baku <span class="required">*</span></label>
						  <div class="col-lg-10">
							<select multiple class=" form-control" id="no_pemakaian_bahan_baku" name="no_pemakaian_bahan_baku[]">
							<?PHP foreach($pemakaian_bahan_baku_list as $dl): ?>
							<option value = "<?PHP echo $dl->no_transaksi; ?>" <?PHP echo isset($no_pemakaian_bahan_baku)?((strpos($no_pemakaian_bahan_baku, $dl->no_transaksi) !== false)?'selected':''):''; ?>><?PHP echo $dl->no_transaksi; ?></option>
							<?PHP endforeach; ?>
							</select>
						  </div>
						</div>
						<div class="form-group">
						  <div class="col-lg-offset-2 col-lg-10">
							<button class="btn btn-primary" type="submit">Save</button>
							<a href="<?PHP echo $this->module_name; ?>" class="btn btn-default">Cancel</a>
						  </div>
						</div>
					  </form>
					</div>
				  </div>
				</section>
			  </div>
			</div>
          </div>
        </div>
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
    <?PHP $this->load->view('parts/inner_footer'); ?>
  </section>
  <!-- container section end -->

	<!-- javascripts -->
	<script src="<?PHP echo JSPATH; ?>/jquery.js"></script>
	<script src="<?PHP echo JSPATH; ?>/bootstrap.min.js"></script>
	<!-- nicescroll -->
	<script src="<?PHP echo JSPATH; ?>/jquery.scrollTo.min.js"></script>
	<script src="<?PHP echo JSPATH; ?>/jquery.nicescroll.js" type="text/javascript"></script>

	<!-- jquery validate js -->
	<script type="text/javascript" src="<?PHP echo JSPATH; ?>/jquery.validate.min.js"></script>
  
	<!-- jquery ui -->
	<!--<script src="<?PHP echo JSPATH; ?>/jquery-ui-1.9.2.custom.min.js"></script>-->
	<script src="<?PHP echo VENDORPATH; ?>/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
	
	<!-- custome script for all page -->
	<script src="<?PHP echo JSPATH; ?>/scripts.js"></script>

	<script src="<?PHP echo VENDORPATH; ?>/select2/select2.min.js" type="text/javascript"></script>
	
	<script>
	$().ready(function() {
		$("#content_form").validate({
			rules: {
				kode: {
					jenis_dokumen: true
				},
				no_dokumen_pabean: {
					required: true
				},
				tanggal_dokumen_pabean: {
					required: true
				},
				no_seri_barang: {
					required: true
				},
				no_bukti_penerimaan_barang: {
					required: true
				},
				jumlah: {
					required: true
				},
				nilai_barang: {
					required: true
				}
			}
		});
		$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
		$(".select2").select2();
	});

	
	</script>
</body>