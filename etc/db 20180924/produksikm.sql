-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2018 at 06:50 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `produksikm`
--

-- --------------------------------------------------------

--
-- Table structure for table `core_app_config`
--

CREATE TABLE IF NOT EXISTS `core_app_config` (
  `id` int(10) NOT NULL,
  `config_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `config_value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `core_app_config`
--

INSERT INTO `core_app_config` (`id`, `config_key`, `config_value`) VALUES
(1, 'WEB_NAME', 'Administrasi Kilang Mie Gunung Mas'),
(2, 'COMPANY_NAME', 'Kilang Mie Gunung Mas');

-- --------------------------------------------------------

--
-- Table structure for table `core_app_log`
--

CREATE TABLE IF NOT EXISTS `core_app_log` (
  `id` int(10) NOT NULL,
  `ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `forwarded_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `accept_language` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `device_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `event` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `input_data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `json_return` text CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=450 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `core_app_log`
--

INSERT INTO `core_app_log` (`id`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `device_id`, `user_id`, `event`, `message`, `module`, `action`, `link`, `created_on`, `input_data`, `json_return`) VALUES
(1, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-10 00:47:15', '{"user_name":"admin","password":"12345678"}', '[]'),
(2, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-10 00:49:22', '{"user_name":"admin","password":"12345678"}', '[]'),
(3, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-10 00:49:26', '{"user_name":"admin","password":"12345678"}', '[]'),
(4, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-10 00:49:48', '{"user_name":"admin","password":"12345678"}', '[]'),
(5, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost:81/ProduksiKM/index.php/home/logout_process', '2018-03-10 00:53:09', '[]', '[]'),
(6, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-10 00:53:16', '{"user_name":"admin","password":"12345678"}', '[]'),
(7, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-11 21:28:27', '{"user_name":"admin","password":"12345678"}', '[]'),
(8, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost:81/ProduksiKM/index.php/home/logout_process', '2018-03-11 23:44:23', '[]', '[]'),
(9, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-11 23:44:37', '{"user_name":"admin","password":"12345678"}', '[]'),
(10, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Failed - ', 'msatuan', 'create_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/create_process', '2018-03-12 06:31:41', '{"kode":"GR","nama":"Gramss"}', 'null'),
(11, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'msatuan', 'create_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/create_process', '2018-03-12 06:39:02', '{"kode":"GR","nama":"Gramss"}', '[]'),
(12, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'edit_process', 'Success', 'msatuan', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/edit_process', '2018-03-12 06:48:18', '{"kode":"MG","nama":"Miligram"}', '[]'),
(13, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'edit_process', 'Success', 'msatuan', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/edit_process', '2018-03-12 06:49:35', '{"id":"3","kode":"MG","nama":"Miligram"}', '[]'),
(14, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'edit_process', 'Success', 'msatuan', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/edit_process', '2018-03-12 06:49:50', '{"id":"2","kode":"GR","nama":"Gram"}', '[]'),
(15, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'msatuan', 'create_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/create_process', '2018-03-12 06:50:16', '{"kode":"XXX","nama":"XXXXX"}', '[]'),
(16, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'delete_process', 'Success', 'msatuan', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/4', '2018-03-12 06:58:27', '[]', '[]'),
(17, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'delete_process', 'Success', 'msatuan', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/4', '2018-03-12 06:59:30', '[]', '[]'),
(18, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'delete_process', 'Success', 'msatuan', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/3', '2018-03-12 07:00:33', '[]', '[]'),
(19, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'msatuan', 'create_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/create_process', '2018-03-12 07:00:46', '{"kode":"asdasdasdasd","nama":"asdasd"}', '[]'),
(20, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-12 20:05:35', '{"user_name":"admin","password":"12345678"}', '[]'),
(21, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'delete_process', 'Success', 'msatuan', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/5', '2018-03-12 20:17:29', '[]', '[]'),
(22, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'delete_process', 'Success', 'msatuan', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/1', '2018-03-12 21:05:19', '[]', '[]'),
(23, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'delete_process', 'Success', 'msatuan', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/5', '2018-03-12 21:42:15', '[]', '[]'),
(24, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'delete_process', 'Success', 'msatuan', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/4', '2018-03-12 21:42:25', '[]', '[]'),
(25, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'mmuang', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mmuang/create_process', '2018-03-12 21:49:32', '{"kode":"US$","nama":"US Dollar"}', '[]'),
(26, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'edit_process', 'Success', 'mmuang', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/mmuang/edit_process', '2018-03-12 21:49:38', '{"id":"2","kode":"USD","nama":"US Dollar"}', '[]'),
(27, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'mnegara', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mnegara/create_process', '2018-03-12 21:52:59', '{"kode":"SG","nama":"Singapore"}', '[]'),
(28, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'mnegara', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mnegara/create_process', '2018-03-12 21:53:11', '{"kode":"MY","nama":"Malaysias"}', '[]'),
(29, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'edit_process', 'Success', 'mnegara', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/mnegara/edit_process', '2018-03-12 21:53:15', '{"id":"3","kode":"MY","nama":"Malaysia"}', '[]'),
(30, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'mgudang', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mgudang/create_process', '2018-03-12 21:55:58', '{"kode":"GD1","nama":"Gudang 1"}', '[]'),
(31, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'edit_process', 'Success', 'mgudang', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/mgudang/edit_process', '2018-03-12 21:56:02', '{"id":"1","kode":"GD1","nama":"Gudang 12"}', '[]'),
(32, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'edit_process', 'Success', 'mgudang', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/mgudang/edit_process', '2018-03-12 21:56:06', '{"id":"1","kode":"GD1","nama":"Gudang 1"}', '[]'),
(33, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost:81/ProduksiKM/index.php/home/logout_process', '2018-03-12 22:12:24', '[]', '[]'),
(34, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-12 22:12:29', '{"user_name":"admin","password":"12345678"}', '[]'),
(35, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost:81/ProduksiKM/index.php/home/logout_process', '2018-03-12 22:44:26', '[]', '[]'),
(36, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-12 22:44:33', '{"user_name":"admin","password":"12345678"}', '[]'),
(37, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'msupplier', 'create_process', 'http://localhost:81/ProduksiKM/index.php/msupplier/create_process', '2018-03-12 23:08:51', '{"kode":"SP1","nama":"Supplier 1","alamat":"Jalan Raden Saleh Dalam No 1","telp":"061 4532658","fax":"061 4553976"}', '[]'),
(38, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'edit_process', 'Success', 'msupplier', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/msupplier/edit_process', '2018-03-12 23:11:28', '{"id":"1","kode":"KMTJ","nama":"Kilang Mie Tj Morawa","alamat":"Jalan Tanjung Morawa Km..","telp":"061 1111111","fax":"061 2222"}', '[]'),
(39, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'delete_process', 'Success', 'msupplier', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/msupplier/delete_process/2', '2018-03-12 23:11:34', '[]', '[]'),
(40, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'mcustomer', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mcustomer/create_process', '2018-03-12 23:14:53', '{"kode":"TDY","nama":"Teddy","alamat":"Jalan Raden Saleh Dalam no 85","telp":"08982874427","fax":"081360149004"}', '[]'),
(41, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mbbaku/create_process', '2018-03-13 00:47:54', '{"kode":"TRG","nama":"Terigu","satuan":"2","harga":"100000"}', '[]'),
(42, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'edit_process', 'Success', 'mbbaku', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/mbbaku/edit_process', '2018-03-13 00:52:45', '{"id":"2","kode":"TRG","nama":"Terigu","satuan":"3","harga":"100000"}', '[]'),
(43, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mbbaku/create_process', '2018-03-13 00:59:33', '{"kode":"TPG","nama":"Tepung","satuan":"2","harga":"5000"}', '[]'),
(44, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mbjadi/create_process', '2018-03-13 01:03:13', '{"kode":"BHN1","nama":"Bihun 1","satuan":"1","harga":"222"}', '[]'),
(45, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'create_process', 'Success', 'mbwaste', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mbwaste/create_process', '2018-03-13 01:06:59', '{"kode":"KRG","nama":"Karung","satuan":"2","harga":"2000"}', '[]'),
(46, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-14 00:40:02', '{"user_name":"admin","password":"12345678"}', '[]'),
(47, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', '', '', 1, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost:81/ProduksiKM/index.php/home/logout_process', '2018-03-14 01:55:35', '[]', '[]'),
(48, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-18 23:25:44', '{"user_name":"admin","password":"12345678"}', '[]'),
(49, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-19 03:45:53', '{"user_name":"admin","password":"12345678"}', '[]'),
(50, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-19 19:59:20', '{"user_name":"admin","password":"12345678"}', '[]'),
(51, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-19 20:47:01', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000001","no_bukti_penerimaan_barang":"821498","bahan_baku":"1","jumlah":"20","mata_uang":"1","nilai_barang":"200000","gudang":"1","negara_asal_barang":"1"}', '[]'),
(52, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-19 20:48:56', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000001","no_bukti_penerimaan_barang":"821498","bahan_baku":"1","jumlah":"20","mata_uang":"1","nilai_barang":"200000","gudang":"1","negara_asal_barang":"1"}', '[]'),
(53, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mbbaku/create_process', '2018-03-19 20:52:08', '{"kode":"TRG","nama":"Terigu","satuan":"1","harga":"10000"}', '[]'),
(54, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mbbaku/create_process', '2018-03-19 20:52:24', '{"kode":"GRM","nama":"Garam","satuan":"1","harga":"14000"}', '[]'),
(55, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mgudang', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mgudang/create_process', '2018-03-19 20:52:37', '{"kode":"GD2","nama":"Gudang 2"}', '[]'),
(56, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-19 20:53:14', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000001","no_bukti_penerimaan_barang":"123608","bahan_baku":"1","jumlah":"20","mata_uang":"1","nilai_barang":"200000","gudang":"1","negara_asal_barang":"1"}', '[]'),
(57, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-19 20:55:29', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000002","no_bukti_penerimaan_barang":"999777","bahan_baku":"1","jumlah":"20","mata_uang":"1","nilai_barang":"200000","gudang":"2","negara_asal_barang":"1"}', '[]'),
(58, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-19 20:56:34', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000004","no_bukti_penerimaan_barang":"123123123","bahan_baku":"2","jumlah":"15","mata_uang":"1","nilai_barang":"200000","gudang":"1","negara_asal_barang":"1"}', '[]'),
(59, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-19 20:59:12', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000005","no_bukti_penerimaan_barang":"123608","bahan_baku":"1","jumlah":"10","mata_uang":"1","nilai_barang":"200000","gudang":"1","negara_asal_barang":"1"}', '[]'),
(60, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-19 22:42:04', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000006","tanggal_dokumen_pabean":"2018-03-19","no_seri_barang":"0000111","no_bukti_penerimaan_barang":"999776","bahan_baku":"3","jumlah":"35","mata_uang":"1","nilai_barang":"123456","gudang":"1","negara_asal_barang":"1"}', '[]'),
(61, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-03-20 00:51:35', '{"no_bukti_pengeluaran":"111357","tanggal_bukti_pengeluaran":"2018-03-18","bahan_baku":"3","jumlah":"14","gudang":"1"}', '[]'),
(62, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost:81/ProduksiKM/index.php/home/logout_process', '2018-03-20 00:59:42', '[]', '[]'),
(63, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-20 22:00:17', '{"user_name":"admin","password":"12345678"}', '[]'),
(64, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-21 02:17:12', '{"user_name":"admin","password":"12345678"}', '[]'),
(65, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-22 19:41:38', '{"user_name":"admin","password":"12345678"}', '[]'),
(66, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-22 23:32:10', '{"user_name":"admin","password":"12345678"}', '[]'),
(67, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process', '2018-03-23 04:26:25', '{"no_bukti_penerimaan":"1112556","tanggal_bukti_penerimaan":"2018-03-07","barang_jadi":"1","jumlah":"60","gudang":"1"}', '[]'),
(68, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpengeluaranbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpengeluaranbjadi/create_process', '2018-03-23 04:27:18', '{"no_feb":"G0056677","tanggal_feb":"2018-03-15","no_bukti_pengeluaran":"111357","tanggal_bukti_pengeluaran":"2018-03-27","pembeli":"1","negara_tujuan":"1","barang_jadi":"1","jumlah":"15","mata_uang":"1","nilai_barang":"123456","gudang":"1"}', '[]'),
(69, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost:81/ProduksiKM/index.php/home/logout_process', '2018-03-23 04:47:59', '[]', '[]'),
(70, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-23 04:48:06', '{"user_name":"admin","password":"12345678"}', '[]'),
(71, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-23 21:03:09', '{"user_name":"admin","password":"12345678"}', '[]'),
(72, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'msatuan', 'create_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/create_process', '2018-03-23 21:04:27', '{"kode":"TON","nama":"Tons"}', '[]'),
(73, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mmuang', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mmuang/create_process', '2018-03-23 21:05:07', '{"kode":"SGD","nama":"Singapore Dollar"}', '[]'),
(74, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'delete_process', 'Success', 'mmuang', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/mmuang/delete_process/3', '2018-03-23 21:05:14', '[]', '[]'),
(75, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mbbaku/create_process', '2018-03-23 21:07:31', '{"kode":"CASANA","nama":"Casana ","satuan":"1","harga":"111111"}', '[]'),
(76, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-23 21:11:29', '{"jenis_dokumen":"1111111","no_dokumen_pabean":"0000007","tanggal_dokumen_pabean":"2018-03-24","no_seri_barang":"111111","no_bukti_penerimaan_barang":"123123123","bahan_baku":"4","jumlah":"1000","mata_uang":"2","nilai_barang":"10000","gudang":"1","negara_asal_barang":"1"}', '[]'),
(77, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-03-23 21:15:21', '{"no_bukti_pengeluaran":"111357","tanggal_bukti_pengeluaran":"2018-03-24","bahan_baku":"4","jumlah":"500","gudang":"1"}', '[]'),
(78, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mbjadi/create_process', '2018-03-23 21:17:32', '{"kode":"SPS","nama":"Super Potato Startch","satuan":"1","harga":"10000"}', '[]'),
(79, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process', '2018-03-23 21:19:28', '{"no_bukti_penerimaan":"11125562","tanggal_bukti_penerimaan":"2018-03-24","barang_jadi":"2","jumlah":"520","gudang":"2"}', '[]'),
(80, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-24 01:29:38', '{"user_name":"admin","password":"12345678"}', '[]'),
(81, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process', '2018-03-24 01:45:46', '{"no_bukti_penerimaan":"1112556","tanggal_bukti_penerimaan":"2018-03-14","barang_jadi":"1","jumlah":"1","gudang":"1","no_pemakaian_bahan_baku":["OBB20180300001","OBB20180300002"]}', '[]'),
(82, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process', '2018-03-24 01:58:38', '{"no_bukti_penerimaan":"11125562","tanggal_bukti_penerimaan":"2018-03-12","barang_jadi":"1","jumlah":"2","gudang":"1","no_pemakaian_bahan_baku":["OBB20180300001","OBB20180300002"]}', '[]'),
(83, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-24 02:06:09', '{"jenis_dokumen":"1111111222","no_dokumen_pabean":"0000009","tanggal_dokumen_pabean":"2018-03-07","no_seri_barang":"2222","no_bukti_penerimaan_barang":"22222","bahan_baku":"1","jumlah":"200","mata_uang":"1","nilai_barang":"123123123","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"1000000"}', '[]'),
(84, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpengeluaranbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpengeluaranbjadi/create_process', '2018-03-24 02:06:52', '{"no_feb":"G00566773","tanggal_feb":"2018-03-23","no_bukti_pengeluaran":"33333","tanggal_bukti_pengeluaran":"2018-03-28","pembeli":"1","negara_tujuan":"1","barang_jadi":"1","jumlah":"10000","mata_uang":"1","nilai_barang":"123456","gudang":"1","harga_satuan":"3000000"}', '[]'),
(85, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-25 20:12:01', '{"user_name":"admin","password":"12345678"}', '[]'),
(86, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpenyelesaianbwaste', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/create_process', '2018-03-25 20:31:42', '{"no_bc_24":"113123","tanggal_bc_24":"2018-03-21","barang_waste":"1","jumlah":"30","nilai":"1000","gudang":"1","no_pengeluaran_barang_jadi":["OBJ20180300001","OBJ20180300002"]}', '[]'),
(87, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpenyelesaianbwaste', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/create_process', '2018-03-25 20:32:27', '{"no_bc_24":"113123","tanggal_bc_24":"2018-03-21","barang_waste":"1","jumlah":"30","nilai":"1000","gudang":"1","no_pengeluaran_barang_jadi":["OBJ20180300001","OBJ20180300002"]}', '[]'),
(88, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-25 21:57:16', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000010","tanggal_dokumen_pabean":"2018-03-27","no_seri_barang":"111111","no_bukti_penerimaan_barang":"123123123","bahan_baku":"4","jumlah":"600","mata_uang":"1","nilai_barang":"50000","gudang":"2","negara_asal_barang":"1","supplier":"1","harga_satuan":"1000000"}', '[]'),
(89, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-03-25 23:00:41', '{"id":"10","no_transaksi":"IBB20180300010","tanggal":"2018-03-25 21:57:16","jenis_dokumen":"PBN","no_dokumen_pabean":"0000010","tanggal_dokumen_pabean":"2018-03-27 00:00:00","no_seri_barang":"111111","no_bukti_penerimaan_barang":"123123123","bahan_baku":"4","jumlah":"700","mata_uang":"1","nilai_barang":"50000","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"1000000"}', '[]'),
(90, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-03-26 00:04:08', '{"no_bukti_pengeluaran":"1236899","tanggal_bukti_pengeluaran":"2018-03-15","bahan_baku":"4","jumlah":"1050","gudang":"1"}', '[]'),
(91, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-03-26 00:22:39', '{"no_bukti_pengeluaran":"12368992","tanggal_bukti_pengeluaran":"2018-03-30","bahan_baku":"4","jumlah":"75","gudang":"1"}', '[]'),
(92, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-03-26 00:24:53', '{"id":"4","no_transaksi":"OBB20180300004","tanggal":"2018-03-26 00:22:39","no_bukti_pengeluaran":"12368992","tanggal_bukti_pengeluaran":"2018-03-30 00:00:00","bahan_baku":"4","jumlah":"80","gudang":"1"}', '[]'),
(93, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mbjadi/create_process', '2018-03-26 00:25:54', '{"kode":"MIAC","nama":"Mie Aceh","satuan":"3","harga":"50000"}', '[]'),
(94, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process', '2018-03-26 00:26:24', '{"no_bukti_penerimaan":"1112556","tanggal_bukti_penerimaan":"2018-03-23","barang_jadi":"3","jumlah":"600","gudang":"1","no_pemakaian_bahan_baku":["OBB20180300001","OBB20180300002","OBB20180300004"]}', '[]'),
(95, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbjadi', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/confirm_process', '2018-03-26 00:27:31', '{"id":"5","no_transaksi":"IBJ20180300005","tanggal":"2018-03-26 00:26:24","no_bukti_penerimaan":"1112556","tanggal_bukti_penerimaan":"2018-03-23 00:00:00","barang_jadi":"1","jumlah":"10000","gudang":"1"}', '[]'),
(96, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbjadi', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/confirm_process', '2018-03-26 00:27:46', '{"id":"5","no_transaksi":"IBJ20180300005","tanggal":"2018-03-26 00:26:24","no_bukti_penerimaan":"1112556","tanggal_bukti_penerimaan":"2018-03-23 00:00:00","barang_jadi":"1","jumlah":"10000","gudang":"1","no_pemakaian_bahan_baku":["OBB20180300001","OBB20180300002"]}', '[]'),
(97, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpengeluaranbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpengeluaranbjadi/create_process', '2018-03-26 00:29:13', '{"no_feb":"G0056677","tanggal_feb":"2018-03-29","no_bukti_pengeluaran":"33333","tanggal_bukti_pengeluaran":"2018-03-29","pembeli":"1","negara_tujuan":"1","barang_jadi":"1","jumlah":"5045","mata_uang":"1","nilai_barang":"50000","gudang":"1","harga_satuan":"1000000"}', '[]'),
(98, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpengeluaranbjadi', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpengeluaranbjadi/confirm_process', '2018-03-26 00:29:29', '{"id":"3","no_transaksi":"OBJ20180300003","tanggal":"2018-03-26 00:29:13","no_feb":"G0056677","tanggal_feb":"2018-03-29 00:00:00","no_bukti_pengeluaran":"33333","tanggal_bukti_pengeluaran":"2018-03-29 00:00:00","pembeli":"1","negara_tujuan":"1","barang_jadi":"1","jumlah":"5045","mata_uang":"1","nilai_barang":"50000","gudang":"1","harga_satuan":"1000000"}', '[]'),
(99, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpenyelesaianbwaste', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/create_process', '2018-03-26 00:32:38', '{"no_bc_24":"113123","tanggal_bc_24":"2018-03-22","barang_waste":"1","jumlah":"90","nilai":"123","gudang":"1","no_pengeluaran_barang_jadi":["OBJ20180300003"]}', '[]'),
(100, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpenyelesaianbwaste', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/confirm_process', '2018-03-26 00:32:58', '{"id":"4","no_transaksi":"IBJ20180300004","tanggal":"2018-03-26 00:32:38","no_bc_24":"113123","tanggal_bc_24":"2018-03-22 00:00:00","barang_waste":"1","jumlah":"90","nilai":"123","gudang":"1","no_pengeluaran_barang_jadi":["OBJ20180300001"]}', '[]'),
(101, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process', '2018-03-26 00:54:07', '{"no_bukti_penerimaan":"11125562","tanggal_bukti_penerimaan":"2018-03-28","barang_jadi":"3","jumlah":"5","gudang":"1","no_pemakaian_bahan_baku":["OBB20180300002"]}', '[]'),
(102, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbjadi', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/confirm_process', '2018-03-26 00:54:17', '{"id":"6","no_transaksi":"IBJ20180300006","tanggal":"2018-03-26 00:54:07","no_bukti_penerimaan":"11125562","tanggal_bukti_penerimaan":"2018-03-28 00:00:00","barang_jadi":"3","jumlah":"5","gudang":"1"}', '[]'),
(103, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbjadi', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process', '2018-03-26 00:54:59', '{"no_bukti_penerimaan":"123123","tanggal_bukti_penerimaan":"2018-03-21","barang_jadi":"3","jumlah":"10","gudang":"1","no_pemakaian_bahan_baku":["OBB20180300003"]}', '[]'),
(104, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpenyelesaianbwaste', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/create_process', '2018-03-26 01:10:30', '{"no_bc_24":"113123","tanggal_bc_24":"2018-03-14","barang_waste":"1","jumlah":"25","nilai":"25000","gudang":"1","no_pengeluaran_barang_jadi":["OBJ20180300002"]}', '[]'),
(105, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'delete_process', 'Success', 'tpenyelesaianbwaste', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/delete_process/5', '2018-03-26 01:12:09', '[]', '[]'),
(106, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-26 04:49:27', '{"user_name":"admin","password":"12345678"}', '[]'),
(107, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-26 19:38:37', '{"user_name":"admin","password":"12345678"}', '[]'),
(108, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-28 01:48:03', '{"user_name":"admin","password":"12345678"}', '[]'),
(109, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-28 04:41:46', '{"user_name":"admin","password":"12345678"}', '[]'),
(110, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-28 04:45:27', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000011","tanggal_dokumen_pabean":"2018-03-31","no_seri_barang":"111123","no_bukti_penerimaan_barang":"1231231255","tanggal_bukti_penerimaan_barang":"2018-03-30","bahan_baku":"2","jumlah":"500","mata_uang":"1","nilai_barang":"500000","gudang":"2","negara_asal_barang":"1","supplier":"1","harga_satuan":"5000"}', '[]'),
(111, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-03-28 04:45:47', '{"id":"11","no_transaksi":"IBB20180300011","tanggal":"2018-03-28 04:45:27","jenis_dokumen":"PBN","no_dokumen_pabean":"0000011","tanggal_dokumen_pabean":"2018-03-31","no_seri_barang":"111123","no_bukti_penerimaan_barang":"1231231255","tanggal_bukti_penerimaan_barang":"2018-03-30","bahan_baku":"2","jumlah":"500","mata_uang":"1","nilai_barang":"500000","gudang":"2","negara_asal_barang":"1","supplier":"1","harga_satuan":"5000"}', '[]'),
(112, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-28 04:46:37', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000012","tanggal_dokumen_pabean":"2018-03-23","no_seri_barang":"213","no_bukti_penerimaan_barang":"12312312","tanggal_bukti_penerimaan_barang":"2018-03-29","bahan_baku":"1","jumlah":"12","mata_uang":"1","nilai_barang":"1233","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"6000"}', '[]'),
(113, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mbbaku/create_process', '2018-03-28 04:47:15', '{"kode":"GULA","nama":"Gula","satuan":"3","harga":"6000"}', '[]'),
(114, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-28 04:50:39', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000013","tanggal_dokumen_pabean":"2018-03-31","no_seri_barang":"565465465465","no_bukti_penerimaan_barang":"123123123","tanggal_bukti_penerimaan_barang":"2018-03-29","bahan_baku":"1","jumlah":"70","mata_uang":"1","nilai_barang":"11111111","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"111111"}', '[]'),
(115, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-03-28 04:50:48', '{"id":"12","no_transaksi":"IBB20180300012","tanggal":"2018-03-28 04:46:37","jenis_dokumen":"PBN","no_dokumen_pabean":"0000012","tanggal_dokumen_pabean":"2018-03-23 00:00:00","no_seri_barang":"213","no_bukti_penerimaan_barang":"12312312","tanggal_bukti_penerimaan_barang":"2018-03-29 00:00:00","bahan_baku":"1","jumlah":"12","mata_uang":"1","nilai_barang":"1233","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"6000"}', '[]'),
(116, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-03-28 04:50:54', '{"id":"13","no_transaksi":"IBB20180300013","tanggal":"2018-03-28 04:50:39","jenis_dokumen":"PBN","no_dokumen_pabean":"0000013","tanggal_dokumen_pabean":"2018-03-31 00:00:00","no_seri_barang":"565465465465","no_bukti_penerimaan_barang":"123123123","tanggal_bukti_penerimaan_barang":"2018-03-29 00:00:00","bahan_baku":"1","jumlah":"70","mata_uang":"1","nilai_barang":"11111111","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"111111"}', '[]'),
(117, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-03-28 19:26:07', '{"user_name":"admin","password":"12345678"}', '[]'),
(118, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-28 20:12:21', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000014","tanggal_dokumen_pabean":"2018-03-30","no_seri_barang":"12314","no_bukti_penerimaan_barang":"123124","tanggal_bukti_penerimaan_barang":"2018-03-26","bahan_baku":"5","jumlah":"500","mata_uang":"1","nilai_barang":"5000","gudang":"2","negara_asal_barang":"1","supplier":"1","harga_satuan":"123123"}', '[]'),
(119, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-03-28 20:12:35', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000014","tanggal_dokumen_pabean":"2018-03-30","no_seri_barang":"12314","no_bukti_penerimaan_barang":"123124","tanggal_bukti_penerimaan_barang":"2018-03-26","bahan_baku":"5","jumlah":"500","mata_uang":"1","nilai_barang":"5000","gudang":"2","negara_asal_barang":"1","supplier":"1","harga_satuan":"123123"}', '[]'),
(120, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-03-28 20:13:27', '{"id":"14","no_transaksi":"IBB20180300013","tanggal":"2018-03-28 20:12:21","jenis_dokumen":"PBN","no_dokumen_pabean":"0000014","tanggal_dokumen_pabean":"2018-03-30 00:00:00","no_seri_barang":"12314","no_bukti_penerimaan_barang":"123124","tanggal_bukti_penerimaan_barang":"2018-03-26 00:00:00","bahan_baku":"5","jumlah":"500","mata_uang":"1","nilai_barang":"5000","gudang":"2","negara_asal_barang":"1","supplier":"1","harga_satuan":"123123"}', '[]'),
(121, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-03-28 20:13:35', '{"id":"15","no_transaksi":"IBB20180300014","tanggal":"2018-03-28 20:12:35","jenis_dokumen":"PBN","no_dokumen_pabean":"0000014","tanggal_dokumen_pabean":"2018-03-30 00:00:00","no_seri_barang":"12314","no_bukti_penerimaan_barang":"123124","tanggal_bukti_penerimaan_barang":"2018-03-26 00:00:00","bahan_baku":"5","jumlah":"500","mata_uang":"1","nilai_barang":"5000","gudang":"2","negara_asal_barang":"1","supplier":"1","harga_satuan":"123123"}', '[]'),
(122, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-04-28 20:17:29', '{"user_name":"admin","password":"12345678"}', '[]'),
(123, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-04-28 20:20:53', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000001","tanggal_dokumen_pabean":"2018-04-12","no_seri_barang":"4846894665","no_bukti_penerimaan_barang":"2111351651","tanggal_bukti_penerimaan_barang":"2018-04-25","bahan_baku":"1","jumlah":"500","mata_uang":"1","nilai_barang":"200000","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"6000"}', '[]'),
(124, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-04-28 20:22:26', '{"id":"16","no_transaksi":"IBB20180400002","tanggal":"2018-04-28 20:20:53","jenis_dokumen":"PBN","no_dokumen_pabean":"0000001","tanggal_dokumen_pabean":"2018-04-12 00:00:00","no_seri_barang":"4846894665","no_bukti_penerimaan_barang":"2111351651","tanggal_bukti_penerimaan_barang":"2018-04-25 00:00:00","bahan_baku":"1","jumlah":"500","mata_uang":"1","nilai_barang":"200000","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"6000"}', '[]'),
(125, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-05-28 20:23:04', '{"user_name":"admin","password":"12345678"}', '[]');
INSERT INTO `core_app_log` (`id`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `device_id`, `user_id`, `event`, `message`, `module`, `action`, `link`, `created_on`, `input_data`, `json_return`) VALUES
(126, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-05-28 20:23:47', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000009","tanggal_dokumen_pabean":"2018-05-11","no_seri_barang":"111123","no_bukti_penerimaan_barang":"2111351651","tanggal_bukti_penerimaan_barang":"2018-05-30","bahan_baku":"1","jumlah":"60","mata_uang":"1","nilai_barang":"7000","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"60"}', '[]'),
(127, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-06-28 20:25:44', '{"user_name":"admin","password":"12345678"}', '[]'),
(128, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-06-28 20:31:19', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"0000011","tanggal_dokumen_pabean":"2018-06-20","no_seri_barang":"4846894665","no_bukti_penerimaan_barang":"123608","tanggal_bukti_penerimaan_barang":"2018-04-25","bahan_baku":"1","jumlah":"50","mata_uang":"1","nilai_barang":"10000","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"10000"}', '[]'),
(129, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'delete_process', 'Success', 'tpemasukanbbaku', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/delete_process/17', '2018-06-28 20:31:52', '[]', '[]'),
(130, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'delete_process', 'Success', 'tpemasukanbbaku', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/delete_process/24', '2018-06-28 20:31:58', '[]', '[]'),
(131, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost:81/ProduksiKM/index.php/home/logout_process', '2018-03-28 22:09:40', '[]', '[]'),
(132, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-04-01 19:38:40', '{"user_name":"admin","password":"12345678"}', '[]'),
(133, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'edit_process', 'Success', 'msatuan', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/edit_process', '2018-04-01 19:39:09', '{"id":"6","kode":"TON","nama":"Ton"}', '[]'),
(134, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'msatuan', 'create_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/create_process', '2018-04-01 19:39:35', '{"kode":"SAK","nama":"sak"}', '[]'),
(135, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'edit_process', 'Success', 'msatuan', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/msatuan/edit_process', '2018-04-01 19:39:51', '{"id":"7","kode":"SAK","nama":"Sak"}', '[]'),
(136, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mmuang', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mmuang/create_process', '2018-04-01 19:58:17', '{"kode":"SGD","nama":"Sgd"}', '[]'),
(137, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'edit_process', 'Success', 'mmuang', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/mmuang/edit_process', '2018-04-01 19:58:23', '{"id":"4","kode":"SGD","nama":"Sgd"}', '[]'),
(138, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'delete_process', 'Success', 'mmuang', 'delete_process', 'http://localhost:81/ProduksiKM/index.php/mmuang/delete_process/4', '2018-04-01 19:58:29', '[]', '[]'),
(139, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mmuang', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mmuang/create_process', '2018-04-01 20:00:35', '{"kode":"SGD","nama":"Sgd"}', '[]'),
(140, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'edit_process', 'Success', 'mmuang', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/mmuang/edit_process', '2018-04-01 20:00:47', '{"id":"5","kode":"SGD","nama":"Singapore Dollar"}', '[]'),
(141, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mnegara', 'create_process', 'http://localhost:81/ProduksiKM/index.php/mnegara/create_process', '2018-04-01 20:22:43', '{"kode":"CN","nama":"China"}', '[]'),
(142, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-04-02 20:24:53', '{"user_name":"admin","password":"12345678"}', '[]'),
(143, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-04-11 20:08:44', '{"user_name":"admin","password":"12345678"}', '[]'),
(144, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-04-11 20:20:20', '{"user_name":"admin","password":"12345678"}', '[]'),
(145, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'edit_process', 'Success', 'mprofile', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/mprofile/edit_process', '2018-04-11 21:07:36', '{"id":"1","password":"87654321","confirm_password":"87654321"}', '[]'),
(146, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost:81/ProduksiKM/index.php/home/logout_process', '2018-04-11 21:07:41', '[]', '[]'),
(147, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-04-11 21:07:50', '{"user_name":"admin","password":"87654321"}', '[]'),
(148, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'edit_process', 'Success', 'mprofile', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/mprofile/edit_process', '2018-04-11 21:08:01', '{"id":"1","password":"87654321","confirm_password":"87654321"}', '[]'),
(149, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'edit_process', 'Success', 'mprofile', 'edit_process', 'http://localhost:81/ProduksiKM/index.php/mprofile/edit_process', '2018-04-11 21:10:10', '{"id":"1","password":"12345678","confirm_password":"12345678"}', '[]'),
(150, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost:81/ProduksiKM/index.php/home/logout_process', '2018-04-11 21:24:12', '[]', '[]'),
(151, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-04-11 21:24:16', '{"user_name":"admin","password":"12345678"}', '[]'),
(152, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', '', '', 1, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost:81/ProduksiKM/index.php/home/logout_process', '2018-04-11 21:31:59', '[]', '[]'),
(153, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-04-12 20:17:27', '{"user_name":"admin","password":"12345678"}', '[]'),
(154, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-04-13 00:21:35', '{"user_name":"admin","password":"12345678"}', '[]'),
(155, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-04-22 22:06:05', '{"user_name":"admin","password":"12345678"}', '[]'),
(156, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost:81/ProduksiKM/index.php/home/login_process', '2018-05-01 21:29:57', '{"user_name":"admin","password":"12345678"}', '[]'),
(157, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-12 06:00:37', '{"user_name":"admin","password":"12345678"}', '[]'),
(158, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'logout_process', 'Success', 'home', 'logout_process', 'http://localhost/ProduksiKM/index.php/home/logout_process', '2018-05-12 06:02:23', '[]', '[]'),
(159, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-12 06:02:35', '{"user_name":"admin","password":"12345678"}', '[]'),
(160, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'msatuan', 'create_process', 'http://localhost/ProduksiKM/index.php/msatuan/create_process', '2018-05-12 06:03:55', '{"kode":"GONI","nama":"Goni"}', '[]'),
(161, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'edit_process', 'Success', 'msatuan', 'edit_process', 'http://localhost/ProduksiKM/index.php/msatuan/edit_process', '2018-05-12 06:04:29', '{"id":"8","kode":"GONI","nama":"Goni putih"}', '[]'),
(162, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'delete_process', 'Success', 'msatuan', 'delete_process', 'http://localhost/ProduksiKM/index.php/msatuan/delete_process/8', '2018-05-12 06:04:39', '[]', '[]'),
(163, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/mbbaku/create_process', '2018-05-12 06:10:58', '{"kode":"TEST","nama":"TEST","satuan":"6","harga":"10000"}', '[]'),
(164, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-05-12 06:13:33', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"1212121212","tanggal_dokumen_pabean":"2018-05-13","no_seri_barang":"2123121","no_bukti_penerimaan_barang":"78321231212123","tanggal_bukti_penerimaan_barang":"2018-05-13","bahan_baku":"6","jumlah":"200","mata_uang":"2","nilai_barang":"1500000","gudang":"1","negara_asal_barang":"1","supplier":"2","harga_satuan":"121212"}', '[]'),
(165, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-05-12 06:14:42', '{"id":"25","no_transaksi":"IBB20180500002","tanggal":"2018-05-12 06:13:33","jenis_dokumen":"PBN","no_dokumen_pabean":"1212121212","tanggal_dokumen_pabean":"2018-05-13 00:00:00","no_seri_barang":"2123121","no_bukti_penerimaan_barang":"78321231212123","tanggal_bukti_penerimaan_barang":"2018-05-13 00:00:00","bahan_baku":"6","jumlah":"300","mata_uang":"2","nilai_barang":"1500000","gudang":"1","negara_asal_barang":"1","supplier":"2","harga_satuan":"121212"}', '[]'),
(166, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-05-12 06:17:29', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"1212121212","tanggal_dokumen_pabean":"2018-05-18","no_seri_barang":"2123121","no_bukti_penerimaan_barang":"78321231212123","tanggal_bukti_penerimaan_barang":"2018-05-13","bahan_baku":"6","jumlah":"100","mata_uang":"1","nilai_barang":"1","gudang":"2","negara_asal_barang":"1","supplier":"1","harga_satuan":"1"}', '[]'),
(167, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-05-12 06:17:46', '{"id":"26","no_transaksi":"IBB20180500003","tanggal":"2018-05-12 06:17:29","jenis_dokumen":"PBN","no_dokumen_pabean":"1212121212","tanggal_dokumen_pabean":"2018-05-18 00:00:00","no_seri_barang":"2123121","no_bukti_penerimaan_barang":"78321231212123","tanggal_bukti_penerimaan_barang":"2018-05-13 00:00:00","bahan_baku":"6","jumlah":"100","mata_uang":"1","nilai_barang":"1","gudang":"2","negara_asal_barang":"1","supplier":"1","harga_satuan":"1"}', '[]'),
(168, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-05-12 06:19:45', '{"jenis_dokumen":"PBN","no_dokumen_pabean":"1212121212","tanggal_dokumen_pabean":"2018-05-18","no_seri_barang":"2123121","no_bukti_penerimaan_barang":"78321231212123","tanggal_bukti_penerimaan_barang":"2018-05-18","bahan_baku":"6","jumlah":"500","mata_uang":"1","nilai_barang":"1","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"1"}', '[]'),
(169, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-05-12 06:20:15', '{"id":"27","no_transaksi":"IBB20180500004","tanggal":"2018-05-12 06:19:45","jenis_dokumen":"PBN","no_dokumen_pabean":"1212121212","tanggal_dokumen_pabean":"2018-05-18 00:00:00","no_seri_barang":"2123121","no_bukti_penerimaan_barang":"78321231212123","tanggal_bukti_penerimaan_barang":"2018-05-18 00:00:00","bahan_baku":"6","jumlah":"500","mata_uang":"1","nilai_barang":"1","gudang":"1","negara_asal_barang":"1","supplier":"1","harga_satuan":"1"}', '[]'),
(170, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-05-12 06:23:11', '{"no_bukti_pengeluaran":"1111345","tanggal_bukti_pengeluaran":"2018-05-11","bahan_baku":"6","jumlah":"400","gudang":"1"}', '[]'),
(171, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-05-12 06:23:23', '{"id":"5","no_transaksi":"OBB20180500001","tanggal":"2018-05-12 06:23:11","no_bukti_pengeluaran":"1111345","tanggal_bukti_pengeluaran":"2018-05-11 00:00:00","bahan_baku":"6","jumlah":"400","gudang":"1"}', '[]'),
(172, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbjadi/create_process', '2018-05-12 06:26:53', '{"no_bukti_penerimaan":"123123123","tanggal_bukti_penerimaan":"2018-05-11","barang_jadi":"2","jumlah":"500","gudang":"1","no_pemakaian_bahan_baku":["OBB20180300001","OBB20180300002","OBB20180300004"]}', '[]'),
(173, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbjadi', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbjadi/confirm_process', '2018-05-12 06:27:16', '{"id":"8","no_transaksi":"IBJ20180500001","tanggal":"2018-05-12 06:26:53","no_bukti_penerimaan":"123123123","tanggal_bukti_penerimaan":"2018-05-11 00:00:00","barang_jadi":"2","jumlah":"500","gudang":"1","no_pemakaian_bahan_baku":["OBB20180300001","OBB20180300002","OBB20180300004"]}', '[]'),
(174, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpengeluaranbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/tpengeluaranbjadi/create_process', '2018-05-12 06:30:24', '{"no_feb":"11124","tanggal_feb":"2018-05-11","no_bukti_pengeluaran":"1111345","tanggal_bukti_pengeluaran":"2018-05-18","pembeli":"1","negara_tujuan":"1","barang_jadi":"1","jumlah":"20","mata_uang":"1","nilai_barang":"1","gudang":"1","harga_satuan":"1"}', '[]'),
(175, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpengeluaranbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/tpengeluaranbjadi/create_process', '2018-05-12 06:30:25', '{"no_feb":"11124","tanggal_feb":"2018-05-11","no_bukti_pengeluaran":"1111345","tanggal_bukti_pengeluaran":"2018-05-18","pembeli":"1","negara_tujuan":"1","barang_jadi":"1","jumlah":"20","mata_uang":"1","nilai_barang":"1","gudang":"1","harga_satuan":"1"}', '[]'),
(176, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpengeluaranbjadi', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpengeluaranbjadi/confirm_process', '2018-05-12 06:30:34', '{"id":"5","no_transaksi":"OBJ20180500002","tanggal":"2018-05-12 06:30:25","no_feb":"11124","tanggal_feb":"2018-05-11 00:00:00","no_bukti_pengeluaran":"1111345","tanggal_bukti_pengeluaran":"2018-05-18 00:00:00","pembeli":"1","negara_tujuan":"1","barang_jadi":"1","jumlah":"20","mata_uang":"1","nilai_barang":"1","gudang":"1","harga_satuan":"1"}', '[]'),
(177, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'confirm_process', 'Success', 'tpengeluaranbjadi', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpengeluaranbjadi/confirm_process', '2018-05-12 06:31:55', '{"id":"4","no_transaksi":"OBJ20180500001","tanggal":"2018-05-12 06:30:24","no_feb":"11124","tanggal_feb":"2018-05-11 00:00:00","no_bukti_pengeluaran":"1111345","tanggal_bukti_pengeluaran":"2018-05-18 00:00:00","pembeli":"1","negara_tujuan":"1","barang_jadi":"1","jumlah":"20","mata_uang":"1","nilai_barang":"1","gudang":"1","harga_satuan":"1"}', '[]'),
(178, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'create_process', 'Success', 'tpenyelesaianbwaste', 'create_process', 'http://localhost/ProduksiKM/index.php/tpenyelesaianbwaste/create_process', '2018-05-12 06:34:18', '{"no_bc_24":"111","tanggal_bc_24":"2018-05-18","barang_waste":"1","jumlah":"500","nilai":"99999","gudang":"2","no_pengeluaran_barang_jadi":["OBJ20180300002"]}', '[]'),
(179, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-12 09:04:52', '{"user_name":"admin","password":"12345678"}', '[]'),
(180, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-16 03:32:23', '{"user_name":"admin","password":"12345678"}', '[]'),
(181, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-16 04:23:20', '{"id":"2","kode":"SPS","nama":"Super Potato Startch","satuan":"1","harga":"10000"}', '[]'),
(182, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-17 06:49:13', '{"user_name":"admin","password":"12345678"}', '[]'),
(183, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-17 09:18:51', '{"user_name":"admin","password":"12345678"}', '[]'),
(184, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-17 11:33:21', '{"user_name":"admin","password":"12345678"}', '[]'),
(185, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-18 06:13:15', '{"user_name":"admin","password":"12345678"}', '[]'),
(186, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-21 03:37:22', '{"user_name":"admin","password":"12345678"}', '[]'),
(187, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/mbbaku/create_process', '2018-05-21 04:17:50', '{"kode":"PS","nama":"Pati Kentang","satuan":"6","harga":"790"}', '[]'),
(188, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/mbbaku/create_process', '2018-05-21 04:19:23', '{"kode":"WS","nama":"Pati Gandum","satuan":"6","harga":"420"}', '[]'),
(189, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/mbbaku/create_process', '2018-05-21 04:20:07', '{"kode":"CS","nama":"Pati Ubi Kayu","satuan":"1","harga":"550"}', '[]'),
(190, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbbaku', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbbaku/edit_process', '2018-05-21 04:21:08', '{"id":"9","kode":"CasS","nama":"Pati Ubi Kayu","satuan":"1","harga":"550"}', '[]'),
(191, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/mbbaku/create_process', '2018-05-21 04:22:05', '{"kode":"CS","nama":"Pati Jagung","satuan":"6","harga":"400"}', '[]'),
(192, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/mbbaku/create_process', '2018-05-21 04:23:37', '{"kode":"MCASS","nama":"Pati Ubi Kayu Modifikasi","satuan":"6","harga":"700"}', '[]'),
(193, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbbaku', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbbaku/edit_process', '2018-05-21 04:26:39', '{"id":"9","kode":"CasS","nama":"Pati Ubi Kayu","satuan":"6","harga":"550"}', '[]'),
(194, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/mbbaku/create_process', '2018-05-21 04:30:08', '{"kode":"MCASS","nama":"Pati Ubi Kayu Modifikasi","satuan":"6","harga":"700"}', '[]'),
(195, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'mbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/mbbaku/delete_process/11', '2018-05-21 04:30:26', '[]', '[]'),
(196, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbbaku', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbbaku/edit_process', '2018-05-21 04:31:00', '{"id":"9","kode":"CASS","nama":"Pati Ubi Kayu","satuan":"6","harga":"550"}', '[]'),
(197, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-05-21 04:44:11', '{"jenis_dokumen":"Doc.Import","no_dokumen_pabean":"11081300","tanggal_dokumen_pabean":"2017-11-14","no_seri_barang":"EE1","no_bukti_penerimaan_barang":"01","tanggal_bukti_penerimaan_barang":"2018-07-02","bahan_baku":"7","jumlah":"7000","mata_uang":"2","nilai_barang":"7000","gudang":"1","negara_asal_barang":"4","supplier":"1","harga_satuan":"790"}', '[]'),
(198, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'msupplier', 'create_process', 'http://localhost/ProduksiKM/index.php/msupplier/create_process', '2018-05-21 04:50:24', '{"kode":"KMGM","nama":"Kilang Mie Gunung Mas","alamat":"Jalan Raya Tanjung Morawa Km. 14.2 - Tanjung Morawa\\r\\nSumatera Utara - Indonesia","telp":"(62-61) 7940385","fax":"(62-61) 7940384"}', '[]'),
(199, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-05-21 04:51:00', '{"id":"28","no_transaksi":"IBB20180500005","tanggal":"2018-05-21 04:44:11","jenis_dokumen":"Doc.Import","no_dokumen_pabean":"11081300","tanggal_dokumen_pabean":"2017-11-14 00:00:00","no_seri_barang":"EE1","no_bukti_penerimaan_barang":"01","tanggal_bukti_penerimaan_barang":"2018-07-02 00:00:00","bahan_baku":"7","jumlah":"7000","mata_uang":"2","nilai_barang":"7000","gudang":"1","negara_asal_barang":"4","supplier":"3","harga_satuan":"790"}', '[]'),
(200, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'mbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/mbbaku/delete_process/1', '2018-05-21 05:18:53', '[]', '[]'),
(201, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-21 09:14:03', '{"user_name":"admin","password":"12345678"}', '[]'),
(202, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'mbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/mbbaku/delete_process/2', '2018-05-21 09:16:44', '[]', '[]'),
(203, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'mbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/mbbaku/delete_process/3', '2018-05-21 09:16:53', '[]', '[]'),
(204, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'mbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/mbbaku/delete_process/4', '2018-05-21 09:20:52', '[]', '[]'),
(205, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'mbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/mbbaku/delete_process/5', '2018-05-21 09:20:57', '[]', '[]'),
(206, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'mbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/mbbaku/delete_process/6', '2018-05-21 09:21:05', '[]', '[]'),
(207, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'mbjadi', 'delete_process', 'http://localhost/ProduksiKM/index.php/mbjadi/delete_process/1', '2018-05-21 09:21:29', '[]', '[]'),
(208, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'mbjadi', 'delete_process', 'http://localhost/ProduksiKM/index.php/mbjadi/delete_process/3', '2018-05-21 09:21:37', '[]', '[]'),
(209, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-21 09:21:53', '{"id":"2","kode":"SPS","nama":"Super Potato Startch","satuan":"6","harga":"790"}', '[]'),
(210, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-22 04:01:44', '{"user_name":"admin","password":"12345678"}', '[]'),
(211, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'msupplier', 'delete_process', 'http://localhost/ProduksiKM/index.php/msupplier/delete_process/2', '2018-05-22 04:03:03', '[]', '[]'),
(212, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'msupplier', 'delete_process', 'http://localhost/ProduksiKM/index.php/msupplier/delete_process/1', '2018-05-22 04:03:10', '[]', '[]'),
(213, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-22 04:04:48', '{"id":"2","kode":"SPS","nama":"Super Potato Startch","satuan":"6","harga":"790"}', '[]'),
(214, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-22 04:05:29', '{"id":"2","kode":"SPSB1","nama":"Super Potato Startch B1","satuan":"6","harga":"790"}', '[]'),
(215, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-22 05:01:08', '{"id":"2","kode":"SPSB1","nama":"Super Potato Startch B1","satuan":"6","harga":" 13,411.15 "}', '[]'),
(216, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-22 05:01:52', '{"id":"2","kode":"SPSB1","nama":"Super Potato Startch B1","satuan":"6","harga":" 803.94 "}', '[]'),
(217, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-22 08:57:05', '{"user_name":"admin","password":"12345678"}', '[]'),
(218, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-22 08:57:51', '{"kode":"SPSC1","nama":"Super Potato Starch C1","satuan":"6","harga":"765"}', '[]'),
(219, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-22 08:59:38', '{"kode":"SPSD1","nama":"Super Potato Starch D1","satuan":"6","harga":"783.50"}', '[]'),
(220, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-22 09:00:02', '{"id":"2","kode":"SPSB1","nama":"Super Potato Startch B1","satuan":"6","harga":"795"}', '[]'),
(221, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-22 09:00:18', '{"id":"2","kode":"SPSB1","nama":"Super Potato Startch B1","satuan":"6","harga":"795.00"}', '[]'),
(222, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-22 09:00:33', '{"id":"4","kode":"SPSC1","nama":"Super Potato Starch C1","satuan":"6","harga":"765.00"}', '[]'),
(223, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-22 09:35:07', '{"kode":"SPSE1","nama":"Super Potato Starch E1","satuan":"6","harga":"728.00"}', '[]'),
(224, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-22 09:38:55', '{"kode":"SPSF1","nama":"Super Potato Starch F1","satuan":"6","harga":"741.00"}', '[]'),
(225, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-22 09:40:28', '{"kode":"SPSG1","nama":"Super Potato Starch G1","satuan":"6","harga":"691.00"}', '[]'),
(226, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-22 10:17:20', '{"kode":"SPS H1","nama":"Super Potato Starch H1","satuan":"6","harga":"819.50"}', '[]'),
(227, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-22 11:18:46', '{"id":"9","kode":"SPSH1","nama":"Super Potato Starch H1","satuan":"6","harga":"819.50"}', '[]'),
(228, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-22 11:19:01', '{"id":"2","kode":"SPSB1","nama":"Super Potato Starch B1","satuan":"6","harga":"795.00"}', '[]'),
(229, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-23 03:34:45', '{"user_name":"admin","password":"12345678"}', '[]'),
(230, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'tpemasukanbjadi', 'delete_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbjadi/delete_process/7', '2018-05-23 03:36:54', '[]', '[]'),
(231, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-23 04:13:07', '{"kode":"SPSI1","nama":"Super Potato Starch I1","satuan":"6","harga":"826.00"}', '[]'),
(232, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-23 04:13:59', '{"kode":"SPSJ1","nama":"Super Potato StarchJ1","satuan":"6","harga":"814.00"}', '[]'),
(233, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-23 04:14:17', '{"id":"8","kode":"SPSG1","nama":"Super Potato StarchG1","satuan":"6","harga":"691.00"}', '[]'),
(234, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-23 04:14:28', '{"id":"9","kode":"SPSH1","nama":"Super Potato StarchH1","satuan":"6","harga":"819.50"}', '[]'),
(235, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-23 04:14:49', '{"id":"2","kode":"SPSB1","nama":"Super Potato StarchB1","satuan":"6","harga":"795.00"}', '[]'),
(236, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-23 04:15:01', '{"id":"2","kode":"SPSB1","nama":"Super Potato Starch B1","satuan":"6","harga":"795.00"}', '[]'),
(237, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-23 04:15:11', '{"id":"4","kode":"SPSC1","nama":"Super Potato Starch C1","satuan":"6","harga":"765.00"}', '[]'),
(238, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-23 04:15:27', '{"id":"7","kode":"SPSF1","nama":"Super Potato Starch F1","satuan":"6","harga":"741.00"}', '[]'),
(239, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-23 04:15:40', '{"id":"8","kode":"SPSG1","nama":"Super Potato Starch G1","satuan":"6","harga":"691.00"}', '[]'),
(240, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-23 04:16:07', '{"id":"9","kode":"SPSH1","nama":"Super Potato Starch H1","satuan":"6","harga":"819.50"}', '[]'),
(241, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-23 04:16:17', '{"id":"10","kode":"SPSI1","nama":"Super Potato Starch I1","satuan":"6","harga":"826.00"}', '[]'),
(242, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-23 04:16:29', '{"id":"11","kode":"SPSJ1","nama":"Super Potato Starch J1","satuan":"6","harga":"814.00"}', '[]'),
(243, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-23 04:17:08', '{"kode":"SPSK1","nama":"Super Potato Starch K1","satuan":"6","harga":"802.00"}', '[]'),
(244, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-23 08:08:49', '{"user_name":"admin","password":"12345678"}', '[]'),
(245, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-23 08:12:53', '{"kode":"SPSL1","nama":"Super Potato Starch L1","satuan":"6","harga":"794.50"}', '[]'),
(246, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-23 08:15:28', '{"kode":"SPSM1","nama":"Super Potato Starch M1","satuan":"6","harga":"787.00"}', '[]'),
(247, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-23 08:16:54', '{"kode":"SPSN1","nama":"Super Potato StarchN1","satuan":"6","harga":"802.00"}', '[]'),
(248, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-23 08:18:35', '{"kode":"SPSO1","nama":"Super Potato Strach O1","satuan":"6","harga":"797.50"}', '[]'),
(249, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-23 08:19:21', '{"kode":"SPSP1","nama":"Super Potato Starch P1","satuan":"6","harga":"813.00"}', '[]'),
(250, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-23 08:20:04', '{"kode":"SPSQ1","nama":"Super Potato Starch Q1","satuan":"6","harga":"776.00"}', '[]'),
(251, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-23 08:21:59', '{"kode":"SPSR1","nama":"Super Potato Starch R1","satuan":"6","harga":"739.00"}', '[]'),
(252, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-23 11:38:26', '{"user_name":"admin","password":"12345678"}', '[]'),
(253, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-05-24 03:35:40', '{"user_name":"admin","password":"12345678"}', '[]'),
(254, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-24 03:36:54', '{"kode":"MPSX1","nama":"Modified Potato Starch X1","satuan":"6","harga":"850.00"}', '[]'),
(255, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-24 03:37:28', '{"kode":"MPSX2","nama":"Modified Potato Starch X2","satuan":"1","harga":"826.00"}', '[]'),
(256, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'edit_process', 'Success', 'mbjadi', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbjadi/edit_process', '2018-05-24 03:44:45', '{"id":"21","kode":"MPSX2","nama":"Modified Potato Starch X2","satuan":"6","harga":"826.00"}', '[]'),
(257, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-24 03:45:37', '{"kode":"MPSX3","nama":"Modified Potato Starch X3","satuan":"6","harga":"813.00"}', '[]'),
(258, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-24 03:46:26', '{"kode":"MPSX4","nama":"Modified Potato Starch X4","satuan":"6","harga":"841.00"}', '[]'),
(259, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-24 03:47:03', '{"kode":"MPSX5","nama":"Modified Potato Starch X5","satuan":"6","harga":"809.50"}', '[]'),
(260, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-24 03:47:38', '{"kode":"MPSX6","nama":"Modified Potato Starch X6","satuan":"6","harga":"789.00"}', '[]'),
(261, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-24 03:48:09', '{"kode":"MPSX7","nama":"Modified Potato Starch X7","satuan":"6","harga":"702.00"}', '[]'),
(262, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-24 03:48:46', '{"kode":"MPSX8","nama":"Modified Potato Starch X8","satuan":"6","harga":"739.00"}', '[]'),
(263, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-24 03:49:21', '{"kode":"MPSX9","nama":"Modified Potato Starch X9","satuan":"6","harga":"678.00"}', '[]');
INSERT INTO `core_app_log` (`id`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `device_id`, `user_id`, `event`, `message`, `module`, `action`, `link`, `created_on`, `input_data`, `json_return`) VALUES
(264, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'create_process', 'Success', 'mbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/mbjadi/create_process', '2018-05-24 03:50:05', '{"kode":"MPSX10","nama":"Modified Potato Starch X10","satuan":"6","harga":"776.00"}', '[]'),
(265, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-06-02 03:42:32', '{"user_name":"admin","password":"12345678"}', '[]'),
(266, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-06-02 08:28:20', '{"user_name":"admin","password":"12345678"}', '[]'),
(267, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-06-22 04:45:13', '{"user_name":"admin","password":"12345678"}', '[]'),
(268, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', 1, 'delete_process', 'Success', 'msatuan', 'delete_process', 'http://localhost/ProduksiKM/index.php/msatuan/delete_process/7', '2018-06-22 04:47:58', '[]', '[]'),
(269, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-06-23 03:35:54', '{"user_name":"admin","password":"12345678"}', '[]'),
(270, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-06-23 06:30:06', '{"user_name":"admin","password":"12345678"}', '[]'),
(271, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-07-05 05:51:50', '{"user_name":"admin","password":"12345678"}', '[]'),
(272, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-07-26 08:31:35', '{"user_name":"admin","password":"12345678"}', '[]'),
(273, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-07-26 11:42:05', '{"user_name":"admin","password":"12345678"}', '[]'),
(274, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-07-28 03:37:48', '{"user_name":"admin","password":"12345678"}', '[]'),
(275, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'edit_process', 'Success', 'msupplier', 'edit_process', 'http://localhost/ProduksiKM/index.php/msupplier/edit_process', '2018-07-28 03:47:44', '{"id":"3","kode":"KMGM","nama":"Kilang Mie Gunung Mas","alamat":"Jalan Raya Tanjung Morawa Km. 14.2 Dusun I Desa Bangun Sari Baru \\r\\nKecamatan Tanjung Morawa, Kabupaten Deli Serdang.\\r\\nSumatera Utara 20362 - Indonesia","telp":"62-61 7940385","fax":"62-61 7940384"}', '[]'),
(276, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'mnegara', 'create_process', 'http://localhost/ProduksiKM/index.php/mnegara/create_process', '2018-07-28 04:05:52', '{"kode":"SDY","nama":"SYDNEY"}', '[]'),
(277, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'edit_process', 'Success', 'mnegara', 'edit_process', 'http://localhost/ProduksiKM/index.php/mnegara/edit_process', '2018-07-28 04:06:58', '{"id":"5","kode":"SDY","nama":"SYDNEY - AUSTRALIA"}', '[]'),
(278, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'edit_process', 'Success', 'mnegara', 'edit_process', 'http://localhost/ProduksiKM/index.php/mnegara/edit_process', '2018-07-28 04:08:03', '{"id":"5","kode":"SDY","nama":"Sydney - Australia"}', '[]'),
(279, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-07-28 04:48:44', '{"jenis_dokumen":"Doc. Import","no_dokumen_pabean":"KM-17\\/WBC.02\\/2018","tanggal_dokumen_pabean":"2018-05-22","no_seri_barang":"11081100","no_bukti_penerimaan_barang":"SUDU38SYD007114X","tanggal_bukti_penerimaan_barang":"2018-07-30","bahan_baku":"8","jumlah":"22","mata_uang":"2","nilai_barang":"7.560","gudang":"1","negara_asal_barang":"5","supplier":"3","harga_satuan":"420"}', '[]'),
(280, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'mcustomer', 'create_process', 'http://localhost/ProduksiKM/index.php/mcustomer/create_process', '2018-07-28 05:23:47', '{"kode":"01","nama":"YUE GREAT CHOICE (SHENZHEN) TRADING CO.,LTD.","alamat":"SUIT 1106. MEILAN BUSINESS CENTER INTERCHANGE OF XIXIANG BOULEVARD AND QIANJIN 2ND ROAD XIXIANG STREET\\r\\nBAOAN DISTRICT, SHENZHEN CITY. GUANGDONG PROVINCE - CHINA","telp":"86-755-27380636","fax":"86755-27380636"}', '[]'),
(281, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'mgudang', 'delete_process', 'http://localhost/ProduksiKM/index.php/mgudang/delete_process/2', '2018-07-28 05:24:05', '[]', '[]'),
(282, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'mbwaste', 'delete_process', 'http://localhost/ProduksiKM/index.php/mbwaste/delete_process/1', '2018-07-28 05:25:51', '[]', '[]'),
(283, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'msatuan', 'delete_process', 'http://localhost/ProduksiKM/index.php/msatuan/delete_process/2', '2018-07-28 05:31:53', '[]', '[]'),
(284, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'msatuan', 'delete_process', 'http://localhost/ProduksiKM/index.php/msatuan/delete_process/3', '2018-07-28 05:31:59', '[]', '[]'),
(285, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'mmuang', 'delete_process', 'http://localhost/ProduksiKM/index.php/mmuang/delete_process/5', '2018-07-28 05:32:20', '[]', '[]'),
(286, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'mnegara', 'delete_process', 'http://localhost/ProduksiKM/index.php/mnegara/delete_process/4', '2018-07-28 05:32:43', '[]', '[]'),
(287, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'edit_process', 'Success', 'mnegara', 'edit_process', 'http://localhost/ProduksiKM/index.php/mnegara/edit_process', '2018-07-28 05:33:30', '{"id":"3","kode":"MY","nama":"Malasyia"}', '[]'),
(288, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'mnegara', 'delete_process', 'http://localhost/ProduksiKM/index.php/mnegara/delete_process/1', '2018-07-28 05:33:43', '[]', '[]'),
(289, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'mnegara', 'delete_process', 'http://localhost/ProduksiKM/index.php/mnegara/delete_process/2', '2018-07-28 05:33:52', '[]', '[]'),
(290, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-08-03 05:44:26', '{"user_name":"admin","password":"12345678"}', '[]'),
(291, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-08-03 09:39:25', '{"user_name":"admin","password":"12345678"}', '[]'),
(292, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-08-14 05:42:21', '{"user_name":"admin","password":"12345678"}', '[]'),
(293, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'tpemasukanbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/delete_process/29', '2018-08-14 05:53:59', '[]', '[]'),
(294, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-08-14 06:30:37', '{"jenis_dokumen":"Dok. Import","no_dokumen_pabean":"010700-000366-20180727-001280","tanggal_dokumen_pabean":"2018-07-27","no_seri_barang":"11081100","no_bukti_penerimaan_barang":"SUDU38SYD007114X","tanggal_bukti_penerimaan_barang":"2018-07-30","bahan_baku":"8","jumlah":"20","mata_uang":"2","nilai_barang":"7,560.00","gudang":"1","negara_asal_barang":"5","supplier":"3","harga_satuan":"420"}', '[]'),
(295, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-08-16 03:38:02', '{"user_name":"admin","password":"12345678"}', '[]'),
(296, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-08-16 04:16:38', '{"jenis_dokumen":"Dok. Import","no_dokumen_pabean":"010700-000366-20180731-001288","tanggal_dokumen_pabean":"2018-07-31","no_seri_barang":"11081300","no_bukti_penerimaan_barang":"HLCUDUS180521099","tanggal_bukti_penerimaan_barang":"2018-07-21","bahan_baku":"7","jumlah":"1.720","mata_uang":"2","nilai_barang":"33.970","gudang":"1","negara_asal_barang":"3","supplier":"3","harga_satuan":"0.7900"}', '[]'),
(297, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'tpemasukanbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/delete_process/31', '2018-08-16 04:18:47', '[]', '[]'),
(298, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-08-16 04:20:33', '{"jenis_dokumen":"Dok. Import","no_dokumen_pabean":"010700-000366-20180731-001288","tanggal_dokumen_pabean":"2018-07-31","no_seri_barang":"11081300","no_bukti_penerimaan_barang":"HLCUDUS180521099","tanggal_bukti_penerimaan_barang":"2018-07-21","bahan_baku":"7","jumlah":"1.720","mata_uang":"1","nilai_barang":"33.970","gudang":"1","negara_asal_barang":"3","supplier":"3","harga_satuan":"0.7900"}', '[]'),
(299, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'tpemasukanbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/delete_process/32', '2018-08-16 04:21:40', '[]', '[]'),
(300, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-08-16 04:23:10', '{"jenis_dokumen":"Dok. Import","no_dokumen_pabean":"010700-000366-20180731-001288","tanggal_dokumen_pabean":"2018-07-31","no_seri_barang":"11081300","no_bukti_penerimaan_barang":"HLCUDUS180521099","tanggal_bukti_penerimaan_barang":"2018-07-21","bahan_baku":"7","jumlah":"1720","mata_uang":"2","nilai_barang":"33.970","gudang":"1","negara_asal_barang":"3","supplier":"3","harga_satuan":"0.7900"}', '[]'),
(301, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'mnegara', 'create_process', 'http://localhost/ProduksiKM/index.php/mnegara/create_process', '2018-08-16 04:26:54', '{"kode":"AMS","nama":"Amsterdam"}', '[]'),
(302, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'mnegara', 'create_process', 'http://localhost/ProduksiKM/index.php/mnegara/create_process', '2018-08-16 04:28:06', '{"kode":"DNK","nama":"Denmark"}', '[]'),
(303, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'edit_process', 'Success', 'mnegara', 'edit_process', 'http://localhost/ProduksiKM/index.php/mnegara/edit_process', '2018-08-16 04:28:21', '{"id":"7","kode":"DMK","nama":"Denmark"}', '[]'),
(304, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'tpenyelesaianbwaste', 'delete_process', 'http://localhost/ProduksiKM/index.php/tpenyelesaianbwaste/delete_process/6', '2018-08-16 04:38:13', '[]', '[]'),
(305, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'delete_process', 'Success', 'mcustomer', 'delete_process', 'http://localhost/ProduksiKM/index.php/mcustomer/delete_process/1', '2018-08-16 04:41:03', '[]', '[]'),
(306, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'mbwaste', 'create_process', 'http://localhost/ProduksiKM/index.php/mbwaste/create_process', '2018-08-16 04:42:39', '{"kode":"PS","nama":"Potato Starch","satuan":"1","harga":"0.7900"}', '[]'),
(307, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'mbwaste', 'create_process', 'http://localhost/ProduksiKM/index.php/mbwaste/create_process', '2018-08-16 04:44:40', '{"kode":"WS","nama":"Wheat Starch","satuan":"1","harga":"420"}', '[]'),
(308, '127.0.0.1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-08-21 04:27:21', '{"user_name":"admin","password":"12345678"}', '[]'),
(309, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-08-21 10:51:45', '{"user_name":"admin","password":"12345678"}', '[]'),
(310, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-08-23 03:18:42', '{"user_name":"admin","password":"12345678"}', '[]'),
(311, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-08-23 11:18:35', '{"user_name":"admin","password":"12345678"}', '[]'),
(312, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-08-27 11:26:24', '{"user_name":"admin","password":"12345678"}', '[]'),
(313, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'edit_process', 'Success', 'mbwaste', 'edit_process', 'http://localhost/ProduksiKM/index.php/mbwaste/edit_process', '2018-08-27 11:31:13', '{"id":"2","kode":"PS","nama":"Potato Starch","satuan":"1","harga":"0.7900"}', '[]'),
(314, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-09-01 06:43:17', '{"user_name":"admin","password":"12345678"}', '[]'),
(315, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-09-01 10:41:57', '{"user_name":"admin","password":"12345678"}', '[]'),
(316, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-09-06 08:30:46', '{"user_name":"admin","password":"12345678"}', '[]'),
(317, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-09-07 03:49:15', '{"user_name":"admin","password":"12345678"}', '[]'),
(318, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-09-07 09:19:00', '{"user_name":"admin","password":"12345678"}', '[]'),
(319, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/create_process', '2018-09-07 09:22:26', '{"jenis_dokumen":"Dok. Import","no_dokumen_pabean":"010700-000366-20180811-001321","tanggal_dokumen_pabean":"2018-08-11","no_seri_barang":"11081300","no_bukti_penerimaan_barang":"OOLU4026943690","tanggal_bukti_penerimaan_barang":"2018-09-05","bahan_baku":"7","jumlah":"1600","mata_uang":"2","nilai_barang":"30,000","gudang":"1","negara_asal_barang":"6","supplier":"3","harga_satuan":"0,75"}', '[]'),
(320, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-09-19 04:29:51', '{"user_name":"admin","password":"12345678"}', '[]'),
(321, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-09-19 04:32:59', '{"id":"34","no_transaksi":"IBB20180900001","tanggal":"2018-09-07 09:22:26","jenis_dokumen":"Dok. Import","no_dokumen_pabean":"010700-000366-20180811-001321","tanggal_dokumen_pabean":"2018-08-11 00:00:00","no_seri_barang":"11081300","no_bukti_penerimaan_barang":"OOLU4026943690","tanggal_bukti_penerimaan_barang":"2018-09-05 00:00:00","bahan_baku":"7","jumlah":"1600","mata_uang":"2","nilai_barang":"30000","gudang":"1","negara_asal_barang":"6","supplier":"3","harga_satuan":"18.75"}', '[]'),
(322, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-09-22 02:58:08', '{"user_name":"admin","password":"12345678"}', '[]'),
(323, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-09-22 03:03:58', '{"id":"33","no_transaksi":"IBB20180800004","tanggal":"2018-08-16 04:23:10","jenis_dokumen":"Dok. Import","no_dokumen_pabean":"010700-000366-20180731-001288","tanggal_dokumen_pabean":"2018-07-31 00:00:00","no_seri_barang":"11081300","no_bukti_penerimaan_barang":"HLCUDUS180521099","tanggal_bukti_penerimaan_barang":"2018-07-21 00:00:00","bahan_baku":"7","jumlah":"1720","mata_uang":"2","nilai_barang":"33.970","gudang":"1","negara_asal_barang":"3","supplier":"3","harga_satuan":"19,75"}', '[]'),
(324, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbbaku/confirm_process', '2018-09-22 03:11:03', '{"id":"30","no_transaksi":"IBB20180800001","tanggal":"2018-08-14 06:30:37","jenis_dokumen":"Dok. Import","no_dokumen_pabean":"010700-000366-20180727-001280","tanggal_dokumen_pabean":"2018-07-27 00:00:00","no_seri_barang":"11081100","no_bukti_penerimaan_barang":"SUDU38SYD007114X","tanggal_bukti_penerimaan_barang":"2018-07-30 00:00:00","bahan_baku":"8","jumlah":"20","mata_uang":"2","nilai_barang":"7560.00","gudang":"1","negara_asal_barang":"5","supplier":"3","harga_satuan":"10,5"}', '[]'),
(325, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:18:04', '{"no_bukti_pengeluaran":"01","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(326, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:18:24', '{"id":"1","no_transaksi":"OBB20180900001","tanggal":"2018-09-22 03:18:04","no_bukti_pengeluaran":"01","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(327, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:19:02', '{"no_bukti_pengeluaran":"02","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(328, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:19:08', '{"id":"2","no_transaksi":"OBB20180900002","tanggal":"2018-09-22 03:19:02","no_bukti_pengeluaran":"02","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(329, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:19:31', '{"no_bukti_pengeluaran":"03","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(330, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:19:37', '{"id":"3","no_transaksi":"OBB20180900003","tanggal":"2018-09-22 03:19:31","no_bukti_pengeluaran":"03","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(331, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:21:08', '{"no_bukti_pengeluaran":"04","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(332, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:21:26', '{"no_bukti_pengeluaran":"05","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(333, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:21:55', '{"no_bukti_pengeluaran":"06","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(334, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:22:22', '{"no_bukti_pengeluaran":"07","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(335, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:22:57', '{"id":"4","no_transaksi":"OBB20180900004","tanggal":"2018-09-22 03:21:08","no_bukti_pengeluaran":"04","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(336, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:23:18', '{"no_bukti_pengeluaran":"08","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(337, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:23:36', '{"no_bukti_pengeluaran":"09","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(338, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:24:22', '{"no_bukti_pengeluaran":"010","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(339, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:25:10', '{"no_bukti_pengeluaran":"011","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(340, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:25:28', '{"no_bukti_pengeluaran":"012","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(341, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:25:59', '{"no_bukti_pengeluaran":"013","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(342, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:27:25', '{"no_bukti_pengeluaran":"014","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(343, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:27:46', '{"no_bukti_pengeluaran":"015","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(344, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:28:09', '{"no_bukti_pengeluaran":"016","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(345, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:28:27', '{"id":"16","no_transaksi":"OBB20180900016","tanggal":"2018-09-22 03:28:09","no_bukti_pengeluaran":"016","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(346, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:28:34', '{"id":"15","no_transaksi":"OBB20180900015","tanggal":"2018-09-22 03:27:46","no_bukti_pengeluaran":"015","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(347, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:28:40', '{"id":"14","no_transaksi":"OBB20180900014","tanggal":"2018-09-22 03:27:25","no_bukti_pengeluaran":"014","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(348, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:28:56', '{"id":"13","no_transaksi":"OBB20180900013","tanggal":"2018-09-22 03:25:59","no_bukti_pengeluaran":"013","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(349, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:29:02', '{"id":"12","no_transaksi":"OBB20180900012","tanggal":"2018-09-22 03:25:28","no_bukti_pengeluaran":"012","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(350, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:29:11', '{"id":"11","no_transaksi":"OBB20180900011","tanggal":"2018-09-22 03:25:10","no_bukti_pengeluaran":"011","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(351, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:29:23', '{"id":"10","no_transaksi":"OBB20180900010","tanggal":"2018-09-22 03:24:22","no_bukti_pengeluaran":"010","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(352, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:29:31', '{"id":"9","no_transaksi":"OBB20180900009","tanggal":"2018-09-22 03:23:36","no_bukti_pengeluaran":"09","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(353, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:29:38', '{"id":"8","no_transaksi":"OBB20180900008","tanggal":"2018-09-22 03:23:18","no_bukti_pengeluaran":"08","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(354, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:29:46', '{"id":"7","no_transaksi":"OBB20180900007","tanggal":"2018-09-22 03:22:22","no_bukti_pengeluaran":"07","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(355, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:29:57', '{"id":"6","no_transaksi":"OBB20180900006","tanggal":"2018-09-22 03:21:55","no_bukti_pengeluaran":"06","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(356, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:30:11', '{"id":"5","no_transaksi":"OBB20180900005","tanggal":"2018-09-22 03:21:26","no_bukti_pengeluaran":"05","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(357, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:31:05', '{"no_bukti_pengeluaran":"01","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(358, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:31:24', '{"no_bukti_pengeluaran":"02","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(359, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:32:35', '{"no_bukti_pengeluaran":"03","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"8","gudang":"1"}', '[]'),
(360, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:32:49', '{"id":"19","no_transaksi":"OBB20180900019","tanggal":"2018-09-22 03:32:35","no_bukti_pengeluaran":"03","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(361, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:33:04', '{"id":"18","no_transaksi":"OBB20180900018","tanggal":"2018-09-22 03:31:24","no_bukti_pengeluaran":"02","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(362, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:33:11', '{"id":"17","no_transaksi":"OBB20180900017","tanggal":"2018-09-22 03:31:05","no_bukti_pengeluaran":"01","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(363, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:34:08', '{"no_bukti_pengeluaran":"04","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(364, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:34:15', '{"id":"20","no_transaksi":"OBB20180900020","tanggal":"2018-09-22 03:34:08","no_bukti_pengeluaran":"04","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(365, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:34:32', '{"no_bukti_pengeluaran":"05","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(366, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:35:11', '{"no_bukti_pengeluaran":"06","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(367, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:35:24', '{"id":"21","no_transaksi":"OBB20180900021","tanggal":"2018-09-22 03:34:32","no_bukti_pengeluaran":"05","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(368, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:35:34', '{"id":"22","no_transaksi":"OBB20180900022","tanggal":"2018-09-22 03:35:11","no_bukti_pengeluaran":"06","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(369, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:35:50', '{"no_bukti_pengeluaran":"07","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(370, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:35:57', '{"id":"23","no_transaksi":"OBB20180900023","tanggal":"2018-09-22 03:35:50","no_bukti_pengeluaran":"07","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(371, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:36:20', '{"no_bukti_pengeluaran":"08","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(372, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:36:28', '{"id":"24","no_transaksi":"OBB20180900024","tanggal":"2018-09-22 03:36:20","no_bukti_pengeluaran":"08","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(373, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:37:07', '{"no_bukti_pengeluaran":"09","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(374, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:37:19', '{"id":"25","no_transaksi":"OBB20180900025","tanggal":"2018-09-22 03:37:07","no_bukti_pengeluaran":"09","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(375, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:37:36', '{"no_bukti_pengeluaran":"010","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(376, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:37:53', '{"no_bukti_pengeluaran":"011","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(377, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:38:02', '{"id":"26","no_transaksi":"OBB20180900026","tanggal":"2018-09-22 03:37:36","no_bukti_pengeluaran":"010","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(378, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:38:11', '{"id":"27","no_transaksi":"OBB20180900027","tanggal":"2018-09-22 03:37:53","no_bukti_pengeluaran":"011","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(379, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:38:32', '{"no_bukti_pengeluaran":"012","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(380, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:38:41', '{"id":"28","no_transaksi":"OBB20180900028","tanggal":"2018-09-22 03:38:32","no_bukti_pengeluaran":"012","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(381, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:39:01', '{"no_bukti_pengeluaran":"013","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(382, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:39:17', '{"id":"29","no_transaksi":"OBB20180900029","tanggal":"2018-09-22 03:39:01","no_bukti_pengeluaran":"013","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(383, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:39:34', '{"no_bukti_pengeluaran":"014","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(384, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:39:46', '{"id":"30","no_transaksi":"OBB20180900030","tanggal":"2018-09-22 03:39:34","no_bukti_pengeluaran":"014","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(385, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:40:55', '{"no_bukti_pengeluaran":"015","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(386, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:41:14', '{"no_bukti_pengeluaran":"016","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]');
INSERT INTO `core_app_log` (`id`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `device_id`, `user_id`, `event`, `message`, `module`, `action`, `link`, `created_on`, `input_data`, `json_return`) VALUES
(387, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:41:21', '{"id":"31","no_transaksi":"OBB20180900031","tanggal":"2018-09-22 03:40:55","no_bukti_pengeluaran":"015","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(388, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:41:27', '{"id":"32","no_transaksi":"OBB20180900032","tanggal":"2018-09-22 03:41:14","no_bukti_pengeluaran":"016","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(389, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:43:13', '{"no_bukti_pengeluaran":"017","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(390, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:43:37', '{"no_bukti_pengeluaran":"018","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(391, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:44:13', '{"no_bukti_pengeluaran":"019","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(392, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:44:50', '{"no_bukti_pengeluaran":"020","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(393, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:45:30', '{"no_bukti_pengeluaran":"021","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(394, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:46:28', '{"no_bukti_pengeluaran":"022","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(395, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:46:58', '{"no_bukti_pengeluaran":"023","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(396, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:50:01', '{"no_bukti_pengeluaran":"024","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(397, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:50:46', '{"no_bukti_pengeluaran":"025","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(398, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:51:12', '{"no_bukti_pengeluaran":"026","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(399, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:51:45', '{"id":"33","no_transaksi":"OBB20180900033","tanggal":"2018-09-22 03:43:13","no_bukti_pengeluaran":"017","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(400, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:51:52', '{"id":"34","no_transaksi":"OBB20180900034","tanggal":"2018-09-22 03:43:37","no_bukti_pengeluaran":"018","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(401, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:51:59', '{"id":"35","no_transaksi":"OBB20180900035","tanggal":"2018-09-22 03:44:13","no_bukti_pengeluaran":"019","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(402, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:52:05', '{"id":"36","no_transaksi":"OBB20180900036","tanggal":"2018-09-22 03:44:50","no_bukti_pengeluaran":"020","tanggal_bukti_pengeluaran":"2018-09-19 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(403, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:52:12', '{"id":"37","no_transaksi":"OBB20180900037","tanggal":"2018-09-22 03:45:30","no_bukti_pengeluaran":"021","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(404, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:52:24', '{"id":"38","no_transaksi":"OBB20180900038","tanggal":"2018-09-22 03:46:28","no_bukti_pengeluaran":"022","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(405, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:52:31', '{"id":"39","no_transaksi":"OBB20180900039","tanggal":"2018-09-22 03:46:58","no_bukti_pengeluaran":"023","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(406, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:52:39', '{"id":"40","no_transaksi":"OBB20180900040","tanggal":"2018-09-22 03:50:01","no_bukti_pengeluaran":"024","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(407, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:52:45', '{"id":"41","no_transaksi":"OBB20180900041","tanggal":"2018-09-22 03:50:46","no_bukti_pengeluaran":"025","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(408, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:52:51', '{"id":"42","no_transaksi":"OBB20180900042","tanggal":"2018-09-22 03:51:12","no_bukti_pengeluaran":"026","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(409, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:57:12', '{"no_bukti_pengeluaran":"017","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(410, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:57:30', '{"no_bukti_pengeluaran":"018","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(411, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:57:55', '{"no_bukti_pengeluaran":"019","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(412, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:58:14', '{"no_bukti_pengeluaran":"020","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(413, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:58:35', '{"no_bukti_pengeluaran":"021","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(414, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:59:14', '{"no_bukti_pengeluaran":"022","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"7","jumlah":"8","gudang":"1"}', '[]'),
(415, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:59:24', '{"id":"48","no_transaksi":"OBB20180900048","tanggal":"2018-09-22 03:59:14","no_bukti_pengeluaran":"022","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(416, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 03:59:32', '{"id":"47","no_transaksi":"OBB20180900047","tanggal":"2018-09-22 03:58:35","no_bukti_pengeluaran":"021","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(417, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 03:59:48', '{"no_bukti_pengeluaran":"023","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(418, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 04:00:14', '{"no_bukti_pengeluaran":"024","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(419, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 04:00:40', '{"no_bukti_pengeluaran":"025","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(420, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 04:00:57', '{"no_bukti_pengeluaran":"026","tanggal_bukti_pengeluaran":"2018-09-20","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(421, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 04:01:49', '{"id":"43","no_transaksi":"OBB20180900043","tanggal":"2018-09-22 03:57:12","no_bukti_pengeluaran":"017","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(422, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 04:01:55', '{"id":"44","no_transaksi":"OBB20180900044","tanggal":"2018-09-22 03:57:30","no_bukti_pengeluaran":"018","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(423, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 04:02:02', '{"id":"45","no_transaksi":"OBB20180900045","tanggal":"2018-09-22 03:57:55","no_bukti_pengeluaran":"019","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(424, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 04:02:09', '{"id":"46","no_transaksi":"OBB20180900046","tanggal":"2018-09-22 03:58:14","no_bukti_pengeluaran":"020","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(425, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 04:02:15', '{"id":"49","no_transaksi":"OBB20180900049","tanggal":"2018-09-22 03:59:48","no_bukti_pengeluaran":"023","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(426, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 04:02:26', '{"id":"50","no_transaksi":"OBB20180900050","tanggal":"2018-09-22 04:00:14","no_bukti_pengeluaran":"024","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(427, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 04:02:32', '{"id":"51","no_transaksi":"OBB20180900051","tanggal":"2018-09-22 04:00:40","no_bukti_pengeluaran":"025","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(428, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemakaianbbaku', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/confirm_process', '2018-09-22 04:02:39', '{"id":"52","no_transaksi":"OBB20180900052","tanggal":"2018-09-22 04:00:57","no_bukti_pengeluaran":"026","tanggal_bukti_pengeluaran":"2018-09-20 00:00:00","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(429, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemasukanbjadi', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbjadi/create_process', '2018-09-22 04:38:28', '{"no_bukti_penerimaan":"H01","tanggal_bukti_penerimaan":"2018-09-19","barang_jadi":"2","jumlah":"25","gudang":"1","no_pemakaian_bahan_baku":["OBB20180900001","OBB20180900002","OBB20180900003","OBB20180900004","OBB20180900005","OBB20180900006","OBB20180900007","OBB20180900008","OBB20180900009","OBB20180900010","OBB20180900011","OBB20180900012","OBB20180900013","OBB20180900014","OBB20180900015","OBB20180900016","OBB20180900017","OBB20180900018","OBB20180900019","OBB20180900020","OBB20180900021","OBB20180900022","OBB20180900023","OBB20180900024","OBB20180900025","OBB20180900026","OBB20180900027","OBB20180900028","OBB20180900029","OBB20180900030","OBB20180900031","OBB20180900032","OBB20180900033","OBB20180900034","OBB20180900035","OBB20180900036","OBB20180900037","OBB20180900038","OBB20180900039","OBB20180900040","OBB20180900041","OBB20180900042","OBB20180900043","OBB20180900044","OBB20180900045","OBB20180900046","OBB20180900047","OBB20180900048","OBB20180900049","OBB20180900050","OBB20180900051","OBB20180900052"]}', '[]'),
(430, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'confirm_process', 'Success', 'tpemasukanbjadi', 'confirm_process', 'http://localhost/ProduksiKM/index.php/tpemasukanbjadi/confirm_process', '2018-09-22 04:40:43', '{"id":"1","no_transaksi":"IBJ20180900001","tanggal":"2018-09-22 04:38:28","no_bukti_penerimaan":"H01","tanggal_bukti_penerimaan":"2018-09-20","barang_jadi":"2","jumlah":"25","gudang":"1","no_pemakaian_bahan_baku":["OBB20180900001","OBB20180900002","OBB20180900003","OBB20180900004","OBB20180900005","OBB20180900006","OBB20180900007","OBB20180900008","OBB20180900009","OBB20180900010","OBB20180900011","OBB20180900012","OBB20180900013","OBB20180900014","OBB20180900015","OBB20180900016","OBB20180900017","OBB20180900018","OBB20180900019","OBB20180900020","OBB20180900021","OBB20180900022","OBB20180900023","OBB20180900024","OBB20180900025","OBB20180900026","OBB20180900027","OBB20180900028","OBB20180900029","OBB20180900030","OBB20180900031","OBB20180900032","OBB20180900033","OBB20180900034","OBB20180900035","OBB20180900036","OBB20180900037","OBB20180900038","OBB20180900039","OBB20180900040","OBB20180900041","OBB20180900042","OBB20180900043","OBB20180900044","OBB20180900045","OBB20180900046","OBB20180900047","OBB20180900048","OBB20180900049","OBB20180900050","OBB20180900051","OBB20180900052"]}', '[]'),
(431, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:12:30', '{"no_bukti_pengeluaran":"PB01","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(432, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:13:16', '{"no_bukti_pengeluaran":"PB02","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(433, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'delete_process', 'Success', 'tpemakaianbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/delete_process/53', '2018-09-22 08:13:26', '[]', '[]'),
(434, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:13:54', '{"no_bukti_pengeluaran":"PB02","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(435, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:14:50', '{"no_bukti_pengeluaran":"PB03","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(436, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:15:54', '{"no_bukti_pengeluaran":"PB04","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(437, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:16:22', '{"no_bukti_pengeluaran":"PB05","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"8","gudang":"1"}', '[]'),
(438, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'delete_process', 'Success', 'tpemakaianbbaku', 'delete_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/delete_process/58', '2018-09-22 08:16:31', '[]', '[]'),
(439, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:20:19', '{"no_bukti_pengeluaran":"PB06","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(440, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:20:51', '{"no_bukti_pengeluaran":"PB07","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(441, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:21:26', '{"no_bukti_pengeluaran":"PB08","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(442, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:22:01', '{"no_bukti_pengeluaran":"PB09","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(443, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:22:35', '{"no_bukti_pengeluaran":"PB10","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(444, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:22:57', '{"no_bukti_pengeluaran":"PB11","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(445, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:23:52', '{"no_bukti_pengeluaran":"PB12","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(446, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:24:14', '{"no_bukti_pengeluaran":"PB13","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(447, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:24:40', '{"no_bukti_pengeluaran":"PB14","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"7","jumlah":"32","gudang":"1"}', '[]'),
(448, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', 1, 'create_process', 'Success', 'tpemakaianbbaku', 'create_process', 'http://localhost/ProduksiKM/index.php/tpemakaianbbaku/create_process', '2018-09-22 08:24:57', '{"no_bukti_pengeluaran":"PB15","tanggal_bukti_pengeluaran":"2018-09-19","bahan_baku":"8","jumlah":"8","gudang":"1"}', '[]'),
(449, '::1', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', '', '', NULL, 'login_process', 'Success', 'home', 'login_process', 'http://localhost/ProduksiKM/index.php/home/login_process', '2018-09-24 06:29:58', '{"user_name":"admin","password":"12345678"}', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `core_content`
--

CREATE TABLE IF NOT EXISTS `core_content` (
  `id` int(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `record_type` int(10) DEFAULT NULL,
  `text_01` varchar(500) DEFAULT NULL,
  `text_02` varchar(500) DEFAULT NULL,
  `text_03` varchar(500) DEFAULT NULL,
  `text_04` varchar(500) DEFAULT NULL,
  `text_05` varchar(500) DEFAULT NULL,
  `text_06` varchar(500) DEFAULT NULL,
  `text_07` varchar(500) DEFAULT NULL,
  `text_08` varchar(500) DEFAULT NULL,
  `text_09` varchar(500) DEFAULT NULL,
  `int_01` int(10) DEFAULT NULL,
  `int_02` int(10) DEFAULT NULL,
  `int_03` int(10) DEFAULT NULL,
  `int_04` int(10) DEFAULT NULL,
  `int_05` int(10) DEFAULT NULL,
  `int_06` int(10) DEFAULT NULL,
  `int_07` int(10) DEFAULT NULL,
  `int_08` int(10) DEFAULT NULL,
  `int_09` int(10) DEFAULT NULL,
  `note_01` text,
  `note_02` text,
  `note_03` text,
  `note_04` text,
  `note_05` text,
  `note_06` text,
  `note_07` text,
  `note_08` text,
  `note_09` text,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `core_email_log`
--

CREATE TABLE IF NOT EXISTS `core_email_log` (
  `id` int(10) NOT NULL,
  `mail_from` varchar(255) DEFAULT NULL,
  `mail_from_name` varchar(255) DEFAULT NULL,
  `mail_to` text,
  `mail_cc` text,
  `mail_bcc` text,
  `mail_subject` varchar(255) DEFAULT NULL,
  `mail_body` text,
  `mail_error` text,
  `mail_error_info` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_sessions`
--

CREATE TABLE IF NOT EXISTS `data_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_sessions`
--

INSERT INTO `data_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('753ee1aa5238959242c2821350aeee18', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', 1537764219, 'a:4:{s:9:"user_data";s:0:"";s:19:"CLIENTSESSIONENABLE";i:1;s:15:"session_user_id";s:1:"1";s:17:"session_user_name";s:5:"admin";}');

-- --------------------------------------------------------

--
-- Table structure for table `master_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `master_bahan_baku` (
  `id` int(10) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `satuan` int(10) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_bahan_baku`
--

INSERT INTO `master_bahan_baku` (`id`, `kode`, `nama`, `satuan`, `harga`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES
(1, 'TPG', 'Tepung', 2, '5000', '', 1, '2018-03-13 00:59:33', 1, '2018-05-21 05:18:53', 'DELETED'),
(2, 'TRG', 'Terigu', 1, '10000', '', 1, '2018-03-19 20:52:08', 1, '2018-05-21 09:16:44', 'DELETED'),
(3, 'GRM', 'Garam', 1, '14000', '', 1, '2018-03-19 20:52:24', 1, '2018-05-21 09:16:53', 'DELETED'),
(4, 'CASANA', 'Casana ', 1, '111111', '', 1, '2018-03-23 21:07:31', 1, '2018-05-21 09:20:52', 'DELETED'),
(5, 'GULA', 'Gula', 3, '6000', '', 1, '2018-03-28 04:47:15', 1, '2018-05-21 09:20:57', 'DELETED'),
(6, 'TEST', 'TEST', 6, '10000', '', 1, '2018-05-12 06:10:58', 1, '2018-05-21 09:21:05', 'DELETED'),
(7, 'PS', 'Pati Kentang', 6, '790', '', 1, '2018-05-21 04:17:50', NULL, NULL, 'ACTIVE'),
(8, 'WS', 'Pati Gandum', 6, '420', '', 1, '2018-05-21 04:19:23', NULL, NULL, 'ACTIVE'),
(9, 'CASS', 'Pati Ubi Kayu', 6, '550', '', 1, '2018-05-21 04:20:07', 1, '2018-05-21 04:31:00', 'ACTIVE'),
(10, 'CS', 'Pati Jagung', 6, '400', '', 1, '2018-05-21 04:22:05', NULL, NULL, 'ACTIVE'),
(11, 'MCASS', 'Pati Ubi Kayu Modifikasi', 6, '700', '', 1, '2018-05-21 04:23:37', 1, '2018-05-21 04:30:26', 'DELETED'),
(12, 'MCASS', 'Pati Ubi Kayu Modifikasi', 6, '700', '', 1, '2018-05-21 04:30:08', NULL, NULL, 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `master_barang_jadi`
--

CREATE TABLE IF NOT EXISTS `master_barang_jadi` (
  `id` int(10) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `satuan` int(10) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_barang_jadi`
--

INSERT INTO `master_barang_jadi` (`id`, `kode`, `nama`, `satuan`, `harga`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES
(1, 'BHN1', 'Bihun 1', 1, '222', '', 1, '2018-03-13 01:03:13', 1, '2018-05-21 09:21:29', 'DELETED'),
(2, 'SPSB1', 'Super Potato Starch B1', 6, '795.00', '', 1, '2018-03-23 21:17:32', 1, '2018-05-23 04:15:01', 'ACTIVE'),
(3, 'MIAC', 'Mie Aceh', 3, '50000', '', 1, '2018-03-26 00:25:54', 1, '2018-05-21 09:21:37', 'DELETED'),
(4, 'SPSC1', 'Super Potato Starch C1', 6, '765.00', '', 1, '2018-05-22 08:57:51', 1, '2018-05-23 04:15:11', 'ACTIVE'),
(5, 'SPSD1', 'Super Potato Starch D1', 6, '783.50', '', 1, '2018-05-22 08:59:38', NULL, NULL, 'ACTIVE'),
(6, 'SPSE1', 'Super Potato Starch E1', 6, '728.00', '', 1, '2018-05-22 09:35:07', NULL, NULL, 'ACTIVE'),
(7, 'SPSF1', 'Super Potato Starch F1', 6, '741.00', '', 1, '2018-05-22 09:38:55', 1, '2018-05-23 04:15:27', 'ACTIVE'),
(8, 'SPSG1', 'Super Potato Starch G1', 6, '691.00', '', 1, '2018-05-22 09:40:28', 1, '2018-05-23 04:15:40', 'ACTIVE'),
(9, 'SPSH1', 'Super Potato Starch H1', 6, '819.50', '', 1, '2018-05-22 10:17:20', 1, '2018-05-23 04:16:07', 'ACTIVE'),
(10, 'SPSI1', 'Super Potato Starch I1', 6, '826.00', '', 1, '2018-05-23 04:13:07', 1, '2018-05-23 04:16:17', 'ACTIVE'),
(11, 'SPSJ1', 'Super Potato Starch J1', 6, '814.00', '', 1, '2018-05-23 04:13:59', 1, '2018-05-23 04:16:29', 'ACTIVE'),
(12, 'SPSK1', 'Super Potato Starch K1', 6, '802.00', '', 1, '2018-05-23 04:17:08', NULL, NULL, 'ACTIVE'),
(13, 'SPSL1', 'Super Potato Starch L1', 6, '794.50', '', 1, '2018-05-23 08:12:53', NULL, NULL, 'ACTIVE'),
(14, 'SPSM1', 'Super Potato Starch M1', 6, '787.00', '', 1, '2018-05-23 08:15:28', NULL, NULL, 'ACTIVE'),
(15, 'SPSN1', 'Super Potato StarchN1', 6, '802.00', '', 1, '2018-05-23 08:16:54', NULL, NULL, 'ACTIVE'),
(16, 'SPSO1', 'Super Potato Strach O1', 6, '797.50', '', 1, '2018-05-23 08:18:35', NULL, NULL, 'ACTIVE'),
(17, 'SPSP1', 'Super Potato Starch P1', 6, '813.00', '', 1, '2018-05-23 08:19:21', NULL, NULL, 'ACTIVE'),
(18, 'SPSQ1', 'Super Potato Starch Q1', 6, '776.00', '', 1, '2018-05-23 08:20:04', NULL, NULL, 'ACTIVE'),
(19, 'SPSR1', 'Super Potato Starch R1', 6, '739.00', '', 1, '2018-05-23 08:21:59', NULL, NULL, 'ACTIVE'),
(20, 'MPSX1', 'Modified Potato Starch X1', 6, '850.00', '', 1, '2018-05-24 03:36:54', NULL, NULL, 'ACTIVE'),
(21, 'MPSX2', 'Modified Potato Starch X2', 6, '826.00', '', 1, '2018-05-24 03:37:28', 1, '2018-05-24 03:44:45', 'ACTIVE'),
(22, 'MPSX3', 'Modified Potato Starch X3', 6, '813.00', '', 1, '2018-05-24 03:45:37', NULL, NULL, 'ACTIVE'),
(23, 'MPSX4', 'Modified Potato Starch X4', 6, '841.00', '', 1, '2018-05-24 03:46:26', NULL, NULL, 'ACTIVE'),
(24, 'MPSX5', 'Modified Potato Starch X5', 6, '809.50', '', 1, '2018-05-24 03:47:03', NULL, NULL, 'ACTIVE'),
(25, 'MPSX6', 'Modified Potato Starch X6', 6, '789.00', '', 1, '2018-05-24 03:47:38', NULL, NULL, 'ACTIVE'),
(26, 'MPSX7', 'Modified Potato Starch X7', 6, '702.00', '', 1, '2018-05-24 03:48:09', NULL, NULL, 'ACTIVE'),
(27, 'MPSX8', 'Modified Potato Starch X8', 6, '739.00', '', 1, '2018-05-24 03:48:46', NULL, NULL, 'ACTIVE'),
(28, 'MPSX9', 'Modified Potato Starch X9', 6, '678.00', '', 1, '2018-05-24 03:49:21', NULL, NULL, 'ACTIVE'),
(29, 'MPSX10', 'Modified Potato Starch X10', 6, '776.00', '', 1, '2018-05-24 03:50:05', NULL, NULL, 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `master_barang_waste`
--

CREATE TABLE IF NOT EXISTS `master_barang_waste` (
  `id` int(10) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `satuan` int(10) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_barang_waste`
--

INSERT INTO `master_barang_waste` (`id`, `kode`, `nama`, `satuan`, `harga`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES
(1, 'KRG', 'Karung', 2, '2000', '', 1, '2018-03-13 01:06:59', 1, '2018-07-28 05:25:51', 'DELETED'),
(2, 'PS', 'Potato Starch', 1, '0.7900', '', 1, '2018-08-16 04:42:39', 1, '2018-08-27 11:31:13', 'ACTIVE'),
(3, 'WS', 'Wheat Starch', 1, '420', '', 1, '2018-08-16 04:44:39', NULL, NULL, 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `master_customer`
--

CREATE TABLE IF NOT EXISTS `master_customer` (
  `id` int(10) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_customer`
--

INSERT INTO `master_customer` (`id`, `kode`, `nama`, `alamat`, `telp`, `fax`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES
(1, 'TDY', 'Teddy', 'Jalan Raden Saleh Dalam no 85', '08982874427', '081360149004', '', 1, '2018-03-12 23:14:53', 1, '2018-08-16 04:41:03', 'DELETED'),
(2, '01', 'YUE GREAT CHOICE SHENZHEN TRADING CO.LTD.', 'SUIT 1106. MEILAN BUSINESS CENTER INTERCHANGE OF XIXIANG BOULEVARD AND QIANJIN 2ND ROAD XIXIANG STREET\r\nBAOAN DISTRICT SHENZHEN CITY. GUANGDONG PROVINCE - CHINA', '86-755-27380636', '86755-27380636', '', 1, '2018-07-28 05:23:47', NULL, NULL, 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `master_gudang`
--

CREATE TABLE IF NOT EXISTS `master_gudang` (
  `id` int(10) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_gudang`
--

INSERT INTO `master_gudang` (`id`, `kode`, `nama`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES
(1, 'GD1', 'Gudang 1', '', 1, '2018-03-12 21:55:58', 1, '2018-03-12 21:56:06', 'ACTIVE'),
(2, 'GD2', 'Gudang 2', '', 1, '2018-03-19 20:52:37', 1, '2018-07-28 05:24:05', 'DELETED');

-- --------------------------------------------------------

--
-- Table structure for table `master_mata_uang`
--

CREATE TABLE IF NOT EXISTS `master_mata_uang` (
  `id` int(10) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_mata_uang`
--

INSERT INTO `master_mata_uang` (`id`, `kode`, `nama`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES
(1, 'RP', 'Rupiah', NULL, 0, '2018-01-29 20:33:26', NULL, NULL, 'ACTIVE'),
(2, 'USD', 'US Dollar', '', 1, '2018-03-12 21:49:32', 1, '2018-03-12 21:49:38', 'ACTIVE'),
(3, 'SGD', 'Singapore Dollar', '', 1, '2018-03-23 21:05:07', 1, '2018-03-23 21:05:14', 'DELETED'),
(4, 'SGD', 'Sgd', '', 1, '2018-04-01 19:58:17', 1, '2018-04-01 19:58:29', 'DELETED'),
(5, 'SGD', 'Singapore Dollar', '', 1, '2018-04-01 20:00:35', 1, '2018-07-28 05:32:20', 'DELETED');

-- --------------------------------------------------------

--
-- Table structure for table `master_negara`
--

CREATE TABLE IF NOT EXISTS `master_negara` (
  `id` int(10) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_negara`
--

INSERT INTO `master_negara` (`id`, `kode`, `nama`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES
(1, 'IDN', 'Indonesia', NULL, 0, '2018-01-29 20:35:24', 1, '2018-07-28 05:33:43', 'DELETED'),
(2, 'SG', 'Singapore', '', 1, '2018-03-12 21:52:59', 1, '2018-07-28 05:33:52', 'DELETED'),
(3, 'MY', 'Malasyia', '', 1, '2018-03-12 21:53:11', 1, '2018-07-28 05:33:30', 'ACTIVE'),
(4, 'CN', 'China', '', 1, '2018-04-01 20:22:43', 1, '2018-07-28 05:32:43', 'DELETED'),
(5, 'SDY', 'Sydney - Australia', '', 1, '2018-07-28 04:05:52', 1, '2018-07-28 04:08:03', 'ACTIVE'),
(6, 'AMS', 'Amsterdam', '', 1, '2018-08-16 04:26:54', NULL, NULL, 'ACTIVE'),
(7, 'DMK', 'Denmark', '', 1, '2018-08-16 04:28:06', 1, '2018-08-16 04:28:21', 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `master_satuan`
--

CREATE TABLE IF NOT EXISTS `master_satuan` (
  `id` int(10) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_satuan`
--

INSERT INTO `master_satuan` (`id`, `kode`, `nama`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES
(1, 'KG', 'Kilogram', NULL, 0, '2018-01-29 20:32:31', 1, '2018-03-12 21:05:19', 'ACTIVE'),
(2, 'GR', 'Gram', '', 1, '2018-03-12 06:31:41', 1, '2018-07-28 05:31:53', 'DELETED'),
(3, 'MG', 'Miligram', '', 1, '2018-03-12 06:39:02', 1, '2018-07-28 05:31:59', 'DELETED'),
(4, 'XXX', 'XXXXX', '', 1, '2018-03-12 06:50:16', 1, '2018-03-12 21:42:25', 'DELETED'),
(5, 'asdasdasda', 'asdasd', '', 1, '2018-03-12 07:00:46', 1, '2018-03-12 21:42:15', 'DELETED'),
(6, 'TON', 'Ton', '', 1, '2018-03-23 21:04:27', 1, '2018-04-01 19:39:09', 'ACTIVE'),
(7, 'SAK', 'Sak', '', 1, '2018-04-01 19:39:35', 1, '2018-06-22 04:47:58', 'DELETED'),
(8, 'GONI', 'Goni putih', '', 1, '2018-05-12 06:03:55', 1, '2018-05-12 06:04:39', 'DELETED');

-- --------------------------------------------------------

--
-- Table structure for table `master_stock_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `master_stock_bahan_baku` (
  `id_bahan_baku` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `import_flag` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_stock_bahan_baku`
--

INSERT INTO `master_stock_bahan_baku` (`id_bahan_baku`, `id_gudang`, `jumlah`, `import_flag`) VALUES
(1, 1, 812, ''),
(1, 2, 20, ''),
(2, 1, 15, ''),
(2, 2, 500, ''),
(3, 1, 21, ''),
(4, 1, 70, ''),
(5, 2, 1000, ''),
(6, 1, 400, ''),
(6, 2, 100, ''),
(7, 1, 9488, ''),
(8, 1, -188, '');

-- --------------------------------------------------------

--
-- Table structure for table `master_stock_barang_jadi`
--

CREATE TABLE IF NOT EXISTS `master_stock_barang_jadi` (
  `id_barang_jadi` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `import_flag` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_stock_barang_jadi`
--

INSERT INTO `master_stock_barang_jadi` (`id_barang_jadi`, `id_gudang`, `jumlah`, `import_flag`) VALUES
(1, 1, 4963, ''),
(2, 1, 525, ''),
(2, 2, 520, ''),
(3, 1, 5, '');

-- --------------------------------------------------------

--
-- Table structure for table `master_stock_barang_waste`
--

CREATE TABLE IF NOT EXISTS `master_stock_barang_waste` (
  `id_barang_waste` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `import_flag` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_stock_barang_waste`
--

INSERT INTO `master_stock_barang_waste` (`id_barang_waste`, `id_gudang`, `jumlah`, `import_flag`) VALUES
(1, 1, 150, '');

-- --------------------------------------------------------

--
-- Table structure for table `master_supplier`
--

CREATE TABLE IF NOT EXISTS `master_supplier` (
  `id` int(10) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_supplier`
--

INSERT INTO `master_supplier` (`id`, `kode`, `nama`, `alamat`, `telp`, `fax`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES
(1, 'KMTJ', 'Kilang Mie Tj Morawa', 'Jalan Tanjung Morawa Km..', '061 1111111', '061 2222', NULL, 1, '2018-03-12 23:01:55', 1, '2018-05-22 04:03:10', 'DELETED'),
(2, 'SP1', 'Supplier 1', 'Jalan Raden Saleh Dalam No 1', '061 4532658', '061 4553976', '', 1, '2018-03-12 23:08:51', 1, '2018-05-22 04:03:03', 'DELETED'),
(3, 'KMGM', 'Kilang Mie Gunung Mas', 'Jalan Raya Tanjung Morawa Km. 14.2 Dusun I Desa Bangun Sari Baru \r\nKecamatan Tanjung Morawa Kabupaten Deli Serdang.\r\nSumatera Utara 20362 - Indonesia', '62-61 7940385', '62-61 7940384', '', 1, '2018-05-21 04:50:24', 1, '2018-07-28 03:47:44', 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `master_user`
--

CREATE TABLE IF NOT EXISTS `master_user` (
  `id` int(10) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `password` char(64) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `token_cookie` varchar(64) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `user_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_user`
--

INSERT INTO `master_user` (`id`, `user_name`, `password`, `ip`, `token_cookie`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`, `user_status`) VALUES
(1, 'admin', '25d55ad283aa400af464c76d713c07ad', '::1', '', 0, '2018-03-09 22:50:01', 1, '2018-04-11 21:10:10', 'ACTIVE', 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `trans_inventory_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `trans_inventory_bahan_baku` (
  `id` int(10) NOT NULL,
  `id_bahan_baku` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `trans_date` datetime NOT NULL,
  `comment` text NOT NULL,
  `saldo_awal` int(10) NOT NULL DEFAULT '0',
  `jumlah_masuk` int(10) NOT NULL DEFAULT '0',
  `jumlah_keluar` int(10) NOT NULL DEFAULT '0',
  `saldo_akhir` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_inventory_bahan_baku`
--

INSERT INTO `trans_inventory_bahan_baku` (`id`, `id_bahan_baku`, `id_gudang`, `id_user`, `trans_date`, `comment`, `saldo_awal`, `jumlah_masuk`, `jumlah_keluar`, `saldo_akhir`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES
(1, 7, 1, 1, '2018-09-19 04:32:59', 'IBB20180900001', 7000, 1600, 0, 8600, 1, '2018-09-19 04:32:59', NULL, NULL, 'ACTIVE'),
(2, 7, 1, 1, '2018-09-22 03:03:58', 'IBB20180800004', 8600, 1720, 0, 10320, 1, '2018-09-22 03:03:58', NULL, NULL, 'ACTIVE'),
(3, 8, 1, 1, '2018-09-22 03:11:03', 'IBB20180800001', 0, 20, 0, 20, 1, '2018-09-22 03:11:03', NULL, NULL, 'ACTIVE'),
(4, 7, 1, 1, '2018-09-22 03:18:24', 'OBB20180900001', 10320, 0, 32, 10288, 1, '2018-09-22 03:18:24', NULL, NULL, 'ACTIVE'),
(5, 7, 1, 1, '2018-09-22 03:19:08', 'OBB20180900002', 10288, 0, 32, 10256, 1, '2018-09-22 03:19:08', NULL, NULL, 'ACTIVE'),
(6, 7, 1, 1, '2018-09-22 03:19:37', 'OBB20180900003', 10256, 0, 32, 10224, 1, '2018-09-22 03:19:37', NULL, NULL, 'ACTIVE'),
(7, 7, 1, 1, '2018-09-22 03:22:57', 'OBB20180900004', 10224, 0, 32, 10192, 1, '2018-09-22 03:22:57', NULL, NULL, 'ACTIVE'),
(8, 7, 1, 1, '2018-09-22 03:28:27', 'OBB20180900016', 10192, 0, 32, 10160, 1, '2018-09-22 03:28:27', NULL, NULL, 'ACTIVE'),
(9, 7, 1, 1, '2018-09-22 03:28:34', 'OBB20180900015', 10160, 0, 32, 10128, 1, '2018-09-22 03:28:34', NULL, NULL, 'ACTIVE'),
(10, 7, 1, 1, '2018-09-22 03:28:40', 'OBB20180900014', 10128, 0, 32, 10096, 1, '2018-09-22 03:28:40', NULL, NULL, 'ACTIVE'),
(11, 7, 1, 1, '2018-09-22 03:28:56', 'OBB20180900013', 10096, 0, 32, 10064, 1, '2018-09-22 03:28:56', NULL, NULL, 'ACTIVE'),
(12, 7, 1, 1, '2018-09-22 03:29:02', 'OBB20180900012', 10064, 0, 32, 10032, 1, '2018-09-22 03:29:02', NULL, NULL, 'ACTIVE'),
(13, 7, 1, 1, '2018-09-22 03:29:11', 'OBB20180900011', 10032, 0, 32, 10000, 1, '2018-09-22 03:29:11', NULL, NULL, 'ACTIVE'),
(14, 7, 1, 1, '2018-09-22 03:29:23', 'OBB20180900010', 10000, 0, 32, 9968, 1, '2018-09-22 03:29:23', NULL, NULL, 'ACTIVE'),
(15, 7, 1, 1, '2018-09-22 03:29:31', 'OBB20180900009', 9968, 0, 32, 9936, 1, '2018-09-22 03:29:31', NULL, NULL, 'ACTIVE'),
(16, 7, 1, 1, '2018-09-22 03:29:38', 'OBB20180900008', 9936, 0, 32, 9904, 1, '2018-09-22 03:29:38', NULL, NULL, 'ACTIVE'),
(17, 7, 1, 1, '2018-09-22 03:29:46', 'OBB20180900007', 9904, 0, 32, 9872, 1, '2018-09-22 03:29:46', NULL, NULL, 'ACTIVE'),
(18, 7, 1, 1, '2018-09-22 03:29:57', 'OBB20180900006', 9872, 0, 32, 9840, 1, '2018-09-22 03:29:57', NULL, NULL, 'ACTIVE'),
(19, 7, 1, 1, '2018-09-22 03:30:11', 'OBB20180900005', 9840, 0, 32, 9808, 1, '2018-09-22 03:30:11', NULL, NULL, 'ACTIVE'),
(20, 8, 1, 1, '2018-09-22 03:32:49', 'OBB20180900019', 20, 0, 8, 12, 1, '2018-09-22 03:32:49', NULL, NULL, 'ACTIVE'),
(21, 8, 1, 1, '2018-09-22 03:33:04', 'OBB20180900018', 12, 0, 8, 4, 1, '2018-09-22 03:33:04', NULL, NULL, 'ACTIVE'),
(22, 8, 1, 1, '2018-09-22 03:33:11', 'OBB20180900017', 4, 0, 8, -4, 1, '2018-09-22 03:33:11', NULL, NULL, 'ACTIVE'),
(23, 8, 1, 1, '2018-09-22 03:34:15', 'OBB20180900020', -4, 0, 8, -12, 1, '2018-09-22 03:34:15', NULL, NULL, 'ACTIVE'),
(24, 8, 1, 1, '2018-09-22 03:35:24', 'OBB20180900021', -12, 0, 8, -20, 1, '2018-09-22 03:35:24', NULL, NULL, 'ACTIVE'),
(25, 8, 1, 1, '2018-09-22 03:35:34', 'OBB20180900022', -20, 0, 8, -28, 1, '2018-09-22 03:35:34', NULL, NULL, 'ACTIVE'),
(26, 8, 1, 1, '2018-09-22 03:35:57', 'OBB20180900023', -28, 0, 8, -36, 1, '2018-09-22 03:35:57', NULL, NULL, 'ACTIVE'),
(27, 8, 1, 1, '2018-09-22 03:36:28', 'OBB20180900024', -36, 0, 8, -44, 1, '2018-09-22 03:36:28', NULL, NULL, 'ACTIVE'),
(28, 8, 1, 1, '2018-09-22 03:37:19', 'OBB20180900025', -44, 0, 8, -52, 1, '2018-09-22 03:37:19', NULL, NULL, 'ACTIVE'),
(29, 8, 1, 1, '2018-09-22 03:38:02', 'OBB20180900026', -52, 0, 8, -60, 1, '2018-09-22 03:38:02', NULL, NULL, 'ACTIVE'),
(30, 8, 1, 1, '2018-09-22 03:38:11', 'OBB20180900027', -60, 0, 8, -68, 1, '2018-09-22 03:38:11', NULL, NULL, 'ACTIVE'),
(31, 8, 1, 1, '2018-09-22 03:38:41', 'OBB20180900028', -68, 0, 8, -76, 1, '2018-09-22 03:38:41', NULL, NULL, 'ACTIVE'),
(32, 8, 1, 1, '2018-09-22 03:39:17', 'OBB20180900029', -76, 0, 8, -84, 1, '2018-09-22 03:39:17', NULL, NULL, 'ACTIVE'),
(33, 8, 1, 1, '2018-09-22 03:39:46', 'OBB20180900030', -84, 0, 8, -92, 1, '2018-09-22 03:39:46', NULL, NULL, 'ACTIVE'),
(34, 8, 1, 1, '2018-09-22 03:41:21', 'OBB20180900031', -92, 0, 8, -100, 1, '2018-09-22 03:41:21', NULL, NULL, 'ACTIVE'),
(35, 8, 1, 1, '2018-09-22 03:41:27', 'OBB20180900032', -100, 0, 8, -108, 1, '2018-09-22 03:41:27', NULL, NULL, 'ACTIVE'),
(36, 7, 1, 1, '2018-09-22 03:51:45', 'OBB20180900033', 9808, 0, 32, 9776, 1, '2018-09-22 03:51:45', NULL, NULL, 'ACTIVE'),
(37, 7, 1, 1, '2018-09-22 03:51:52', 'OBB20180900034', 9776, 0, 32, 9744, 1, '2018-09-22 03:51:52', NULL, NULL, 'ACTIVE'),
(38, 7, 1, 1, '2018-09-22 03:51:59', 'OBB20180900035', 9744, 0, 32, 9712, 1, '2018-09-22 03:51:59', NULL, NULL, 'ACTIVE'),
(39, 7, 1, 1, '2018-09-22 03:52:05', 'OBB20180900036', 9712, 0, 32, 9680, 1, '2018-09-22 03:52:05', NULL, NULL, 'ACTIVE'),
(40, 7, 1, 1, '2018-09-22 03:52:12', 'OBB20180900037', 9680, 0, 32, 9648, 1, '2018-09-22 03:52:12', NULL, NULL, 'ACTIVE'),
(41, 7, 1, 1, '2018-09-22 03:52:24', 'OBB20180900038', 9648, 0, 32, 9616, 1, '2018-09-22 03:52:24', NULL, NULL, 'ACTIVE'),
(42, 7, 1, 1, '2018-09-22 03:52:31', 'OBB20180900039', 9616, 0, 32, 9584, 1, '2018-09-22 03:52:31', NULL, NULL, 'ACTIVE'),
(43, 7, 1, 1, '2018-09-22 03:52:39', 'OBB20180900040', 9584, 0, 32, 9552, 1, '2018-09-22 03:52:39', NULL, NULL, 'ACTIVE'),
(44, 7, 1, 1, '2018-09-22 03:52:45', 'OBB20180900041', 9552, 0, 32, 9520, 1, '2018-09-22 03:52:45', NULL, NULL, 'ACTIVE'),
(45, 7, 1, 1, '2018-09-22 03:52:51', 'OBB20180900042', 9520, 0, 32, 9488, 1, '2018-09-22 03:52:51', NULL, NULL, 'ACTIVE'),
(46, 8, 1, 1, '2018-09-22 03:59:24', 'OBB20180900048', -108, 0, 8, -116, 1, '2018-09-22 03:59:24', NULL, NULL, 'ACTIVE'),
(47, 8, 1, 1, '2018-09-22 03:59:32', 'OBB20180900047', -116, 0, 8, -124, 1, '2018-09-22 03:59:32', NULL, NULL, 'ACTIVE'),
(48, 8, 1, 1, '2018-09-22 04:01:49', 'OBB20180900043', -124, 0, 8, -132, 1, '2018-09-22 04:01:49', NULL, NULL, 'ACTIVE'),
(49, 8, 1, 1, '2018-09-22 04:01:55', 'OBB20180900044', -132, 0, 8, -140, 1, '2018-09-22 04:01:55', NULL, NULL, 'ACTIVE'),
(50, 8, 1, 1, '2018-09-22 04:02:02', 'OBB20180900045', -140, 0, 8, -148, 1, '2018-09-22 04:02:02', NULL, NULL, 'ACTIVE'),
(51, 8, 1, 1, '2018-09-22 04:02:09', 'OBB20180900046', -148, 0, 8, -156, 1, '2018-09-22 04:02:09', NULL, NULL, 'ACTIVE'),
(52, 8, 1, 1, '2018-09-22 04:02:15', 'OBB20180900049', -156, 0, 8, -164, 1, '2018-09-22 04:02:15', NULL, NULL, 'ACTIVE'),
(53, 8, 1, 1, '2018-09-22 04:02:26', 'OBB20180900050', -164, 0, 8, -172, 1, '2018-09-22 04:02:26', NULL, NULL, 'ACTIVE'),
(54, 8, 1, 1, '2018-09-22 04:02:32', 'OBB20180900051', -172, 0, 8, -180, 1, '2018-09-22 04:02:32', NULL, NULL, 'ACTIVE'),
(55, 8, 1, 1, '2018-09-22 04:02:39', 'OBB20180900052', -180, 0, 8, -188, 1, '2018-09-22 04:02:39', NULL, NULL, 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `trans_inventory_barang_jadi`
--

CREATE TABLE IF NOT EXISTS `trans_inventory_barang_jadi` (
  `id` int(10) NOT NULL,
  `id_barang_jadi` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `trans_date` datetime NOT NULL,
  `comment` text NOT NULL,
  `saldo_awal` int(10) NOT NULL DEFAULT '0',
  `jumlah_masuk` int(10) NOT NULL DEFAULT '0',
  `jumlah_keluar` int(10) NOT NULL DEFAULT '0',
  `saldo_akhir` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_inventory_barang_jadi`
--

INSERT INTO `trans_inventory_barang_jadi` (`id`, `id_barang_jadi`, `id_gudang`, `id_user`, `trans_date`, `comment`, `saldo_awal`, `jumlah_masuk`, `jumlah_keluar`, `saldo_akhir`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES
(1, 2, 1, 1, '2018-09-22 04:40:43', 'IBJ20180900001', 500, 25, 0, 525, 1, '2018-09-22 04:40:43', NULL, NULL, 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `trans_inventory_barang_waste`
--

CREATE TABLE IF NOT EXISTS `trans_inventory_barang_waste` (
  `id` int(10) NOT NULL,
  `id_barang_waste` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `trans_date` datetime NOT NULL,
  `comment` text NOT NULL,
  `saldo_awal` int(10) NOT NULL DEFAULT '0',
  `jumlah_masuk` int(10) NOT NULL DEFAULT '0',
  `jumlah_keluar` int(10) NOT NULL DEFAULT '0',
  `saldo_akhir` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_pemakaian_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `trans_pemakaian_bahan_baku` (
  `id` int(10) NOT NULL,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_bukti_pengeluaran` varchar(50) NOT NULL,
  `tanggal_bukti_pengeluaran` datetime NOT NULL,
  `bahan_baku` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `gudang` int(10) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED'
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_pemakaian_bahan_baku`
--

INSERT INTO `trans_pemakaian_bahan_baku` (`id`, `no_transaksi`, `tanggal`, `no_bukti_pengeluaran`, `tanggal_bukti_pengeluaran`, `bahan_baku`, `jumlah`, `gudang`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`, `trans_status`) VALUES
(1, 'OBB20180900001', '2018-09-22 03:18:04', '01', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:18:04', 1, '2018-09-22 03:18:24', 'ACTIVE', 'CONFIRMED'),
(2, 'OBB20180900002', '2018-09-22 03:19:02', '02', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:19:02', 1, '2018-09-22 03:19:08', 'ACTIVE', 'CONFIRMED'),
(3, 'OBB20180900003', '2018-09-22 03:19:31', '03', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:19:31', 1, '2018-09-22 03:19:37', 'ACTIVE', 'CONFIRMED'),
(4, 'OBB20180900004', '2018-09-22 03:21:08', '04', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:21:08', 1, '2018-09-22 03:22:57', 'ACTIVE', 'CONFIRMED'),
(5, 'OBB20180900005', '2018-09-22 03:21:26', '05', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:21:26', 1, '2018-09-22 03:30:11', 'ACTIVE', 'CONFIRMED'),
(6, 'OBB20180900006', '2018-09-22 03:21:55', '06', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:21:55', 1, '2018-09-22 03:29:57', 'ACTIVE', 'CONFIRMED'),
(7, 'OBB20180900007', '2018-09-22 03:22:22', '07', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:22:22', 1, '2018-09-22 03:29:46', 'ACTIVE', 'CONFIRMED'),
(8, 'OBB20180900008', '2018-09-22 03:23:18', '08', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:23:18', 1, '2018-09-22 03:29:38', 'ACTIVE', 'CONFIRMED'),
(9, 'OBB20180900009', '2018-09-22 03:23:36', '09', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:23:36', 1, '2018-09-22 03:29:31', 'ACTIVE', 'CONFIRMED'),
(10, 'OBB20180900010', '2018-09-22 03:24:22', '010', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:24:22', 1, '2018-09-22 03:29:23', 'ACTIVE', 'CONFIRMED'),
(11, 'OBB20180900011', '2018-09-22 03:25:10', '011', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:25:10', 1, '2018-09-22 03:29:11', 'ACTIVE', 'CONFIRMED'),
(12, 'OBB20180900012', '2018-09-22 03:25:28', '012', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:25:28', 1, '2018-09-22 03:29:02', 'ACTIVE', 'CONFIRMED'),
(13, 'OBB20180900013', '2018-09-22 03:25:59', '013', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:25:59', 1, '2018-09-22 03:28:56', 'ACTIVE', 'CONFIRMED'),
(14, 'OBB20180900014', '2018-09-22 03:27:25', '014', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:27:25', 1, '2018-09-22 03:28:40', 'ACTIVE', 'CONFIRMED'),
(15, 'OBB20180900015', '2018-09-22 03:27:46', '015', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:27:46', 1, '2018-09-22 03:28:34', 'ACTIVE', 'CONFIRMED'),
(16, 'OBB20180900016', '2018-09-22 03:28:09', '016', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:28:09', 1, '2018-09-22 03:28:27', 'ACTIVE', 'CONFIRMED'),
(17, 'OBB20180900017', '2018-09-22 03:31:05', '01', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:31:05', 1, '2018-09-22 03:33:11', 'ACTIVE', 'CONFIRMED'),
(18, 'OBB20180900018', '2018-09-22 03:31:24', '02', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:31:24', 1, '2018-09-22 03:33:04', 'ACTIVE', 'CONFIRMED'),
(19, 'OBB20180900019', '2018-09-22 03:32:35', '03', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:32:35', 1, '2018-09-22 03:32:49', 'ACTIVE', 'CONFIRMED'),
(20, 'OBB20180900020', '2018-09-22 03:34:08', '04', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:34:08', 1, '2018-09-22 03:34:15', 'ACTIVE', 'CONFIRMED'),
(21, 'OBB20180900021', '2018-09-22 03:34:32', '05', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:34:32', 1, '2018-09-22 03:35:24', 'ACTIVE', 'CONFIRMED'),
(22, 'OBB20180900022', '2018-09-22 03:35:11', '06', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:35:11', 1, '2018-09-22 03:35:34', 'ACTIVE', 'CONFIRMED'),
(23, 'OBB20180900023', '2018-09-22 03:35:50', '07', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:35:50', 1, '2018-09-22 03:35:57', 'ACTIVE', 'CONFIRMED'),
(24, 'OBB20180900024', '2018-09-22 03:36:20', '08', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:36:20', 1, '2018-09-22 03:36:28', 'ACTIVE', 'CONFIRMED'),
(25, 'OBB20180900025', '2018-09-22 03:37:07', '09', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:37:07', 1, '2018-09-22 03:37:19', 'ACTIVE', 'CONFIRMED'),
(26, 'OBB20180900026', '2018-09-22 03:37:36', '010', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:37:36', 1, '2018-09-22 03:38:02', 'ACTIVE', 'CONFIRMED'),
(27, 'OBB20180900027', '2018-09-22 03:37:53', '011', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:37:53', 1, '2018-09-22 03:38:11', 'ACTIVE', 'CONFIRMED'),
(28, 'OBB20180900028', '2018-09-22 03:38:32', '012', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:38:32', 1, '2018-09-22 03:38:41', 'ACTIVE', 'CONFIRMED'),
(29, 'OBB20180900029', '2018-09-22 03:39:01', '013', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:39:01', 1, '2018-09-22 03:39:17', 'ACTIVE', 'CONFIRMED'),
(30, 'OBB20180900030', '2018-09-22 03:39:34', '014', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:39:34', 1, '2018-09-22 03:39:46', 'ACTIVE', 'CONFIRMED'),
(31, 'OBB20180900031', '2018-09-22 03:40:55', '015', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:40:55', 1, '2018-09-22 03:41:21', 'ACTIVE', 'CONFIRMED'),
(32, 'OBB20180900032', '2018-09-22 03:41:14', '016', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 03:41:14', 1, '2018-09-22 03:41:27', 'ACTIVE', 'CONFIRMED'),
(33, 'OBB20180900033', '2018-09-22 03:43:13', '017', '2018-09-20 00:00:00', 7, 32, 1, 1, '2018-09-22 03:43:13', 1, '2018-09-22 03:51:45', 'ACTIVE', 'CONFIRMED'),
(34, 'OBB20180900034', '2018-09-22 03:43:37', '018', '2018-09-20 00:00:00', 7, 32, 1, 1, '2018-09-22 03:43:37', 1, '2018-09-22 03:51:52', 'ACTIVE', 'CONFIRMED'),
(35, 'OBB20180900035', '2018-09-22 03:44:13', '019', '2018-09-20 00:00:00', 7, 32, 1, 1, '2018-09-22 03:44:13', 1, '2018-09-22 03:51:59', 'ACTIVE', 'CONFIRMED'),
(36, 'OBB20180900036', '2018-09-22 03:44:50', '020', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 03:44:50', 1, '2018-09-22 03:52:05', 'ACTIVE', 'CONFIRMED'),
(37, 'OBB20180900037', '2018-09-22 03:45:30', '021', '2018-09-20 00:00:00', 7, 32, 1, 1, '2018-09-22 03:45:30', 1, '2018-09-22 03:52:12', 'ACTIVE', 'CONFIRMED'),
(38, 'OBB20180900038', '2018-09-22 03:46:28', '022', '2018-09-20 00:00:00', 7, 32, 1, 1, '2018-09-22 03:46:28', 1, '2018-09-22 03:52:24', 'ACTIVE', 'CONFIRMED'),
(39, 'OBB20180900039', '2018-09-22 03:46:58', '023', '2018-09-20 00:00:00', 7, 32, 1, 1, '2018-09-22 03:46:58', 1, '2018-09-22 03:52:31', 'ACTIVE', 'CONFIRMED'),
(40, 'OBB20180900040', '2018-09-22 03:50:01', '024', '2018-09-20 00:00:00', 7, 32, 1, 1, '2018-09-22 03:50:01', 1, '2018-09-22 03:52:39', 'ACTIVE', 'CONFIRMED'),
(41, 'OBB20180900041', '2018-09-22 03:50:46', '025', '2018-09-20 00:00:00', 7, 32, 1, 1, '2018-09-22 03:50:46', 1, '2018-09-22 03:52:45', 'ACTIVE', 'CONFIRMED'),
(42, 'OBB20180900042', '2018-09-22 03:51:12', '026', '2018-09-20 00:00:00', 7, 32, 1, 1, '2018-09-22 03:51:12', 1, '2018-09-22 03:52:51', 'ACTIVE', 'CONFIRMED'),
(43, 'OBB20180900043', '2018-09-22 03:57:12', '017', '2018-09-20 00:00:00', 8, 8, 1, 1, '2018-09-22 03:57:12', 1, '2018-09-22 04:01:49', 'ACTIVE', 'CONFIRMED'),
(44, 'OBB20180900044', '2018-09-22 03:57:30', '018', '2018-09-20 00:00:00', 8, 8, 1, 1, '2018-09-22 03:57:30', 1, '2018-09-22 04:01:55', 'ACTIVE', 'CONFIRMED'),
(45, 'OBB20180900045', '2018-09-22 03:57:55', '019', '2018-09-20 00:00:00', 8, 8, 1, 1, '2018-09-22 03:57:55', 1, '2018-09-22 04:02:02', 'ACTIVE', 'CONFIRMED'),
(46, 'OBB20180900046', '2018-09-22 03:58:14', '020', '2018-09-20 00:00:00', 8, 8, 1, 1, '2018-09-22 03:58:14', 1, '2018-09-22 04:02:09', 'ACTIVE', 'CONFIRMED'),
(47, 'OBB20180900047', '2018-09-22 03:58:35', '021', '2018-09-20 00:00:00', 8, 8, 1, 1, '2018-09-22 03:58:35', 1, '2018-09-22 03:59:32', 'ACTIVE', 'CONFIRMED'),
(48, 'OBB20180900048', '2018-09-22 03:59:14', '022', '2018-09-20 00:00:00', 8, 8, 1, 1, '2018-09-22 03:59:14', 1, '2018-09-22 03:59:24', 'ACTIVE', 'CONFIRMED'),
(49, 'OBB20180900049', '2018-09-22 03:59:48', '023', '2018-09-20 00:00:00', 8, 8, 1, 1, '2018-09-22 03:59:48', 1, '2018-09-22 04:02:15', 'ACTIVE', 'CONFIRMED'),
(50, 'OBB20180900050', '2018-09-22 04:00:14', '024', '2018-09-20 00:00:00', 8, 8, 1, 1, '2018-09-22 04:00:14', 1, '2018-09-22 04:02:26', 'ACTIVE', 'CONFIRMED'),
(51, 'OBB20180900051', '2018-09-22 04:00:40', '025', '2018-09-20 00:00:00', 8, 8, 1, 1, '2018-09-22 04:00:40', 1, '2018-09-22 04:02:32', 'ACTIVE', 'CONFIRMED'),
(52, 'OBB20180900052', '2018-09-22 04:00:57', '026', '2018-09-20 00:00:00', 8, 8, 1, 1, '2018-09-22 04:00:57', 1, '2018-09-22 04:02:39', 'ACTIVE', 'CONFIRMED'),
(53, 'OBB20180900053', '2018-09-22 08:12:30', 'PB01', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 08:12:30', 1, '2018-09-22 08:13:26', 'DELETED', 'PENDING'),
(54, 'OBB20180900054', '2018-09-22 08:13:16', 'PB02', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 08:13:16', NULL, NULL, 'ACTIVE', 'PENDING'),
(55, 'OBB20180900055', '2018-09-22 08:13:54', 'PB02', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 08:13:54', NULL, NULL, 'ACTIVE', 'PENDING'),
(56, 'OBB20180900056', '2018-09-22 08:14:50', 'PB03', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 08:14:50', NULL, NULL, 'ACTIVE', 'PENDING'),
(57, 'OBB20180900057', '2018-09-22 08:15:54', 'PB04', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 08:15:54', NULL, NULL, 'ACTIVE', 'PENDING'),
(58, 'OBB20180900058', '2018-09-22 08:16:22', 'PB05', '2018-09-19 00:00:00', 7, 8, 1, 1, '2018-09-22 08:16:22', 1, '2018-09-22 08:16:31', 'DELETED', 'PENDING'),
(59, 'OBB20180900059', '2018-09-22 08:20:19', 'PB06', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 08:20:19', NULL, NULL, 'ACTIVE', 'PENDING'),
(60, 'OBB20180900060', '2018-09-22 08:20:51', 'PB07', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 08:20:51', NULL, NULL, 'ACTIVE', 'PENDING'),
(61, 'OBB20180900061', '2018-09-22 08:21:26', 'PB08', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 08:21:26', NULL, NULL, 'ACTIVE', 'PENDING'),
(62, 'OBB20180900062', '2018-09-22 08:22:00', 'PB09', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 08:22:00', NULL, NULL, 'ACTIVE', 'PENDING'),
(63, 'OBB20180900063', '2018-09-22 08:22:35', 'PB10', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 08:22:35', NULL, NULL, 'ACTIVE', 'PENDING'),
(64, 'OBB20180900064', '2018-09-22 08:22:57', 'PB11', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 08:22:57', NULL, NULL, 'ACTIVE', 'PENDING'),
(65, 'OBB20180900065', '2018-09-22 08:23:52', 'PB12', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 08:23:52', NULL, NULL, 'ACTIVE', 'PENDING'),
(66, 'OBB20180900066', '2018-09-22 08:24:14', 'PB13', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 08:24:14', NULL, NULL, 'ACTIVE', 'PENDING'),
(67, 'OBB20180900067', '2018-09-22 08:24:40', 'PB14', '2018-09-19 00:00:00', 7, 32, 1, 1, '2018-09-22 08:24:40', NULL, NULL, 'ACTIVE', 'PENDING'),
(68, 'OBB20180900068', '2018-09-22 08:24:57', 'PB15', '2018-09-19 00:00:00', 8, 8, 1, 1, '2018-09-22 08:24:57', NULL, NULL, 'ACTIVE', 'PENDING');

-- --------------------------------------------------------

--
-- Table structure for table `trans_pemasukan_bahan_baku`
--

CREATE TABLE IF NOT EXISTS `trans_pemasukan_bahan_baku` (
  `id` int(10) NOT NULL,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `jenis_dokumen` varchar(100) NOT NULL,
  `no_dokumen_pabean` varchar(50) NOT NULL,
  `tanggal_dokumen_pabean` datetime NOT NULL,
  `no_seri_barang` varchar(50) NOT NULL,
  `no_bukti_penerimaan_barang` varchar(50) NOT NULL,
  `tanggal_bukti_penerimaan_barang` datetime NOT NULL,
  `bahan_baku` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `mata_uang` int(10) NOT NULL,
  `nilai_barang` varchar(50) NOT NULL,
  `gudang` int(10) NOT NULL,
  `negara_asal_barang` int(10) NOT NULL,
  `supplier` int(10) NOT NULL,
  `harga_satuan` varchar(50) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED'
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_pemasukan_bahan_baku`
--

INSERT INTO `trans_pemasukan_bahan_baku` (`id`, `no_transaksi`, `tanggal`, `jenis_dokumen`, `no_dokumen_pabean`, `tanggal_dokumen_pabean`, `no_seri_barang`, `no_bukti_penerimaan_barang`, `tanggal_bukti_penerimaan_barang`, `bahan_baku`, `jumlah`, `mata_uang`, `nilai_barang`, `gudang`, `negara_asal_barang`, `supplier`, `harga_satuan`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`, `trans_status`) VALUES
(30, 'IBB20180800001', '2018-08-14 06:30:37', 'Dok. Import', '010700-000366-20180727-001280', '2018-07-27 00:00:00', '11081100', 'SUDU38SYD007114X', '2018-07-30 00:00:00', 8, 20, 2, '7560.00', 1, 5, 3, '105', 1, '2018-08-14 06:30:37', 1, '2018-09-22 03:11:03', 'ACTIVE', 'CONFIRMED'),
(31, 'IBB20180800002', '2018-08-16 04:16:38', 'Dok. Import', '010700-000366-20180731-001288', '2018-07-31 00:00:00', '11081300', 'HLCUDUS180521099', '2018-07-21 00:00:00', 7, 2, 2, '33.970', 1, 3, 3, '0.7900', 1, '2018-08-16 04:16:38', 1, '2018-08-16 04:18:47', 'DELETED', 'PENDING'),
(32, 'IBB20180800003', '2018-08-16 04:20:33', 'Dok. Import', '010700-000366-20180731-001288', '2018-07-31 00:00:00', '11081300', 'HLCUDUS180521099', '2018-07-21 00:00:00', 7, 2, 1, '33.970', 1, 3, 3, '0.7900', 1, '2018-08-16 04:20:33', 1, '2018-08-16 04:21:40', 'DELETED', 'PENDING'),
(33, 'IBB20180800004', '2018-08-16 04:23:10', 'Dok. Import', '010700-000366-20180731-001288', '2018-07-31 00:00:00', '11081300', 'HLCUDUS180521099', '2018-07-21 00:00:00', 7, 1720, 2, '33.970', 1, 3, 3, '1975', 1, '2018-08-16 04:23:10', 1, '2018-09-22 03:03:58', 'ACTIVE', 'CONFIRMED'),
(34, 'IBB20180900001', '2018-09-07 09:22:26', 'Dok. Import', '010700-000366-20180811-001321', '2018-08-11 00:00:00', '11081300', 'OOLU4026943690', '2018-09-05 00:00:00', 7, 1600, 2, '30000', 1, 6, 3, '18.75', 1, '2018-09-07 09:22:26', 1, '2018-09-19 04:32:59', 'ACTIVE', 'CONFIRMED');

-- --------------------------------------------------------

--
-- Table structure for table `trans_pemasukan_barang_jadi`
--

CREATE TABLE IF NOT EXISTS `trans_pemasukan_barang_jadi` (
  `id` int(10) NOT NULL,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_bukti_penerimaan` varchar(50) NOT NULL,
  `tanggal_bukti_penerimaan` datetime NOT NULL,
  `barang_jadi` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `gudang` int(10) NOT NULL,
  `no_pemakaian_bahan_baku` varchar(200) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_pemasukan_barang_jadi`
--

INSERT INTO `trans_pemasukan_barang_jadi` (`id`, `no_transaksi`, `tanggal`, `no_bukti_penerimaan`, `tanggal_bukti_penerimaan`, `barang_jadi`, `jumlah`, `gudang`, `no_pemakaian_bahan_baku`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`, `trans_status`) VALUES
(1, 'IBJ20180900001', '2018-09-22 04:38:28', 'H01', '2018-09-20 00:00:00', 2, 25, 1, 'OBB20180900001,OBB20180900002,OBB20180900003,OBB20180900004,OBB20180900005,OBB20180900006,OBB20180900007,OBB20180900008,OBB20180900009,OBB20180900010,OBB20180900011,OBB20180900012,OBB20180900013,OBB20', 1, '2018-09-22 04:38:28', 1, '2018-09-22 04:40:43', 'ACTIVE', 'CONFIRMED');

-- --------------------------------------------------------

--
-- Table structure for table `trans_pengeluaran_barang_jadi`
--

CREATE TABLE IF NOT EXISTS `trans_pengeluaran_barang_jadi` (
  `id` int(10) NOT NULL,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_feb` varchar(50) NOT NULL,
  `tanggal_feb` datetime NOT NULL,
  `no_bukti_pengeluaran` varchar(50) NOT NULL,
  `tanggal_bukti_pengeluaran` datetime NOT NULL,
  `pembeli` int(10) NOT NULL,
  `negara_tujuan` int(10) NOT NULL,
  `barang_jadi` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `mata_uang` int(10) NOT NULL,
  `nilai_barang` varchar(50) NOT NULL,
  `gudang` int(10) NOT NULL,
  `harga_satuan` varchar(50) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_penyelesaian_barang_waste`
--

CREATE TABLE IF NOT EXISTS `trans_penyelesaian_barang_waste` (
  `id` int(10) NOT NULL,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_bc_24` varchar(50) NOT NULL,
  `tanggal_bc_24` datetime NOT NULL,
  `barang_waste` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `nilai` varchar(50) NOT NULL,
  `gudang` int(10) NOT NULL,
  `no_pengeluaran_barang_jadi` varchar(200) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `core_app_config`
--
ALTER TABLE `core_app_config`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `config_key` (`config_key`);

--
-- Indexes for table `core_app_log`
--
ALTER TABLE `core_app_log`
  ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `module` (`module`);

--
-- Indexes for table `core_content`
--
ALTER TABLE `core_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `core_email_log`
--
ALTER TABLE `core_email_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_sessions`
--
ALTER TABLE `data_sessions`
  ADD PRIMARY KEY (`session_id`), ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `master_bahan_baku`
--
ALTER TABLE `master_bahan_baku`
  ADD PRIMARY KEY (`id`), ADD KEY `kode` (`kode`), ADD KEY `nama` (`nama`), ADD KEY `satuan` (`satuan`), ADD KEY `kode_status` (`kode`,`record_status`);

--
-- Indexes for table `master_barang_jadi`
--
ALTER TABLE `master_barang_jadi`
  ADD PRIMARY KEY (`id`), ADD KEY `kode` (`kode`), ADD KEY `nama` (`nama`), ADD KEY `satuan` (`satuan`), ADD KEY `kode_status` (`kode`,`record_status`);

--
-- Indexes for table `master_barang_waste`
--
ALTER TABLE `master_barang_waste`
  ADD PRIMARY KEY (`id`), ADD KEY `kode` (`kode`), ADD KEY `nama` (`nama`), ADD KEY `satuan` (`satuan`), ADD KEY `kode_status` (`kode`,`record_status`);

--
-- Indexes for table `master_customer`
--
ALTER TABLE `master_customer`
  ADD PRIMARY KEY (`id`), ADD KEY `kode` (`kode`), ADD KEY `nama` (`nama`), ADD KEY `kode_status` (`kode`,`record_status`);

--
-- Indexes for table `master_gudang`
--
ALTER TABLE `master_gudang`
  ADD PRIMARY KEY (`id`), ADD KEY `kode` (`kode`), ADD KEY `nama` (`nama`), ADD KEY `kode_status` (`kode`,`record_status`);

--
-- Indexes for table `master_mata_uang`
--
ALTER TABLE `master_mata_uang`
  ADD PRIMARY KEY (`id`), ADD KEY `kode` (`kode`), ADD KEY `nama` (`nama`), ADD KEY `kode_status` (`kode`,`record_status`);

--
-- Indexes for table `master_negara`
--
ALTER TABLE `master_negara`
  ADD PRIMARY KEY (`id`), ADD KEY `kode` (`kode`), ADD KEY `nama` (`nama`), ADD KEY `kode_status` (`kode`,`record_status`);

--
-- Indexes for table `master_satuan`
--
ALTER TABLE `master_satuan`
  ADD PRIMARY KEY (`id`), ADD KEY `kode` (`kode`), ADD KEY `nama` (`nama`), ADD KEY `kode_status` (`kode`,`record_status`);

--
-- Indexes for table `master_stock_bahan_baku`
--
ALTER TABLE `master_stock_bahan_baku`
  ADD PRIMARY KEY (`id_bahan_baku`,`id_gudang`), ADD KEY `id_bahan_baku` (`id_bahan_baku`), ADD KEY `id_gudang` (`id_gudang`), ADD KEY `bahan_baku_gudang` (`id_bahan_baku`,`id_gudang`);

--
-- Indexes for table `master_stock_barang_jadi`
--
ALTER TABLE `master_stock_barang_jadi`
  ADD PRIMARY KEY (`id_barang_jadi`,`id_gudang`), ADD KEY `id_barang_jadi` (`id_barang_jadi`), ADD KEY `id_gudang` (`id_gudang`), ADD KEY `barang_jadi_gudang` (`id_barang_jadi`,`id_gudang`);

--
-- Indexes for table `master_stock_barang_waste`
--
ALTER TABLE `master_stock_barang_waste`
  ADD PRIMARY KEY (`id_barang_waste`,`id_gudang`), ADD KEY `id_barang_waste` (`id_barang_waste`), ADD KEY `id_gudang` (`id_gudang`), ADD KEY `barang_waste_gudang` (`id_barang_waste`,`id_gudang`);

--
-- Indexes for table `master_supplier`
--
ALTER TABLE `master_supplier`
  ADD PRIMARY KEY (`id`), ADD KEY `kode` (`kode`), ADD KEY `nama` (`nama`), ADD KEY `kode_status` (`kode`,`record_status`);

--
-- Indexes for table `master_user`
--
ALTER TABLE `master_user`
  ADD PRIMARY KEY (`id`), ADD KEY `user_name` (`user_name`), ADD KEY `ip` (`ip`), ADD KEY `name_status` (`user_name`,`user_status`);

--
-- Indexes for table `trans_inventory_bahan_baku`
--
ALTER TABLE `trans_inventory_bahan_baku`
  ADD PRIMARY KEY (`id`), ADD KEY `id_bahan_baku` (`id_bahan_baku`), ADD KEY `id_gudang` (`id_gudang`), ADD KEY `id_user` (`id_user`), ADD KEY `bahan_baku_gudang` (`id_bahan_baku`,`id_gudang`);

--
-- Indexes for table `trans_inventory_barang_jadi`
--
ALTER TABLE `trans_inventory_barang_jadi`
  ADD PRIMARY KEY (`id`), ADD KEY `id_barang_jadi` (`id_barang_jadi`), ADD KEY `id_gudang` (`id_gudang`), ADD KEY `id_user` (`id_user`), ADD KEY `barang_jadi_gudang` (`id_barang_jadi`,`id_gudang`);

--
-- Indexes for table `trans_inventory_barang_waste`
--
ALTER TABLE `trans_inventory_barang_waste`
  ADD PRIMARY KEY (`id`), ADD KEY `id_barang_waste` (`id_barang_waste`), ADD KEY `id_gudang` (`id_gudang`), ADD KEY `id_user` (`id_user`), ADD KEY `barang_waste_gudang` (`id_barang_waste`,`id_gudang`);

--
-- Indexes for table `trans_pemakaian_bahan_baku`
--
ALTER TABLE `trans_pemakaian_bahan_baku`
  ADD PRIMARY KEY (`id`), ADD KEY `no_transaksi` (`no_transaksi`), ADD KEY `bahan_baku` (`bahan_baku`), ADD KEY `gudang` (`gudang`), ADD KEY `bahan_baku_gudang` (`bahan_baku`,`gudang`);

--
-- Indexes for table `trans_pemasukan_bahan_baku`
--
ALTER TABLE `trans_pemasukan_bahan_baku`
  ADD PRIMARY KEY (`id`), ADD KEY `no_transaksi` (`no_transaksi`), ADD KEY `bahan_baku` (`bahan_baku`), ADD KEY `mata_uang` (`mata_uang`), ADD KEY `gudang` (`gudang`), ADD KEY `negara_asal_barang` (`negara_asal_barang`), ADD KEY `bahan_baku_gudang` (`bahan_baku`,`gudang`), ADD KEY `supplier` (`supplier`);

--
-- Indexes for table `trans_pemasukan_barang_jadi`
--
ALTER TABLE `trans_pemasukan_barang_jadi`
  ADD PRIMARY KEY (`id`), ADD KEY `no_transaksi` (`no_transaksi`), ADD KEY `barang_jadi` (`barang_jadi`), ADD KEY `gudang` (`gudang`), ADD KEY `barang_jadi_gudang` (`barang_jadi`,`gudang`);

--
-- Indexes for table `trans_pengeluaran_barang_jadi`
--
ALTER TABLE `trans_pengeluaran_barang_jadi`
  ADD PRIMARY KEY (`id`), ADD KEY `no_transaksi` (`no_transaksi`), ADD KEY `barang_jadi` (`barang_jadi`), ADD KEY `mata_uang` (`mata_uang`), ADD KEY `gudang` (`gudang`), ADD KEY `negara_tujuan` (`negara_tujuan`), ADD KEY `pembeli` (`pembeli`), ADD KEY `barang_jadi_gudang` (`barang_jadi`,`gudang`);

--
-- Indexes for table `trans_penyelesaian_barang_waste`
--
ALTER TABLE `trans_penyelesaian_barang_waste`
  ADD PRIMARY KEY (`id`), ADD KEY `no_transaksi` (`no_transaksi`), ADD KEY `barang_waste` (`barang_waste`), ADD KEY `gudang` (`gudang`), ADD KEY `barang_waste_gudang` (`barang_waste`,`gudang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `core_app_config`
--
ALTER TABLE `core_app_config`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `core_app_log`
--
ALTER TABLE `core_app_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=450;
--
-- AUTO_INCREMENT for table `core_content`
--
ALTER TABLE `core_content`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_email_log`
--
ALTER TABLE `core_email_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `master_bahan_baku`
--
ALTER TABLE `master_bahan_baku`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `master_barang_jadi`
--
ALTER TABLE `master_barang_jadi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `master_barang_waste`
--
ALTER TABLE `master_barang_waste`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `master_customer`
--
ALTER TABLE `master_customer`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_gudang`
--
ALTER TABLE `master_gudang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_mata_uang`
--
ALTER TABLE `master_mata_uang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `master_negara`
--
ALTER TABLE `master_negara`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `master_satuan`
--
ALTER TABLE `master_satuan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `master_supplier`
--
ALTER TABLE `master_supplier`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `master_user`
--
ALTER TABLE `master_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trans_inventory_bahan_baku`
--
ALTER TABLE `trans_inventory_bahan_baku`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `trans_inventory_barang_jadi`
--
ALTER TABLE `trans_inventory_barang_jadi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trans_inventory_barang_waste`
--
ALTER TABLE `trans_inventory_barang_waste`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trans_pemakaian_bahan_baku`
--
ALTER TABLE `trans_pemakaian_bahan_baku`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `trans_pemasukan_bahan_baku`
--
ALTER TABLE `trans_pemasukan_bahan_baku`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `trans_pemasukan_barang_jadi`
--
ALTER TABLE `trans_pemasukan_barang_jadi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trans_pengeluaran_barang_jadi`
--
ALTER TABLE `trans_pengeluaran_barang_jadi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trans_penyelesaian_barang_waste`
--
ALTER TABLE `trans_penyelesaian_barang_waste`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `master_bahan_baku`
--
ALTER TABLE `master_bahan_baku`
ADD CONSTRAINT `mbbaku_satuan` FOREIGN KEY (`satuan`) REFERENCES `master_satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `master_barang_jadi`
--
ALTER TABLE `master_barang_jadi`
ADD CONSTRAINT `mbjadi_satuan` FOREIGN KEY (`satuan`) REFERENCES `master_satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `master_barang_waste`
--
ALTER TABLE `master_barang_waste`
ADD CONSTRAINT `mbwaste_satuan` FOREIGN KEY (`satuan`) REFERENCES `master_satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `master_stock_bahan_baku`
--
ALTER TABLE `master_stock_bahan_baku`
ADD CONSTRAINT `mstockbbaku_bbaku` FOREIGN KEY (`id_bahan_baku`) REFERENCES `master_bahan_baku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `mstockbbaku_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `master_stock_barang_jadi`
--
ALTER TABLE `master_stock_barang_jadi`
ADD CONSTRAINT `mstockbjadi_bjadi` FOREIGN KEY (`id_barang_jadi`) REFERENCES `master_barang_jadi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `mstockbjadi_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `master_stock_barang_waste`
--
ALTER TABLE `master_stock_barang_waste`
ADD CONSTRAINT `mstockbwaste_bwaste` FOREIGN KEY (`id_barang_waste`) REFERENCES `master_barang_waste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `mstockbwaste_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trans_inventory_bahan_baku`
--
ALTER TABLE `trans_inventory_bahan_baku`
ADD CONSTRAINT `tinventorybbaku_bbaku` FOREIGN KEY (`id_bahan_baku`) REFERENCES `master_bahan_baku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tinventorybbaku_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trans_inventory_barang_jadi`
--
ALTER TABLE `trans_inventory_barang_jadi`
ADD CONSTRAINT `tinventorybjadi_bjadi` FOREIGN KEY (`id_barang_jadi`) REFERENCES `master_barang_jadi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tinventorybjadi_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trans_inventory_barang_waste`
--
ALTER TABLE `trans_inventory_barang_waste`
ADD CONSTRAINT `tinventorybwaste_bwaste` FOREIGN KEY (`id_barang_waste`) REFERENCES `master_barang_waste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tinventorybwaste_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trans_pemakaian_bahan_baku`
--
ALTER TABLE `trans_pemakaian_bahan_baku`
ADD CONSTRAINT `tpemakaianbbaku_bbaku` FOREIGN KEY (`bahan_baku`) REFERENCES `master_bahan_baku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tpemakaianbbaku_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trans_pemasukan_bahan_baku`
--
ALTER TABLE `trans_pemasukan_bahan_baku`
ADD CONSTRAINT `tpemasukanbbaku_bbaku` FOREIGN KEY (`bahan_baku`) REFERENCES `master_bahan_baku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tpemasukanbbaku_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tpemasukanbbaku_muang` FOREIGN KEY (`mata_uang`) REFERENCES `master_mata_uang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tpemasukanbbaku_negara` FOREIGN KEY (`negara_asal_barang`) REFERENCES `master_negara` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tpemasukanbbaku_supplier` FOREIGN KEY (`supplier`) REFERENCES `master_supplier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trans_pemasukan_barang_jadi`
--
ALTER TABLE `trans_pemasukan_barang_jadi`
ADD CONSTRAINT `tpemasukanbjadi_bjadi` FOREIGN KEY (`barang_jadi`) REFERENCES `master_barang_jadi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tpemasukanbjadi_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trans_pengeluaran_barang_jadi`
--
ALTER TABLE `trans_pengeluaran_barang_jadi`
ADD CONSTRAINT `tpengeluaranbjadi_bjadi` FOREIGN KEY (`barang_jadi`) REFERENCES `master_barang_jadi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tpengeluaranbjadi_customer` FOREIGN KEY (`pembeli`) REFERENCES `master_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tpengeluaranbjadi_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tpengeluaranbjadi_muang` FOREIGN KEY (`mata_uang`) REFERENCES `master_mata_uang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tpengeluaranbjadi_negara` FOREIGN KEY (`negara_tujuan`) REFERENCES `master_negara` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trans_penyelesaian_barang_waste`
--
ALTER TABLE `trans_penyelesaian_barang_waste`
ADD CONSTRAINT `tpenyelesaianbwaste_bwaste` FOREIGN KEY (`barang_waste`) REFERENCES `master_barang_waste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tpenyelesaianbwaste_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
