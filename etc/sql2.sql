CREATE TABLE `master_stock_olahan_bahan_baku` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_bahan_baku` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `import_flag` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_bahan_baku` (`id_bahan_baku`),
  CONSTRAINT `mstockolahbbaku_bbaku` FOREIGN KEY (`id_bahan_baku`) REFERENCES `master_bahan_baku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1


CREATE TABLE `master_stock_olahan_barang_jadi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_barang_jadi` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `import_flag` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_barang_jadi` (`id_barang_jadi`),
  CONSTRAINT `mstocolahkbjadi_bjadi` FOREIGN KEY (`id_barang_jadi`) REFERENCES `master_barang_jadi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1


CREATE TABLE `trans_pengeluaran_bahan_baku` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_bukti_pengeluaran` varchar(50) NOT NULL,
  `tanggal_bukti_pengeluaran` datetime NOT NULL,
  `bahan_baku` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `gudang` int(10) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED',
  PRIMARY KEY (`id`),
  KEY `no_transaksi` (`no_transaksi`),
  KEY `bahan_baku` (`bahan_baku`),
  KEY `gudang` (`gudang`),
  KEY `bahan_baku_gudang` (`bahan_baku`,`gudang`),
  CONSTRAINT `tpengeluaranbbaku_bbaku` FOREIGN KEY (`bahan_baku`) REFERENCES `master_bahan_baku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpengeluaranbbaku_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1


CREATE TABLE `trans_pemakaian_barang_jadi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_feb` varchar(50) NOT NULL,
  `tanggal_feb` datetime NOT NULL,
  `no_bukti_pengeluaran` varchar(50) NOT NULL,
  `tanggal_bukti_pengeluaran` datetime NOT NULL,
  `pembeli` int(10) NOT NULL,
  `negara_tujuan` int(10) NOT NULL,
  `barang_jadi` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `mata_uang` int(10) NOT NULL,
  `nilai_barang` varchar(50) NOT NULL,
  `gudang` int(10) NOT NULL,
  `harga_satuan` varchar(50) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED',
  PRIMARY KEY (`id`),
  KEY `no_transaksi` (`no_transaksi`),
  KEY `barang_jadi` (`barang_jadi`),
  KEY `mata_uang` (`mata_uang`),
  KEY `gudang` (`gudang`),
  KEY `negara_tujuan` (`negara_tujuan`),
  KEY `pembeli` (`pembeli`),
  KEY `barang_jadi_gudang` (`barang_jadi`,`gudang`),
  CONSTRAINT `tpemakaianbjadi_bjadi` FOREIGN KEY (`barang_jadi`) REFERENCES `master_barang_jadi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpemakaianbjadi_customer` FOREIGN KEY (`pembeli`) REFERENCES `master_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpemakaianbjadi_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpemakaianbjadi_muang` FOREIGN KEY (`mata_uang`) REFERENCES `master_mata_uang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpemakaianbjadi_negara` FOREIGN KEY (`negara_tujuan`) REFERENCES `master_negara` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1



ALTER TABLE `produksikm`.`trans_pemasukan_bahan_baku` 
ADD COLUMN `no_pib` VARCHAR(45) NULL AFTER `harga_satuan`,
ADD COLUMN `tanggal_pib` DATETIME NULL AFTER `no_pib`,
ADD COLUMN `negara_asal_pib` INT(10) NULL DEFAULT NULL AFTER `tanggal_pib`;


ALTER TABLE `produksikm`.`trans_pemakaian_barang_jadi` 
ADD COLUMN `no_peb` VARCHAR(45) NULL DEFAULT NULL AFTER `harga_satuan`,
ADD COLUMN `tanggal_peb` DATETIME NULL DEFAULT NULL AFTER `no_peb`,
ADD COLUMN `negara_tujuan_peb` INT(10) NULL DEFAULT NULL AFTER `tanggal_peb`;




ALTER TABLE `produksikm`.`master_user` 
ADD COLUMN `role` INT(10) NOT NULL DEFAULT 1 AFTER `password`;



