
SELECT 
    s.id, s.id_bahan_baku, s.id_gudang, s.id_user, 
    s.comment, 
    s.trans_date, s.jumlah_masuk, s.jumlah_keluar,
    @b AS saldo_awal,
    @b := @b + s.jumlah_masuk - s.jumlah_keluar AS saldo_akhir
FROM
    (SELECT @b := 0.0) AS dummy 
  CROSS JOIN
    trans_inventory_bahan_baku AS s
WHERE s.id_bahan_baku = 7
ORDER BY
    trans_date ;



