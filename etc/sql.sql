/*
SQLyog Ultimate - MySQL GUI v8.2 
MySQL - 5.1.41 : Database - produksikm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`produksikm` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `produksikm`;

/*Table structure for table `core_app_config` */

DROP TABLE IF EXISTS `core_app_config`;

CREATE TABLE `core_app_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `config_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `config_value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `config_key` (`config_key`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `core_app_config` */

LOCK TABLES `core_app_config` WRITE;

insert  into `core_app_config`(`id`,`config_key`,`config_value`) values (1,'WEB_NAME','Administrasi Produksi'),(2,'COMPANY_NAME','Kilang Mie Gunung Mas2');

UNLOCK TABLES;

/*Table structure for table `core_app_log` */

DROP TABLE IF EXISTS `core_app_log`;

CREATE TABLE `core_app_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `forwarded_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `accept_language` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `device_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `event` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `input_data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `json_return` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=latin1;

/*Data for the table `core_app_log` */

LOCK TABLES `core_app_log` WRITE;

insert  into `core_app_log`(`id`,`ip`,`forwarded_ip`,`user_agent`,`accept_language`,`device_id`,`user_id`,`event`,`message`,`module`,`action`,`link`,`created_on`,`input_data`,`json_return`) values (1,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-10 00:47:15','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(2,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-10 00:49:22','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(3,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-10 00:49:26','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(4,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-10 00:49:48','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(5,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'logout_process','Success','home','logout_process','http://localhost:81/ProduksiKM/index.php/home/logout_process','2018-03-10 00:53:09','[]','[]'),(6,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-10 00:53:16','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(7,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-11 21:28:27','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(8,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'logout_process','Success','home','logout_process','http://localhost:81/ProduksiKM/index.php/home/logout_process','2018-03-11 23:44:23','[]','[]'),(9,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-11 23:44:37','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(10,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Failed - ','msatuan','create_process','http://localhost:81/ProduksiKM/index.php/msatuan/create_process','2018-03-12 06:31:41','{\"kode\":\"GR\",\"nama\":\"Gramss\"}','null'),(11,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','msatuan','create_process','http://localhost:81/ProduksiKM/index.php/msatuan/create_process','2018-03-12 06:39:02','{\"kode\":\"GR\",\"nama\":\"Gramss\"}','[]'),(12,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'edit_process','Success','msatuan','edit_process','http://localhost:81/ProduksiKM/index.php/msatuan/edit_process','2018-03-12 06:48:18','{\"kode\":\"MG\",\"nama\":\"Miligram\"}','[]'),(13,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'edit_process','Success','msatuan','edit_process','http://localhost:81/ProduksiKM/index.php/msatuan/edit_process','2018-03-12 06:49:35','{\"id\":\"3\",\"kode\":\"MG\",\"nama\":\"Miligram\"}','[]'),(14,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'edit_process','Success','msatuan','edit_process','http://localhost:81/ProduksiKM/index.php/msatuan/edit_process','2018-03-12 06:49:50','{\"id\":\"2\",\"kode\":\"GR\",\"nama\":\"Gram\"}','[]'),(15,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','msatuan','create_process','http://localhost:81/ProduksiKM/index.php/msatuan/create_process','2018-03-12 06:50:16','{\"kode\":\"XXX\",\"nama\":\"XXXXX\"}','[]'),(16,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'delete_process','Success','msatuan','delete_process','http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/4','2018-03-12 06:58:27','[]','[]'),(17,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'delete_process','Success','msatuan','delete_process','http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/4','2018-03-12 06:59:30','[]','[]'),(18,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'delete_process','Success','msatuan','delete_process','http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/3','2018-03-12 07:00:33','[]','[]'),(19,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','msatuan','create_process','http://localhost:81/ProduksiKM/index.php/msatuan/create_process','2018-03-12 07:00:46','{\"kode\":\"asdasdasdasd\",\"nama\":\"asdasd\"}','[]'),(20,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-12 20:05:35','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(21,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'delete_process','Success','msatuan','delete_process','http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/5','2018-03-12 20:17:29','[]','[]'),(22,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'delete_process','Success','msatuan','delete_process','http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/1','2018-03-12 21:05:19','[]','[]'),(23,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'delete_process','Success','msatuan','delete_process','http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/5','2018-03-12 21:42:15','[]','[]'),(24,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'delete_process','Success','msatuan','delete_process','http://localhost:81/ProduksiKM/index.php/msatuan/delete_process/4','2018-03-12 21:42:25','[]','[]'),(25,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','mmuang','create_process','http://localhost:81/ProduksiKM/index.php/mmuang/create_process','2018-03-12 21:49:32','{\"kode\":\"US$\",\"nama\":\"US Dollar\"}','[]'),(26,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'edit_process','Success','mmuang','edit_process','http://localhost:81/ProduksiKM/index.php/mmuang/edit_process','2018-03-12 21:49:38','{\"id\":\"2\",\"kode\":\"USD\",\"nama\":\"US Dollar\"}','[]'),(27,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','mnegara','create_process','http://localhost:81/ProduksiKM/index.php/mnegara/create_process','2018-03-12 21:52:59','{\"kode\":\"SG\",\"nama\":\"Singapore\"}','[]'),(28,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','mnegara','create_process','http://localhost:81/ProduksiKM/index.php/mnegara/create_process','2018-03-12 21:53:11','{\"kode\":\"MY\",\"nama\":\"Malaysias\"}','[]'),(29,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'edit_process','Success','mnegara','edit_process','http://localhost:81/ProduksiKM/index.php/mnegara/edit_process','2018-03-12 21:53:15','{\"id\":\"3\",\"kode\":\"MY\",\"nama\":\"Malaysia\"}','[]'),(30,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','mgudang','create_process','http://localhost:81/ProduksiKM/index.php/mgudang/create_process','2018-03-12 21:55:58','{\"kode\":\"GD1\",\"nama\":\"Gudang 1\"}','[]'),(31,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'edit_process','Success','mgudang','edit_process','http://localhost:81/ProduksiKM/index.php/mgudang/edit_process','2018-03-12 21:56:02','{\"id\":\"1\",\"kode\":\"GD1\",\"nama\":\"Gudang 12\"}','[]'),(32,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'edit_process','Success','mgudang','edit_process','http://localhost:81/ProduksiKM/index.php/mgudang/edit_process','2018-03-12 21:56:06','{\"id\":\"1\",\"kode\":\"GD1\",\"nama\":\"Gudang 1\"}','[]'),(33,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'logout_process','Success','home','logout_process','http://localhost:81/ProduksiKM/index.php/home/logout_process','2018-03-12 22:12:24','[]','[]'),(34,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-12 22:12:29','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(35,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'logout_process','Success','home','logout_process','http://localhost:81/ProduksiKM/index.php/home/logout_process','2018-03-12 22:44:26','[]','[]'),(36,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-12 22:44:33','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(37,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','msupplier','create_process','http://localhost:81/ProduksiKM/index.php/msupplier/create_process','2018-03-12 23:08:51','{\"kode\":\"SP1\",\"nama\":\"Supplier 1\",\"alamat\":\"Jalan Raden Saleh Dalam No 1\",\"telp\":\"061 4532658\",\"fax\":\"061 4553976\"}','[]'),(38,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'edit_process','Success','msupplier','edit_process','http://localhost:81/ProduksiKM/index.php/msupplier/edit_process','2018-03-12 23:11:28','{\"id\":\"1\",\"kode\":\"KMTJ\",\"nama\":\"Kilang Mie Tj Morawa\",\"alamat\":\"Jalan Tanjung Morawa Km..\",\"telp\":\"061 1111111\",\"fax\":\"061 2222\"}','[]'),(39,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'delete_process','Success','msupplier','delete_process','http://localhost:81/ProduksiKM/index.php/msupplier/delete_process/2','2018-03-12 23:11:34','[]','[]'),(40,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','mcustomer','create_process','http://localhost:81/ProduksiKM/index.php/mcustomer/create_process','2018-03-12 23:14:53','{\"kode\":\"TDY\",\"nama\":\"Teddy\",\"alamat\":\"Jalan Raden Saleh Dalam no 85\",\"telp\":\"08982874427\",\"fax\":\"081360149004\"}','[]'),(41,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','mbbaku','create_process','http://localhost:81/ProduksiKM/index.php/mbbaku/create_process','2018-03-13 00:47:54','{\"kode\":\"TRG\",\"nama\":\"Terigu\",\"satuan\":\"2\",\"harga\":\"100000\"}','[]'),(42,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'edit_process','Success','mbbaku','edit_process','http://localhost:81/ProduksiKM/index.php/mbbaku/edit_process','2018-03-13 00:52:45','{\"id\":\"2\",\"kode\":\"TRG\",\"nama\":\"Terigu\",\"satuan\":\"3\",\"harga\":\"100000\"}','[]'),(43,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','mbbaku','create_process','http://localhost:81/ProduksiKM/index.php/mbbaku/create_process','2018-03-13 00:59:33','{\"kode\":\"TPG\",\"nama\":\"Tepung\",\"satuan\":\"2\",\"harga\":\"5000\"}','[]'),(44,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','mbjadi','create_process','http://localhost:81/ProduksiKM/index.php/mbjadi/create_process','2018-03-13 01:03:13','{\"kode\":\"BHN1\",\"nama\":\"Bihun 1\",\"satuan\":\"1\",\"harga\":\"222\"}','[]'),(45,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'create_process','Success','mbwaste','create_process','http://localhost:81/ProduksiKM/index.php/mbwaste/create_process','2018-03-13 01:06:59','{\"kode\":\"KRG\",\"nama\":\"Karung\",\"satuan\":\"2\",\"harga\":\"2000\"}','[]'),(46,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-14 00:40:02','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(47,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0','','',1,'logout_process','Success','home','logout_process','http://localhost:81/ProduksiKM/index.php/home/logout_process','2018-03-14 01:55:35','[]','[]'),(48,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-18 23:25:44','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(49,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-19 03:45:53','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(50,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-19 19:59:20','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(51,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-19 20:47:01','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000001\",\"no_bukti_penerimaan_barang\":\"821498\",\"bahan_baku\":\"1\",\"jumlah\":\"20\",\"mata_uang\":\"1\",\"nilai_barang\":\"200000\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\"}','[]'),(52,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-19 20:48:56','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000001\",\"no_bukti_penerimaan_barang\":\"821498\",\"bahan_baku\":\"1\",\"jumlah\":\"20\",\"mata_uang\":\"1\",\"nilai_barang\":\"200000\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\"}','[]'),(53,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','mbbaku','create_process','http://localhost:81/ProduksiKM/index.php/mbbaku/create_process','2018-03-19 20:52:08','{\"kode\":\"TRG\",\"nama\":\"Terigu\",\"satuan\":\"1\",\"harga\":\"10000\"}','[]'),(54,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','mbbaku','create_process','http://localhost:81/ProduksiKM/index.php/mbbaku/create_process','2018-03-19 20:52:24','{\"kode\":\"GRM\",\"nama\":\"Garam\",\"satuan\":\"1\",\"harga\":\"14000\"}','[]'),(55,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','mgudang','create_process','http://localhost:81/ProduksiKM/index.php/mgudang/create_process','2018-03-19 20:52:37','{\"kode\":\"GD2\",\"nama\":\"Gudang 2\"}','[]'),(56,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-19 20:53:14','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000001\",\"no_bukti_penerimaan_barang\":\"123608\",\"bahan_baku\":\"1\",\"jumlah\":\"20\",\"mata_uang\":\"1\",\"nilai_barang\":\"200000\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\"}','[]'),(57,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-19 20:55:29','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000002\",\"no_bukti_penerimaan_barang\":\"999777\",\"bahan_baku\":\"1\",\"jumlah\":\"20\",\"mata_uang\":\"1\",\"nilai_barang\":\"200000\",\"gudang\":\"2\",\"negara_asal_barang\":\"1\"}','[]'),(58,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-19 20:56:34','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000004\",\"no_bukti_penerimaan_barang\":\"123123123\",\"bahan_baku\":\"2\",\"jumlah\":\"15\",\"mata_uang\":\"1\",\"nilai_barang\":\"200000\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\"}','[]'),(59,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-19 20:59:12','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000005\",\"no_bukti_penerimaan_barang\":\"123608\",\"bahan_baku\":\"1\",\"jumlah\":\"10\",\"mata_uang\":\"1\",\"nilai_barang\":\"200000\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\"}','[]'),(60,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-19 22:42:04','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000006\",\"tanggal_dokumen_pabean\":\"2018-03-19\",\"no_seri_barang\":\"0000111\",\"no_bukti_penerimaan_barang\":\"999776\",\"bahan_baku\":\"3\",\"jumlah\":\"35\",\"mata_uang\":\"1\",\"nilai_barang\":\"123456\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\"}','[]'),(61,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemakaianbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemakaianbbaku/create_process','2018-03-20 00:51:35','{\"no_bukti_pengeluaran\":\"111357\",\"tanggal_bukti_pengeluaran\":\"2018-03-18\",\"bahan_baku\":\"3\",\"jumlah\":\"14\",\"gudang\":\"1\"}','[]'),(62,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'logout_process','Success','home','logout_process','http://localhost:81/ProduksiKM/index.php/home/logout_process','2018-03-20 00:59:42','[]','[]'),(63,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-20 22:00:17','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(64,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-21 02:17:12','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(65,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-22 19:41:38','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(66,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-22 23:32:10','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(67,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbjadi','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process','2018-03-23 04:26:25','{\"no_bukti_penerimaan\":\"1112556\",\"tanggal_bukti_penerimaan\":\"2018-03-07\",\"barang_jadi\":\"1\",\"jumlah\":\"60\",\"gudang\":\"1\"}','[]'),(68,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpengeluaranbjadi','create_process','http://localhost:81/ProduksiKM/index.php/tpengeluaranbjadi/create_process','2018-03-23 04:27:18','{\"no_feb\":\"G0056677\",\"tanggal_feb\":\"2018-03-15\",\"no_bukti_pengeluaran\":\"111357\",\"tanggal_bukti_pengeluaran\":\"2018-03-27\",\"pembeli\":\"1\",\"negara_tujuan\":\"1\",\"barang_jadi\":\"1\",\"jumlah\":\"15\",\"mata_uang\":\"1\",\"nilai_barang\":\"123456\",\"gudang\":\"1\"}','[]'),(69,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'logout_process','Success','home','logout_process','http://localhost:81/ProduksiKM/index.php/home/logout_process','2018-03-23 04:47:59','[]','[]'),(70,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-23 04:48:06','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(71,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-23 21:03:09','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(72,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','msatuan','create_process','http://localhost:81/ProduksiKM/index.php/msatuan/create_process','2018-03-23 21:04:27','{\"kode\":\"TON\",\"nama\":\"Tons\"}','[]'),(73,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','mmuang','create_process','http://localhost:81/ProduksiKM/index.php/mmuang/create_process','2018-03-23 21:05:07','{\"kode\":\"SGD\",\"nama\":\"Singapore Dollar\"}','[]'),(74,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'delete_process','Success','mmuang','delete_process','http://localhost:81/ProduksiKM/index.php/mmuang/delete_process/3','2018-03-23 21:05:14','[]','[]'),(75,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','mbbaku','create_process','http://localhost:81/ProduksiKM/index.php/mbbaku/create_process','2018-03-23 21:07:31','{\"kode\":\"CASANA\",\"nama\":\"Casana \",\"satuan\":\"1\",\"harga\":\"111111\"}','[]'),(76,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-23 21:11:29','{\"jenis_dokumen\":\"1111111\",\"no_dokumen_pabean\":\"0000007\",\"tanggal_dokumen_pabean\":\"2018-03-24\",\"no_seri_barang\":\"111111\",\"no_bukti_penerimaan_barang\":\"123123123\",\"bahan_baku\":\"4\",\"jumlah\":\"1000\",\"mata_uang\":\"2\",\"nilai_barang\":\"10000\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\"}','[]'),(77,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemakaianbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemakaianbbaku/create_process','2018-03-23 21:15:21','{\"no_bukti_pengeluaran\":\"111357\",\"tanggal_bukti_pengeluaran\":\"2018-03-24\",\"bahan_baku\":\"4\",\"jumlah\":\"500\",\"gudang\":\"1\"}','[]'),(78,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','mbjadi','create_process','http://localhost:81/ProduksiKM/index.php/mbjadi/create_process','2018-03-23 21:17:32','{\"kode\":\"SPS\",\"nama\":\"Super Potato Startch\",\"satuan\":\"1\",\"harga\":\"10000\"}','[]'),(79,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbjadi','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process','2018-03-23 21:19:28','{\"no_bukti_penerimaan\":\"11125562\",\"tanggal_bukti_penerimaan\":\"2018-03-24\",\"barang_jadi\":\"2\",\"jumlah\":\"520\",\"gudang\":\"2\"}','[]'),(80,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-24 01:29:38','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(81,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbjadi','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process','2018-03-24 01:45:46','{\"no_bukti_penerimaan\":\"1112556\",\"tanggal_bukti_penerimaan\":\"2018-03-14\",\"barang_jadi\":\"1\",\"jumlah\":\"1\",\"gudang\":\"1\",\"no_pemakaian_bahan_baku\":[\"OBB20180300001\",\"OBB20180300002\"]}','[]'),(82,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbjadi','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process','2018-03-24 01:58:38','{\"no_bukti_penerimaan\":\"11125562\",\"tanggal_bukti_penerimaan\":\"2018-03-12\",\"barang_jadi\":\"1\",\"jumlah\":\"2\",\"gudang\":\"1\",\"no_pemakaian_bahan_baku\":[\"OBB20180300001\",\"OBB20180300002\"]}','[]'),(83,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-24 02:06:09','{\"jenis_dokumen\":\"1111111222\",\"no_dokumen_pabean\":\"0000009\",\"tanggal_dokumen_pabean\":\"2018-03-07\",\"no_seri_barang\":\"2222\",\"no_bukti_penerimaan_barang\":\"22222\",\"bahan_baku\":\"1\",\"jumlah\":\"200\",\"mata_uang\":\"1\",\"nilai_barang\":\"123123123\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"1000000\"}','[]'),(84,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpengeluaranbjadi','create_process','http://localhost:81/ProduksiKM/index.php/tpengeluaranbjadi/create_process','2018-03-24 02:06:52','{\"no_feb\":\"G00566773\",\"tanggal_feb\":\"2018-03-23\",\"no_bukti_pengeluaran\":\"33333\",\"tanggal_bukti_pengeluaran\":\"2018-03-28\",\"pembeli\":\"1\",\"negara_tujuan\":\"1\",\"barang_jadi\":\"1\",\"jumlah\":\"10000\",\"mata_uang\":\"1\",\"nilai_barang\":\"123456\",\"gudang\":\"1\",\"harga_satuan\":\"3000000\"}','[]'),(85,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-25 20:12:01','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(86,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpenyelesaianbwaste','create_process','http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/create_process','2018-03-25 20:31:42','{\"no_bc_24\":\"113123\",\"tanggal_bc_24\":\"2018-03-21\",\"barang_waste\":\"1\",\"jumlah\":\"30\",\"nilai\":\"1000\",\"gudang\":\"1\",\"no_pengeluaran_barang_jadi\":[\"OBJ20180300001\",\"OBJ20180300002\"]}','[]'),(87,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpenyelesaianbwaste','create_process','http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/create_process','2018-03-25 20:32:27','{\"no_bc_24\":\"113123\",\"tanggal_bc_24\":\"2018-03-21\",\"barang_waste\":\"1\",\"jumlah\":\"30\",\"nilai\":\"1000\",\"gudang\":\"1\",\"no_pengeluaran_barang_jadi\":[\"OBJ20180300001\",\"OBJ20180300002\"]}','[]'),(88,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-25 21:57:16','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000010\",\"tanggal_dokumen_pabean\":\"2018-03-27\",\"no_seri_barang\":\"111111\",\"no_bukti_penerimaan_barang\":\"123123123\",\"bahan_baku\":\"4\",\"jumlah\":\"600\",\"mata_uang\":\"1\",\"nilai_barang\":\"50000\",\"gudang\":\"2\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"1000000\"}','[]'),(89,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpemasukanbbaku','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process','2018-03-25 23:00:41','{\"id\":\"10\",\"no_transaksi\":\"IBB20180300010\",\"tanggal\":\"2018-03-25 21:57:16\",\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000010\",\"tanggal_dokumen_pabean\":\"2018-03-27 00:00:00\",\"no_seri_barang\":\"111111\",\"no_bukti_penerimaan_barang\":\"123123123\",\"bahan_baku\":\"4\",\"jumlah\":\"700\",\"mata_uang\":\"1\",\"nilai_barang\":\"50000\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"1000000\"}','[]'),(90,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemakaianbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemakaianbbaku/create_process','2018-03-26 00:04:08','{\"no_bukti_pengeluaran\":\"1236899\",\"tanggal_bukti_pengeluaran\":\"2018-03-15\",\"bahan_baku\":\"4\",\"jumlah\":\"1050\",\"gudang\":\"1\"}','[]'),(91,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemakaianbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemakaianbbaku/create_process','2018-03-26 00:22:39','{\"no_bukti_pengeluaran\":\"12368992\",\"tanggal_bukti_pengeluaran\":\"2018-03-30\",\"bahan_baku\":\"4\",\"jumlah\":\"75\",\"gudang\":\"1\"}','[]'),(92,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpemakaianbbaku','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemakaianbbaku/confirm_process','2018-03-26 00:24:53','{\"id\":\"4\",\"no_transaksi\":\"OBB20180300004\",\"tanggal\":\"2018-03-26 00:22:39\",\"no_bukti_pengeluaran\":\"12368992\",\"tanggal_bukti_pengeluaran\":\"2018-03-30 00:00:00\",\"bahan_baku\":\"4\",\"jumlah\":\"80\",\"gudang\":\"1\"}','[]'),(93,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','mbjadi','create_process','http://localhost:81/ProduksiKM/index.php/mbjadi/create_process','2018-03-26 00:25:54','{\"kode\":\"MIAC\",\"nama\":\"Mie Aceh\",\"satuan\":\"3\",\"harga\":\"50000\"}','[]'),(94,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbjadi','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process','2018-03-26 00:26:24','{\"no_bukti_penerimaan\":\"1112556\",\"tanggal_bukti_penerimaan\":\"2018-03-23\",\"barang_jadi\":\"3\",\"jumlah\":\"600\",\"gudang\":\"1\",\"no_pemakaian_bahan_baku\":[\"OBB20180300001\",\"OBB20180300002\",\"OBB20180300004\"]}','[]'),(95,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpemasukanbjadi','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/confirm_process','2018-03-26 00:27:31','{\"id\":\"5\",\"no_transaksi\":\"IBJ20180300005\",\"tanggal\":\"2018-03-26 00:26:24\",\"no_bukti_penerimaan\":\"1112556\",\"tanggal_bukti_penerimaan\":\"2018-03-23 00:00:00\",\"barang_jadi\":\"1\",\"jumlah\":\"10000\",\"gudang\":\"1\"}','[]'),(96,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpemasukanbjadi','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/confirm_process','2018-03-26 00:27:46','{\"id\":\"5\",\"no_transaksi\":\"IBJ20180300005\",\"tanggal\":\"2018-03-26 00:26:24\",\"no_bukti_penerimaan\":\"1112556\",\"tanggal_bukti_penerimaan\":\"2018-03-23 00:00:00\",\"barang_jadi\":\"1\",\"jumlah\":\"10000\",\"gudang\":\"1\",\"no_pemakaian_bahan_baku\":[\"OBB20180300001\",\"OBB20180300002\"]}','[]'),(97,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpengeluaranbjadi','create_process','http://localhost:81/ProduksiKM/index.php/tpengeluaranbjadi/create_process','2018-03-26 00:29:13','{\"no_feb\":\"G0056677\",\"tanggal_feb\":\"2018-03-29\",\"no_bukti_pengeluaran\":\"33333\",\"tanggal_bukti_pengeluaran\":\"2018-03-29\",\"pembeli\":\"1\",\"negara_tujuan\":\"1\",\"barang_jadi\":\"1\",\"jumlah\":\"5045\",\"mata_uang\":\"1\",\"nilai_barang\":\"50000\",\"gudang\":\"1\",\"harga_satuan\":\"1000000\"}','[]'),(98,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpengeluaranbjadi','confirm_process','http://localhost:81/ProduksiKM/index.php/tpengeluaranbjadi/confirm_process','2018-03-26 00:29:29','{\"id\":\"3\",\"no_transaksi\":\"OBJ20180300003\",\"tanggal\":\"2018-03-26 00:29:13\",\"no_feb\":\"G0056677\",\"tanggal_feb\":\"2018-03-29 00:00:00\",\"no_bukti_pengeluaran\":\"33333\",\"tanggal_bukti_pengeluaran\":\"2018-03-29 00:00:00\",\"pembeli\":\"1\",\"negara_tujuan\":\"1\",\"barang_jadi\":\"1\",\"jumlah\":\"5045\",\"mata_uang\":\"1\",\"nilai_barang\":\"50000\",\"gudang\":\"1\",\"harga_satuan\":\"1000000\"}','[]'),(99,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpenyelesaianbwaste','create_process','http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/create_process','2018-03-26 00:32:38','{\"no_bc_24\":\"113123\",\"tanggal_bc_24\":\"2018-03-22\",\"barang_waste\":\"1\",\"jumlah\":\"90\",\"nilai\":\"123\",\"gudang\":\"1\",\"no_pengeluaran_barang_jadi\":[\"OBJ20180300003\"]}','[]'),(100,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpenyelesaianbwaste','confirm_process','http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/confirm_process','2018-03-26 00:32:58','{\"id\":\"4\",\"no_transaksi\":\"IBJ20180300004\",\"tanggal\":\"2018-03-26 00:32:38\",\"no_bc_24\":\"113123\",\"tanggal_bc_24\":\"2018-03-22 00:00:00\",\"barang_waste\":\"1\",\"jumlah\":\"90\",\"nilai\":\"123\",\"gudang\":\"1\",\"no_pengeluaran_barang_jadi\":[\"OBJ20180300001\"]}','[]'),(101,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbjadi','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process','2018-03-26 00:54:07','{\"no_bukti_penerimaan\":\"11125562\",\"tanggal_bukti_penerimaan\":\"2018-03-28\",\"barang_jadi\":\"3\",\"jumlah\":\"5\",\"gudang\":\"1\",\"no_pemakaian_bahan_baku\":[\"OBB20180300002\"]}','[]'),(102,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpemasukanbjadi','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/confirm_process','2018-03-26 00:54:17','{\"id\":\"6\",\"no_transaksi\":\"IBJ20180300006\",\"tanggal\":\"2018-03-26 00:54:07\",\"no_bukti_penerimaan\":\"11125562\",\"tanggal_bukti_penerimaan\":\"2018-03-28 00:00:00\",\"barang_jadi\":\"3\",\"jumlah\":\"5\",\"gudang\":\"1\"}','[]'),(103,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbjadi','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbjadi/create_process','2018-03-26 00:54:59','{\"no_bukti_penerimaan\":\"123123\",\"tanggal_bukti_penerimaan\":\"2018-03-21\",\"barang_jadi\":\"3\",\"jumlah\":\"10\",\"gudang\":\"1\",\"no_pemakaian_bahan_baku\":[\"OBB20180300003\"]}','[]'),(104,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpenyelesaianbwaste','create_process','http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/create_process','2018-03-26 01:10:30','{\"no_bc_24\":\"113123\",\"tanggal_bc_24\":\"2018-03-14\",\"barang_waste\":\"1\",\"jumlah\":\"25\",\"nilai\":\"25000\",\"gudang\":\"1\",\"no_pengeluaran_barang_jadi\":[\"OBJ20180300002\"]}','[]'),(105,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'delete_process','Success','tpenyelesaianbwaste','delete_process','http://localhost:81/ProduksiKM/index.php/tpenyelesaianbwaste/delete_process/5','2018-03-26 01:12:09','[]','[]'),(106,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-26 04:49:27','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(107,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-26 19:38:37','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(108,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-28 01:48:03','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(109,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-28 04:41:46','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(110,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-28 04:45:27','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000011\",\"tanggal_dokumen_pabean\":\"2018-03-31\",\"no_seri_barang\":\"111123\",\"no_bukti_penerimaan_barang\":\"1231231255\",\"tanggal_bukti_penerimaan_barang\":\"2018-03-30\",\"bahan_baku\":\"2\",\"jumlah\":\"500\",\"mata_uang\":\"1\",\"nilai_barang\":\"500000\",\"gudang\":\"2\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"5000\"}','[]'),(111,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpemasukanbbaku','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process','2018-03-28 04:45:47','{\"id\":\"11\",\"no_transaksi\":\"IBB20180300011\",\"tanggal\":\"2018-03-28 04:45:27\",\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000011\",\"tanggal_dokumen_pabean\":\"2018-03-31\",\"no_seri_barang\":\"111123\",\"no_bukti_penerimaan_barang\":\"1231231255\",\"tanggal_bukti_penerimaan_barang\":\"2018-03-30\",\"bahan_baku\":\"2\",\"jumlah\":\"500\",\"mata_uang\":\"1\",\"nilai_barang\":\"500000\",\"gudang\":\"2\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"5000\"}','[]'),(112,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-28 04:46:37','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000012\",\"tanggal_dokumen_pabean\":\"2018-03-23\",\"no_seri_barang\":\"213\",\"no_bukti_penerimaan_barang\":\"12312312\",\"tanggal_bukti_penerimaan_barang\":\"2018-03-29\",\"bahan_baku\":\"1\",\"jumlah\":\"12\",\"mata_uang\":\"1\",\"nilai_barang\":\"1233\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"6000\"}','[]'),(113,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','mbbaku','create_process','http://localhost:81/ProduksiKM/index.php/mbbaku/create_process','2018-03-28 04:47:15','{\"kode\":\"GULA\",\"nama\":\"Gula\",\"satuan\":\"3\",\"harga\":\"6000\"}','[]'),(114,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-28 04:50:39','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000013\",\"tanggal_dokumen_pabean\":\"2018-03-31\",\"no_seri_barang\":\"565465465465\",\"no_bukti_penerimaan_barang\":\"123123123\",\"tanggal_bukti_penerimaan_barang\":\"2018-03-29\",\"bahan_baku\":\"1\",\"jumlah\":\"70\",\"mata_uang\":\"1\",\"nilai_barang\":\"11111111\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"111111\"}','[]'),(115,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpemasukanbbaku','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process','2018-03-28 04:50:48','{\"id\":\"12\",\"no_transaksi\":\"IBB20180300012\",\"tanggal\":\"2018-03-28 04:46:37\",\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000012\",\"tanggal_dokumen_pabean\":\"2018-03-23 00:00:00\",\"no_seri_barang\":\"213\",\"no_bukti_penerimaan_barang\":\"12312312\",\"tanggal_bukti_penerimaan_barang\":\"2018-03-29 00:00:00\",\"bahan_baku\":\"1\",\"jumlah\":\"12\",\"mata_uang\":\"1\",\"nilai_barang\":\"1233\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"6000\"}','[]'),(116,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpemasukanbbaku','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process','2018-03-28 04:50:54','{\"id\":\"13\",\"no_transaksi\":\"IBB20180300013\",\"tanggal\":\"2018-03-28 04:50:39\",\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000013\",\"tanggal_dokumen_pabean\":\"2018-03-31 00:00:00\",\"no_seri_barang\":\"565465465465\",\"no_bukti_penerimaan_barang\":\"123123123\",\"tanggal_bukti_penerimaan_barang\":\"2018-03-29 00:00:00\",\"bahan_baku\":\"1\",\"jumlah\":\"70\",\"mata_uang\":\"1\",\"nilai_barang\":\"11111111\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"111111\"}','[]'),(117,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-03-28 19:26:07','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(118,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-28 20:12:21','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000014\",\"tanggal_dokumen_pabean\":\"2018-03-30\",\"no_seri_barang\":\"12314\",\"no_bukti_penerimaan_barang\":\"123124\",\"tanggal_bukti_penerimaan_barang\":\"2018-03-26\",\"bahan_baku\":\"5\",\"jumlah\":\"500\",\"mata_uang\":\"1\",\"nilai_barang\":\"5000\",\"gudang\":\"2\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"123123\"}','[]'),(119,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-03-28 20:12:35','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000014\",\"tanggal_dokumen_pabean\":\"2018-03-30\",\"no_seri_barang\":\"12314\",\"no_bukti_penerimaan_barang\":\"123124\",\"tanggal_bukti_penerimaan_barang\":\"2018-03-26\",\"bahan_baku\":\"5\",\"jumlah\":\"500\",\"mata_uang\":\"1\",\"nilai_barang\":\"5000\",\"gudang\":\"2\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"123123\"}','[]'),(120,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpemasukanbbaku','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process','2018-03-28 20:13:27','{\"id\":\"14\",\"no_transaksi\":\"IBB20180300013\",\"tanggal\":\"2018-03-28 20:12:21\",\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000014\",\"tanggal_dokumen_pabean\":\"2018-03-30 00:00:00\",\"no_seri_barang\":\"12314\",\"no_bukti_penerimaan_barang\":\"123124\",\"tanggal_bukti_penerimaan_barang\":\"2018-03-26 00:00:00\",\"bahan_baku\":\"5\",\"jumlah\":\"500\",\"mata_uang\":\"1\",\"nilai_barang\":\"5000\",\"gudang\":\"2\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"123123\"}','[]'),(121,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpemasukanbbaku','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process','2018-03-28 20:13:35','{\"id\":\"15\",\"no_transaksi\":\"IBB20180300014\",\"tanggal\":\"2018-03-28 20:12:35\",\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000014\",\"tanggal_dokumen_pabean\":\"2018-03-30 00:00:00\",\"no_seri_barang\":\"12314\",\"no_bukti_penerimaan_barang\":\"123124\",\"tanggal_bukti_penerimaan_barang\":\"2018-03-26 00:00:00\",\"bahan_baku\":\"5\",\"jumlah\":\"500\",\"mata_uang\":\"1\",\"nilai_barang\":\"5000\",\"gudang\":\"2\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"123123\"}','[]'),(122,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-04-28 20:17:29','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(123,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-04-28 20:20:53','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000001\",\"tanggal_dokumen_pabean\":\"2018-04-12\",\"no_seri_barang\":\"4846894665\",\"no_bukti_penerimaan_barang\":\"2111351651\",\"tanggal_bukti_penerimaan_barang\":\"2018-04-25\",\"bahan_baku\":\"1\",\"jumlah\":\"500\",\"mata_uang\":\"1\",\"nilai_barang\":\"200000\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"6000\"}','[]'),(124,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'confirm_process','Success','tpemasukanbbaku','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process','2018-04-28 20:22:26','{\"id\":\"16\",\"no_transaksi\":\"IBB20180400002\",\"tanggal\":\"2018-04-28 20:20:53\",\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000001\",\"tanggal_dokumen_pabean\":\"2018-04-12 00:00:00\",\"no_seri_barang\":\"4846894665\",\"no_bukti_penerimaan_barang\":\"2111351651\",\"tanggal_bukti_penerimaan_barang\":\"2018-04-25 00:00:00\",\"bahan_baku\":\"1\",\"jumlah\":\"500\",\"mata_uang\":\"1\",\"nilai_barang\":\"200000\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"6000\"}','[]'),(125,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-05-28 20:23:04','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(126,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-05-28 20:23:47','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000009\",\"tanggal_dokumen_pabean\":\"2018-05-11\",\"no_seri_barang\":\"111123\",\"no_bukti_penerimaan_barang\":\"2111351651\",\"tanggal_bukti_penerimaan_barang\":\"2018-05-30\",\"bahan_baku\":\"1\",\"jumlah\":\"60\",\"mata_uang\":\"1\",\"nilai_barang\":\"7000\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"60\"}','[]'),(127,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-06-28 20:25:44','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(128,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-06-28 20:31:19','{\"jenis_dokumen\":\"PBN\",\"no_dokumen_pabean\":\"0000011\",\"tanggal_dokumen_pabean\":\"2018-06-20\",\"no_seri_barang\":\"4846894665\",\"no_bukti_penerimaan_barang\":\"123608\",\"tanggal_bukti_penerimaan_barang\":\"2018-04-25\",\"bahan_baku\":\"1\",\"jumlah\":\"50\",\"mata_uang\":\"1\",\"nilai_barang\":\"10000\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"10000\"}','[]'),(129,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'delete_process','Success','tpemasukanbbaku','delete_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/delete_process/17','2018-06-28 20:31:52','[]','[]'),(130,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'delete_process','Success','tpemasukanbbaku','delete_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/delete_process/24','2018-06-28 20:31:58','[]','[]'),(131,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'logout_process','Success','home','logout_process','http://localhost:81/ProduksiKM/index.php/home/logout_process','2018-03-28 22:09:40','[]','[]'),(132,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-04-01 19:38:40','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(133,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'edit_process','Success','msatuan','edit_process','http://localhost:81/ProduksiKM/index.php/msatuan/edit_process','2018-04-01 19:39:09','{\"id\":\"6\",\"kode\":\"TON\",\"nama\":\"Ton\"}','[]'),(134,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','msatuan','create_process','http://localhost:81/ProduksiKM/index.php/msatuan/create_process','2018-04-01 19:39:35','{\"kode\":\"SAK\",\"nama\":\"sak\"}','[]'),(135,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'edit_process','Success','msatuan','edit_process','http://localhost:81/ProduksiKM/index.php/msatuan/edit_process','2018-04-01 19:39:51','{\"id\":\"7\",\"kode\":\"SAK\",\"nama\":\"Sak\"}','[]'),(136,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','mmuang','create_process','http://localhost:81/ProduksiKM/index.php/mmuang/create_process','2018-04-01 19:58:17','{\"kode\":\"SGD\",\"nama\":\"Sgd\"}','[]'),(137,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'edit_process','Success','mmuang','edit_process','http://localhost:81/ProduksiKM/index.php/mmuang/edit_process','2018-04-01 19:58:23','{\"id\":\"4\",\"kode\":\"SGD\",\"nama\":\"Sgd\"}','[]'),(138,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'delete_process','Success','mmuang','delete_process','http://localhost:81/ProduksiKM/index.php/mmuang/delete_process/4','2018-04-01 19:58:29','[]','[]'),(139,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','mmuang','create_process','http://localhost:81/ProduksiKM/index.php/mmuang/create_process','2018-04-01 20:00:35','{\"kode\":\"SGD\",\"nama\":\"Sgd\"}','[]'),(140,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'edit_process','Success','mmuang','edit_process','http://localhost:81/ProduksiKM/index.php/mmuang/edit_process','2018-04-01 20:00:47','{\"id\":\"5\",\"kode\":\"SGD\",\"nama\":\"Singapore Dollar\"}','[]'),(141,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'create_process','Success','mnegara','create_process','http://localhost:81/ProduksiKM/index.php/mnegara/create_process','2018-04-01 20:22:43','{\"kode\":\"CN\",\"nama\":\"China\"}','[]'),(142,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-04-02 20:24:53','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(143,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-04-11 20:08:44','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(144,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-04-11 20:20:20','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(145,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'edit_process','Success','mprofile','edit_process','http://localhost:81/ProduksiKM/index.php/mprofile/edit_process','2018-04-11 21:07:36','{\"id\":\"1\",\"password\":\"87654321\",\"confirm_password\":\"87654321\"}','[]'),(146,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'logout_process','Success','home','logout_process','http://localhost:81/ProduksiKM/index.php/home/logout_process','2018-04-11 21:07:41','[]','[]'),(147,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-04-11 21:07:50','{\"user_name\":\"admin\",\"password\":\"87654321\"}','[]'),(148,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'edit_process','Success','mprofile','edit_process','http://localhost:81/ProduksiKM/index.php/mprofile/edit_process','2018-04-11 21:08:01','{\"id\":\"1\",\"password\":\"87654321\",\"confirm_password\":\"87654321\"}','[]'),(149,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'edit_process','Success','mprofile','edit_process','http://localhost:81/ProduksiKM/index.php/mprofile/edit_process','2018-04-11 21:10:10','{\"id\":\"1\",\"password\":\"12345678\",\"confirm_password\":\"12345678\"}','[]'),(150,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',1,'logout_process','Success','home','logout_process','http://localhost:81/ProduksiKM/index.php/home/logout_process','2018-04-11 21:24:12','[]','[]'),(151,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-04-11 21:24:16','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(152,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36','','',1,'logout_process','Success','home','logout_process','http://localhost:81/ProduksiKM/index.php/home/logout_process','2018-04-11 21:31:59','[]','[]'),(153,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-04-12 20:17:27','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(154,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-04-13 00:21:35','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(155,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-04-22 22:06:05','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(156,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-05-01 21:29:57','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(157,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0','','',NULL,'login_process','Success','home','login_process','http://localhost:81/ProduksiKM/index.php/home/login_process','2018-09-23 22:36:28','{\"user_name\":\"admin\",\"password\":\"12345678\"}','[]'),(158,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0','','',1,'create_process','Success','mbbaku','create_process','http://localhost:81/ProduksiKM/index.php/mbbaku/create_process','2018-09-23 22:37:20','{\"kode\":\"TED\",\"nama\":\"TEDDY\",\"satuan\":\"1\",\"harga\":\"12500\"}','[]'),(159,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-09-23 22:38:58','{\"jenis_dokumen\":\"12312312\",\"no_dokumen_pabean\":\"123123123\",\"tanggal_dokumen_pabean\":\"2018-09-25\",\"no_seri_barang\":\"123123\",\"no_bukti_penerimaan_barang\":\"123123\",\"tanggal_bukti_penerimaan_barang\":\"2018-09-28\",\"bahan_baku\":\"6\",\"jumlah\":\"17\",\"mata_uang\":\"1\",\"nilai_barang\":\"12500\",\"gudang\":\"2\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"12500\"}','[]'),(160,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0','','',1,'confirm_process','Success','tpemasukanbbaku','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process','2018-09-23 22:39:11','{\"id\":\"25\",\"no_transaksi\":\"IBB20180900001\",\"tanggal\":\"2018-09-23 22:38:58\",\"jenis_dokumen\":\"12312312\",\"no_dokumen_pabean\":\"123123123\",\"tanggal_dokumen_pabean\":\"2018-09-25 00:00:00\",\"no_seri_barang\":\"123123\",\"no_bukti_penerimaan_barang\":\"123123\",\"tanggal_bukti_penerimaan_barang\":\"2018-09-28 00:00:00\",\"bahan_baku\":\"6\",\"jumlah\":\"17\",\"mata_uang\":\"1\",\"nilai_barang\":\"12500\",\"gudang\":\"2\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"12500\"}','[]'),(161,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0','','',1,'create_process','Success','tpemasukanbbaku','create_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/create_process','2018-09-23 22:39:40','{\"jenis_dokumen\":\"12312312\",\"no_dokumen_pabean\":\"123123123\",\"tanggal_dokumen_pabean\":\"2018-09-19\",\"no_seri_barang\":\"123123\",\"no_bukti_penerimaan_barang\":\"123123\",\"tanggal_bukti_penerimaan_barang\":\"2018-09-18\",\"bahan_baku\":\"6\",\"jumlah\":\"6\",\"mata_uang\":\"1\",\"nilai_barang\":\"12500\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"2\"}','[]'),(162,'::1','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0','','',1,'confirm_process','Success','tpemasukanbbaku','confirm_process','http://localhost:81/ProduksiKM/index.php/tpemasukanbbaku/confirm_process','2018-09-23 22:39:49','{\"id\":\"26\",\"no_transaksi\":\"IBB20180900002\",\"tanggal\":\"2018-09-23 22:39:40\",\"jenis_dokumen\":\"12312312\",\"no_dokumen_pabean\":\"123123123\",\"tanggal_dokumen_pabean\":\"2018-09-19 00:00:00\",\"no_seri_barang\":\"123123\",\"no_bukti_penerimaan_barang\":\"123123\",\"tanggal_bukti_penerimaan_barang\":\"2018-09-18 00:00:00\",\"bahan_baku\":\"6\",\"jumlah\":\"6\",\"mata_uang\":\"1\",\"nilai_barang\":\"12500\",\"gudang\":\"1\",\"negara_asal_barang\":\"1\",\"supplier\":\"1\",\"harga_satuan\":\"2\"}','[]');

UNLOCK TABLES;

/*Table structure for table `core_content` */

DROP TABLE IF EXISTS `core_content`;

CREATE TABLE `core_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `record_type` int(10) DEFAULT NULL,
  `text_01` varchar(500) DEFAULT NULL,
  `text_02` varchar(500) DEFAULT NULL,
  `text_03` varchar(500) DEFAULT NULL,
  `text_04` varchar(500) DEFAULT NULL,
  `text_05` varchar(500) DEFAULT NULL,
  `text_06` varchar(500) DEFAULT NULL,
  `text_07` varchar(500) DEFAULT NULL,
  `text_08` varchar(500) DEFAULT NULL,
  `text_09` varchar(500) DEFAULT NULL,
  `int_01` int(10) DEFAULT NULL,
  `int_02` int(10) DEFAULT NULL,
  `int_03` int(10) DEFAULT NULL,
  `int_04` int(10) DEFAULT NULL,
  `int_05` int(10) DEFAULT NULL,
  `int_06` int(10) DEFAULT NULL,
  `int_07` int(10) DEFAULT NULL,
  `int_08` int(10) DEFAULT NULL,
  `int_09` int(10) DEFAULT NULL,
  `note_01` text,
  `note_02` text,
  `note_03` text,
  `note_04` text,
  `note_05` text,
  `note_06` text,
  `note_07` text,
  `note_08` text,
  `note_09` text,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `core_content` */

LOCK TABLES `core_content` WRITE;

UNLOCK TABLES;

/*Table structure for table `core_email_log` */

DROP TABLE IF EXISTS `core_email_log`;

CREATE TABLE `core_email_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mail_from` varchar(255) DEFAULT NULL,
  `mail_from_name` varchar(255) DEFAULT NULL,
  `mail_to` text,
  `mail_cc` text,
  `mail_bcc` text,
  `mail_subject` varchar(255) DEFAULT NULL,
  `mail_body` text,
  `mail_error` text,
  `mail_error_info` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `core_email_log` */

LOCK TABLES `core_email_log` WRITE;

UNLOCK TABLES;

/*Table structure for table `data_sessions` */

DROP TABLE IF EXISTS `data_sessions`;

CREATE TABLE `data_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `data_sessions` */

LOCK TABLES `data_sessions` WRITE;

insert  into `data_sessions`(`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) values ('f29a9a58d86632f578446ba2956a3f42','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0',1525235387,'a:4:{s:9:\"user_data\";s:0:\"\";s:19:\"CLIENTSESSIONENABLE\";i:1;s:15:\"session_user_id\";s:1:\"1\";s:17:\"session_user_name\";s:5:\"admin\";}'),('2513cd789fa3152e61abe43ce2c66504','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0',1537767689,'a:4:{s:9:\"user_data\";s:0:\"\";s:19:\"CLIENTSESSIONENABLE\";i:1;s:15:\"session_user_id\";s:1:\"1\";s:17:\"session_user_name\";s:5:\"admin\";}'),('6678ab6739d95c87480ecb55b63ab5a6','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0',1524460456,'a:4:{s:9:\"user_data\";s:0:\"\";s:19:\"CLIENTSESSIONENABLE\";i:1;s:15:\"session_user_id\";s:1:\"1\";s:17:\"session_user_name\";s:5:\"admin\";}'),('4540c3d22a6c1903384c938f5845dae4','::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0',1524106543,'');

UNLOCK TABLES;

/*Table structure for table `master_bahan_baku` */

DROP TABLE IF EXISTS `master_bahan_baku`;

CREATE TABLE `master_bahan_baku` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `satuan` int(10) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `satuan` (`satuan`),
  KEY `kode_status` (`kode`,`record_status`),
  CONSTRAINT `mbbaku_satuan` FOREIGN KEY (`satuan`) REFERENCES `master_satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `master_bahan_baku` */

LOCK TABLES `master_bahan_baku` WRITE;

insert  into `master_bahan_baku`(`id`,`kode`,`nama`,`satuan`,`harga`,`import_flag`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,'TPG','Tepung',2,'5000','',1,'2018-03-13 00:59:33',NULL,NULL,'ACTIVE'),(2,'TRG','Terigu',1,'10000','',1,'2018-03-19 20:52:08',NULL,NULL,'ACTIVE'),(3,'GRM','Garam',1,'14000','',1,'2018-03-19 20:52:24',NULL,NULL,'ACTIVE'),(4,'CASANA','Casana ',1,'111111','',1,'2018-03-23 21:07:31',NULL,NULL,'ACTIVE'),(5,'GULA','Gula',3,'6000','',1,'2018-03-28 04:47:15',NULL,NULL,'ACTIVE'),(6,'TED','TEDDY',1,'12500','',1,'2018-09-23 22:37:19',NULL,NULL,'ACTIVE');

UNLOCK TABLES;

/*Table structure for table `master_barang_jadi` */

DROP TABLE IF EXISTS `master_barang_jadi`;

CREATE TABLE `master_barang_jadi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `satuan` int(10) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `satuan` (`satuan`),
  KEY `kode_status` (`kode`,`record_status`),
  CONSTRAINT `mbjadi_satuan` FOREIGN KEY (`satuan`) REFERENCES `master_satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `master_barang_jadi` */

LOCK TABLES `master_barang_jadi` WRITE;

insert  into `master_barang_jadi`(`id`,`kode`,`nama`,`satuan`,`harga`,`import_flag`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,'BHN1','Bihun 1',1,'222','',1,'2018-03-13 01:03:13',NULL,NULL,'ACTIVE'),(2,'SPS','Super Potato Startch',1,'10000','',1,'2018-03-23 21:17:32',NULL,NULL,'ACTIVE'),(3,'MIAC','Mie Aceh',3,'50000','',1,'2018-03-26 00:25:54',NULL,NULL,'ACTIVE');

UNLOCK TABLES;

/*Table structure for table `master_barang_waste` */

DROP TABLE IF EXISTS `master_barang_waste`;

CREATE TABLE `master_barang_waste` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `satuan` int(10) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `satuan` (`satuan`),
  KEY `kode_status` (`kode`,`record_status`),
  CONSTRAINT `mbwaste_satuan` FOREIGN KEY (`satuan`) REFERENCES `master_satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `master_barang_waste` */

LOCK TABLES `master_barang_waste` WRITE;

insert  into `master_barang_waste`(`id`,`kode`,`nama`,`satuan`,`harga`,`import_flag`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,'KRG','Karung',2,'2000','',1,'2018-03-13 01:06:59',NULL,NULL,'ACTIVE');

UNLOCK TABLES;

/*Table structure for table `master_customer` */

DROP TABLE IF EXISTS `master_customer`;

CREATE TABLE `master_customer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `master_customer` */

LOCK TABLES `master_customer` WRITE;

insert  into `master_customer`(`id`,`kode`,`nama`,`alamat`,`telp`,`fax`,`import_flag`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,'TDY','Teddy','Jalan Raden Saleh Dalam no 85','08982874427','081360149004','',1,'2018-03-12 23:14:53',NULL,NULL,'ACTIVE');

UNLOCK TABLES;

/*Table structure for table `master_gudang` */

DROP TABLE IF EXISTS `master_gudang`;

CREATE TABLE `master_gudang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `master_gudang` */

LOCK TABLES `master_gudang` WRITE;

insert  into `master_gudang`(`id`,`kode`,`nama`,`import_flag`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,'GD1','Gudang 1','',1,'2018-03-12 21:55:58',1,'2018-03-12 21:56:06','ACTIVE'),(2,'GD2','Gudang 2','',1,'2018-03-19 20:52:37',NULL,NULL,'ACTIVE');

UNLOCK TABLES;

/*Table structure for table `master_mata_uang` */

DROP TABLE IF EXISTS `master_mata_uang`;

CREATE TABLE `master_mata_uang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `master_mata_uang` */

LOCK TABLES `master_mata_uang` WRITE;

insert  into `master_mata_uang`(`id`,`kode`,`nama`,`import_flag`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,'RP','Rupiah',NULL,0,'2018-01-29 20:33:26',NULL,NULL,'ACTIVE'),(2,'USD','US Dollar','',1,'2018-03-12 21:49:32',1,'2018-03-12 21:49:38','ACTIVE'),(3,'SGD','Singapore Dollar','',1,'2018-03-23 21:05:07',1,'2018-03-23 21:05:14','DELETED'),(4,'SGD','Sgd','',1,'2018-04-01 19:58:17',1,'2018-04-01 19:58:29','DELETED'),(5,'SGD','Singapore Dollar','',1,'2018-04-01 20:00:35',1,'2018-04-01 20:00:47','ACTIVE');

UNLOCK TABLES;

/*Table structure for table `master_negara` */

DROP TABLE IF EXISTS `master_negara`;

CREATE TABLE `master_negara` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `master_negara` */

LOCK TABLES `master_negara` WRITE;

insert  into `master_negara`(`id`,`kode`,`nama`,`import_flag`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,'IDN','Indonesia',NULL,0,'2018-01-29 20:35:24',NULL,NULL,'ACTIVE'),(2,'SG','Singapore','',1,'2018-03-12 21:52:59',NULL,NULL,'ACTIVE'),(3,'MY','Malaysia','',1,'2018-03-12 21:53:11',1,'2018-03-12 21:53:15','ACTIVE'),(4,'CN','China','',1,'2018-04-01 20:22:43',NULL,NULL,'ACTIVE');

UNLOCK TABLES;

/*Table structure for table `master_satuan` */

DROP TABLE IF EXISTS `master_satuan`;

CREATE TABLE `master_satuan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `master_satuan` */

LOCK TABLES `master_satuan` WRITE;

insert  into `master_satuan`(`id`,`kode`,`nama`,`import_flag`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,'KG','Kilogram',NULL,0,'2018-01-29 20:32:31',1,'2018-03-12 21:05:19','ACTIVE'),(2,'GR','Gram','',1,'2018-03-12 06:31:41',1,'2018-03-12 06:49:50','ACTIVE'),(3,'MG','Miligram','',1,'2018-03-12 06:39:02',1,'2018-03-12 07:00:33','ACTIVE'),(4,'XXX','XXXXX','',1,'2018-03-12 06:50:16',1,'2018-03-12 21:42:25','DELETED'),(5,'asdasdasda','asdasd','',1,'2018-03-12 07:00:46',1,'2018-03-12 21:42:15','DELETED'),(6,'TON','Ton','',1,'2018-03-23 21:04:27',1,'2018-04-01 19:39:09','ACTIVE'),(7,'SAK','Sak','',1,'2018-04-01 19:39:35',1,'2018-04-01 19:39:51','ACTIVE'),(8,'XXX','XXX','',1,'2018-09-01 00:00:00',1,NULL,'DELETED');

UNLOCK TABLES;

/*Table structure for table `master_stock_bahan_baku` */

DROP TABLE IF EXISTS `master_stock_bahan_baku`;

CREATE TABLE `master_stock_bahan_baku` (
  `id_bahan_baku` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `import_flag` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_bahan_baku`,`id_gudang`),
  KEY `id_bahan_baku` (`id_bahan_baku`),
  KEY `id_gudang` (`id_gudang`),
  KEY `bahan_baku_gudang` (`id_bahan_baku`,`id_gudang`),
  CONSTRAINT `mstockbbaku_bbaku` FOREIGN KEY (`id_bahan_baku`) REFERENCES `master_bahan_baku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mstockbbaku_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `master_stock_bahan_baku` */

LOCK TABLES `master_stock_bahan_baku` WRITE;

insert  into `master_stock_bahan_baku`(`id_bahan_baku`,`id_gudang`,`jumlah`,`import_flag`) values (1,1,812,''),(1,2,20,''),(2,1,15,''),(2,2,500,''),(3,1,21,''),(4,1,70,''),(5,2,1000,''),(6,1,6,''),(6,2,17,'');

UNLOCK TABLES;

/*Table structure for table `master_stock_barang_jadi` */

DROP TABLE IF EXISTS `master_stock_barang_jadi`;

CREATE TABLE `master_stock_barang_jadi` (
  `id_barang_jadi` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `import_flag` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_barang_jadi`,`id_gudang`),
  KEY `id_barang_jadi` (`id_barang_jadi`),
  KEY `id_gudang` (`id_gudang`),
  KEY `barang_jadi_gudang` (`id_barang_jadi`,`id_gudang`),
  CONSTRAINT `mstockbjadi_bjadi` FOREIGN KEY (`id_barang_jadi`) REFERENCES `master_barang_jadi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mstockbjadi_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `master_stock_barang_jadi` */

LOCK TABLES `master_stock_barang_jadi` WRITE;

insert  into `master_stock_barang_jadi`(`id_barang_jadi`,`id_gudang`,`jumlah`,`import_flag`) values (1,1,5003,''),(2,2,520,''),(3,1,5,'');

UNLOCK TABLES;

/*Table structure for table `master_stock_barang_waste` */

DROP TABLE IF EXISTS `master_stock_barang_waste`;

CREATE TABLE `master_stock_barang_waste` (
  `id_barang_waste` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `import_flag` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_barang_waste`,`id_gudang`),
  KEY `id_barang_waste` (`id_barang_waste`),
  KEY `id_gudang` (`id_gudang`),
  KEY `barang_waste_gudang` (`id_barang_waste`,`id_gudang`),
  CONSTRAINT `mstockbwaste_bwaste` FOREIGN KEY (`id_barang_waste`) REFERENCES `master_barang_waste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mstockbwaste_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `master_stock_barang_waste` */

LOCK TABLES `master_stock_barang_waste` WRITE;

insert  into `master_stock_barang_waste`(`id_barang_waste`,`id_gudang`,`jumlah`,`import_flag`) values (1,1,150,'');

UNLOCK TABLES;

/*Table structure for table `master_supplier` */

DROP TABLE IF EXISTS `master_supplier`;

CREATE TABLE `master_supplier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `master_supplier` */

LOCK TABLES `master_supplier` WRITE;

insert  into `master_supplier`(`id`,`kode`,`nama`,`alamat`,`telp`,`fax`,`import_flag`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,'KMTJ','Kilang Mie Tj Morawa','Jalan Tanjung Morawa Km..','061 1111111','061 2222',NULL,1,'2018-03-12 23:01:55',1,'2018-03-12 23:11:28','ACTIVE'),(2,'SP1','Supplier 1','Jalan Raden Saleh Dalam No 1','061 4532658','061 4553976','',1,'2018-03-12 23:08:51',1,'2018-03-12 23:11:34','ACTIVE');

UNLOCK TABLES;

/*Table structure for table `master_user` */

DROP TABLE IF EXISTS `master_user`;

CREATE TABLE `master_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(150) NOT NULL,
  `password` char(64) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `token_cookie` varchar(64) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `user_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `user_name` (`user_name`),
  KEY `ip` (`ip`),
  KEY `name_status` (`user_name`,`user_status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `master_user` */

LOCK TABLES `master_user` WRITE;

insert  into `master_user`(`id`,`user_name`,`password`,`ip`,`token_cookie`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`,`user_status`) values (1,'admin','25d55ad283aa400af464c76d713c07ad','::1','',0,'2018-03-09 22:50:01',1,'2018-04-11 21:10:10','ACTIVE','ACTIVE');

UNLOCK TABLES;

/*Table structure for table `trans_inventory_bahan_baku` */

DROP TABLE IF EXISTS `trans_inventory_bahan_baku`;

CREATE TABLE `trans_inventory_bahan_baku` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_bahan_baku` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `trans_date` datetime NOT NULL,
  `comment` text NOT NULL,
  `saldo_awal` int(10) NOT NULL DEFAULT '0',
  `jumlah_masuk` int(10) NOT NULL DEFAULT '0',
  `jumlah_keluar` int(10) NOT NULL DEFAULT '0',
  `saldo_akhir` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `id_bahan_baku` (`id_bahan_baku`),
  KEY `id_gudang` (`id_gudang`),
  KEY `id_user` (`id_user`),
  KEY `bahan_baku_gudang` (`id_bahan_baku`,`id_gudang`),
  CONSTRAINT `tinventorybbaku_bbaku` FOREIGN KEY (`id_bahan_baku`) REFERENCES `master_bahan_baku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tinventorybbaku_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `trans_inventory_bahan_baku` */

LOCK TABLES `trans_inventory_bahan_baku` WRITE;

insert  into `trans_inventory_bahan_baku`(`id`,`id_bahan_baku`,`id_gudang`,`id_user`,`trans_date`,`comment`,`saldo_awal`,`jumlah_masuk`,`jumlah_keluar`,`saldo_akhir`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,1,1,1,'2018-03-19 20:53:14','IBB20180300001',0,20,0,20,1,'2018-03-19 20:53:14',NULL,NULL,'ACTIVE'),(2,1,2,1,'2018-03-19 20:55:29','IBB20180300003',0,20,0,20,1,'2018-03-19 20:55:29',NULL,NULL,'ACTIVE'),(3,2,1,1,'2018-03-19 20:56:34','IBB20180300004',0,15,0,15,1,'2018-03-19 20:56:34',NULL,NULL,'ACTIVE'),(4,1,1,1,'2018-03-19 20:59:12','IBB20180300006',20,10,0,30,1,'2018-03-19 20:59:12',NULL,NULL,'ACTIVE'),(5,3,1,1,'2018-03-19 22:42:04','IBB20180300007',0,35,0,35,1,'2018-03-19 22:42:04',NULL,NULL,'ACTIVE'),(6,3,1,1,'2018-03-20 00:51:35','OBB20180300001',35,0,14,21,1,'2018-03-20 00:51:35',NULL,NULL,'ACTIVE'),(7,4,1,1,'2018-03-23 21:11:29','IBB20180300008',0,1000,0,1000,1,'2018-03-23 21:11:29',NULL,NULL,'ACTIVE'),(8,4,1,1,'2018-03-23 21:15:21','OBB20180300002',1000,0,500,500,1,'2018-03-23 21:15:21',NULL,NULL,'ACTIVE'),(9,1,1,1,'2018-03-24 02:06:09','IBB20180300009',30,200,0,230,1,'2018-03-24 02:06:09',NULL,NULL,'ACTIVE'),(10,4,1,1,'2018-03-25 23:00:41','IBB20180300010',500,700,0,1200,1,'2018-03-25 23:00:41',NULL,NULL,'ACTIVE'),(11,4,1,1,'2018-03-26 00:04:08','OBB20180300003',1200,0,1050,150,1,'2018-03-26 00:04:08',NULL,NULL,'ACTIVE'),(12,4,1,1,'2018-03-26 00:24:53','OBB20180300004',150,0,80,70,1,'2018-03-26 00:24:53',NULL,NULL,'ACTIVE'),(13,2,2,1,'2018-03-28 04:45:47','IBB20180300011',0,500,0,500,1,'2018-03-28 04:45:47',NULL,NULL,'ACTIVE'),(14,1,1,1,'2018-03-28 04:50:48','IBB20180300012',230,12,0,242,1,'2018-03-28 04:50:48',NULL,NULL,'ACTIVE'),(15,1,1,1,'2018-03-28 04:50:54','IBB20180300013',242,70,0,312,1,'2018-03-28 04:50:54',NULL,NULL,'ACTIVE'),(16,5,2,1,'2018-03-28 20:13:27','IBB20180300013',0,500,0,500,1,'2018-03-28 20:13:27',NULL,NULL,'ACTIVE'),(17,5,2,1,'2018-03-28 20:13:35','IBB20180300014',500,500,0,1000,1,'2018-03-28 20:13:35',NULL,NULL,'ACTIVE'),(18,1,1,1,'2018-04-28 20:22:26','IBB20180400002',312,500,0,812,1,'2018-04-28 20:22:26',NULL,NULL,'ACTIVE'),(19,6,2,1,'2018-09-23 22:39:11','IBB20180900001',0,17,0,17,1,'2018-09-23 22:39:11',NULL,NULL,'ACTIVE'),(20,6,1,1,'2018-09-23 22:39:49','IBB20180900002',0,6,0,6,1,'2018-09-23 22:39:49',NULL,NULL,'ACTIVE');

UNLOCK TABLES;

/*Table structure for table `trans_inventory_barang_jadi` */

DROP TABLE IF EXISTS `trans_inventory_barang_jadi`;

CREATE TABLE `trans_inventory_barang_jadi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_barang_jadi` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `trans_date` datetime NOT NULL,
  `comment` text NOT NULL,
  `saldo_awal` int(10) NOT NULL DEFAULT '0',
  `jumlah_masuk` int(10) NOT NULL DEFAULT '0',
  `jumlah_keluar` int(10) NOT NULL DEFAULT '0',
  `saldo_akhir` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `id_barang_jadi` (`id_barang_jadi`),
  KEY `id_gudang` (`id_gudang`),
  KEY `id_user` (`id_user`),
  KEY `barang_jadi_gudang` (`id_barang_jadi`,`id_gudang`),
  CONSTRAINT `tinventorybjadi_bjadi` FOREIGN KEY (`id_barang_jadi`) REFERENCES `master_barang_jadi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tinventorybjadi_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `trans_inventory_barang_jadi` */

LOCK TABLES `trans_inventory_barang_jadi` WRITE;

insert  into `trans_inventory_barang_jadi`(`id`,`id_barang_jadi`,`id_gudang`,`id_user`,`trans_date`,`comment`,`saldo_awal`,`jumlah_masuk`,`jumlah_keluar`,`saldo_akhir`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,1,1,1,'2018-03-23 04:26:25','IBJ20180300001',0,60,0,60,1,'2018-03-23 04:26:25',NULL,NULL,'ACTIVE'),(2,1,1,1,'2018-03-23 04:27:18','OBJ20180300001',60,0,15,45,1,'2018-03-23 04:27:18',NULL,NULL,'ACTIVE'),(3,2,2,1,'2018-03-23 21:19:28','IBJ20180300002',0,520,0,520,1,'2018-03-23 21:19:28',NULL,NULL,'ACTIVE'),(4,1,1,1,'2018-03-24 01:45:46','IBJ20180300003',45,1,0,46,1,'2018-03-24 01:45:46',NULL,NULL,'ACTIVE'),(5,1,1,1,'2018-03-24 01:58:38','IBJ20180300004',46,2,0,48,1,'2018-03-24 01:58:38',NULL,NULL,'ACTIVE'),(6,1,1,1,'2018-03-24 02:06:52','OBJ20180300002',48,0,10000,-9952,1,'2018-03-24 02:06:52',NULL,NULL,'ACTIVE'),(7,1,1,1,'2018-03-26 00:27:31','IBJ20180300005',-9952,10000,0,48,1,'2018-03-26 00:27:31',NULL,NULL,'ACTIVE'),(8,1,1,1,'2018-03-26 00:27:46','IBJ20180300005',48,10000,0,10048,1,'2018-03-26 00:27:46',NULL,NULL,'ACTIVE'),(9,1,1,1,'2018-03-26 00:29:29','OBJ20180300003',10048,0,5045,5003,1,'2018-03-26 00:29:29',NULL,NULL,'ACTIVE'),(10,3,1,1,'2018-03-26 00:54:17','IBJ20180300006',0,5,0,5,1,'2018-03-26 00:54:17',NULL,NULL,'ACTIVE');

UNLOCK TABLES;

/*Table structure for table `trans_inventory_barang_waste` */

DROP TABLE IF EXISTS `trans_inventory_barang_waste`;

CREATE TABLE `trans_inventory_barang_waste` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_barang_waste` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `trans_date` datetime NOT NULL,
  `comment` text NOT NULL,
  `saldo_awal` int(10) NOT NULL DEFAULT '0',
  `jumlah_masuk` int(10) NOT NULL DEFAULT '0',
  `jumlah_keluar` int(10) NOT NULL DEFAULT '0',
  `saldo_akhir` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `id_barang_waste` (`id_barang_waste`),
  KEY `id_gudang` (`id_gudang`),
  KEY `id_user` (`id_user`),
  KEY `barang_waste_gudang` (`id_barang_waste`,`id_gudang`),
  CONSTRAINT `tinventorybwaste_bwaste` FOREIGN KEY (`id_barang_waste`) REFERENCES `master_barang_waste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tinventorybwaste_gudang` FOREIGN KEY (`id_gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `trans_inventory_barang_waste` */

LOCK TABLES `trans_inventory_barang_waste` WRITE;

insert  into `trans_inventory_barang_waste`(`id`,`id_barang_waste`,`id_gudang`,`id_user`,`trans_date`,`comment`,`saldo_awal`,`jumlah_masuk`,`jumlah_keluar`,`saldo_akhir`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`) values (1,1,1,1,'2018-03-25 20:31:41','IBJ20180300001',0,30,0,30,1,'2018-03-25 20:31:41',NULL,NULL,'ACTIVE'),(2,1,1,1,'2018-03-25 20:32:27','IBJ20180300002',30,30,0,60,1,'2018-03-25 20:32:27',NULL,NULL,'ACTIVE'),(3,1,1,1,'2018-03-26 00:32:58','IBJ20180300004',60,90,0,150,1,'2018-03-26 00:32:58',NULL,NULL,'ACTIVE');

UNLOCK TABLES;

/*Table structure for table `trans_pemakaian_bahan_baku` */

DROP TABLE IF EXISTS `trans_pemakaian_bahan_baku`;

CREATE TABLE `trans_pemakaian_bahan_baku` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_bukti_pengeluaran` varchar(50) NOT NULL,
  `tanggal_bukti_pengeluaran` datetime NOT NULL,
  `bahan_baku` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `gudang` int(10) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED',
  PRIMARY KEY (`id`),
  KEY `no_transaksi` (`no_transaksi`),
  KEY `bahan_baku` (`bahan_baku`),
  KEY `gudang` (`gudang`),
  KEY `bahan_baku_gudang` (`bahan_baku`,`gudang`),
  CONSTRAINT `tpemakaianbbaku_bbaku` FOREIGN KEY (`bahan_baku`) REFERENCES `master_bahan_baku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpemakaianbbaku_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `trans_pemakaian_bahan_baku` */

LOCK TABLES `trans_pemakaian_bahan_baku` WRITE;

insert  into `trans_pemakaian_bahan_baku`(`id`,`no_transaksi`,`tanggal`,`no_bukti_pengeluaran`,`tanggal_bukti_pengeluaran`,`bahan_baku`,`jumlah`,`gudang`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`,`trans_status`) values (1,'OBB20180300001','2018-03-20 00:51:35','111357','2018-03-18 00:00:00',3,14,1,1,'2018-03-20 00:51:35',NULL,NULL,'ACTIVE','CONFIRMED'),(2,'OBB20180300002','2018-03-23 21:15:21','111357','2018-03-24 00:00:00',4,500,1,1,'2018-03-23 21:15:21',NULL,NULL,'ACTIVE','CONFIRMED'),(3,'OBB20180300003','2018-03-26 00:04:08','1236899','2018-03-15 00:00:00',4,1050,1,1,'2018-03-26 00:04:08',NULL,NULL,'ACTIVE','CONFIRMED'),(4,'OBB20180300004','2018-03-26 00:22:39','12368992','2018-03-30 00:00:00',4,80,1,1,'2018-03-26 00:22:39',1,'2018-03-26 00:24:53','ACTIVE','CONFIRMED');

UNLOCK TABLES;

/*Table structure for table `trans_pemasukan_bahan_baku` */

DROP TABLE IF EXISTS `trans_pemasukan_bahan_baku`;

CREATE TABLE `trans_pemasukan_bahan_baku` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `jenis_dokumen` varchar(100) NOT NULL,
  `no_dokumen_pabean` varchar(50) NOT NULL,
  `tanggal_dokumen_pabean` datetime NOT NULL,
  `no_seri_barang` varchar(50) NOT NULL,
  `no_bukti_penerimaan_barang` varchar(50) NOT NULL,
  `tanggal_bukti_penerimaan_barang` datetime NOT NULL,
  `bahan_baku` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `mata_uang` int(10) NOT NULL,
  `nilai_barang` varchar(50) NOT NULL,
  `gudang` int(10) NOT NULL,
  `negara_asal_barang` int(10) NOT NULL,
  `supplier` int(10) NOT NULL,
  `harga_satuan` varchar(50) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED',
  PRIMARY KEY (`id`),
  KEY `no_transaksi` (`no_transaksi`),
  KEY `bahan_baku` (`bahan_baku`),
  KEY `mata_uang` (`mata_uang`),
  KEY `gudang` (`gudang`),
  KEY `negara_asal_barang` (`negara_asal_barang`),
  KEY `bahan_baku_gudang` (`bahan_baku`,`gudang`),
  KEY `supplier` (`supplier`),
  CONSTRAINT `tpemasukanbbaku_bbaku` FOREIGN KEY (`bahan_baku`) REFERENCES `master_bahan_baku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpemasukanbbaku_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpemasukanbbaku_muang` FOREIGN KEY (`mata_uang`) REFERENCES `master_mata_uang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpemasukanbbaku_negara` FOREIGN KEY (`negara_asal_barang`) REFERENCES `master_negara` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpemasukanbbaku_supplier` FOREIGN KEY (`supplier`) REFERENCES `master_supplier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `trans_pemasukan_bahan_baku` */

LOCK TABLES `trans_pemasukan_bahan_baku` WRITE;

insert  into `trans_pemasukan_bahan_baku`(`id`,`no_transaksi`,`tanggal`,`jenis_dokumen`,`no_dokumen_pabean`,`tanggal_dokumen_pabean`,`no_seri_barang`,`no_bukti_penerimaan_barang`,`tanggal_bukti_penerimaan_barang`,`bahan_baku`,`jumlah`,`mata_uang`,`nilai_barang`,`gudang`,`negara_asal_barang`,`supplier`,`harga_satuan`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`,`trans_status`) values (1,'IBB20180300001','2018-03-19 20:53:14','PBN','0000001','0000-00-00 00:00:00','','123608','0000-00-00 00:00:00',1,20,1,'200000',1,1,1,'',1,'2018-03-19 20:53:14',NULL,NULL,'ACTIVE','CONFIRMED'),(3,'IBB20180300003','2018-03-19 20:55:29','PBN','0000002','0000-00-00 00:00:00','','999777','0000-00-00 00:00:00',1,20,1,'200000',2,1,1,'',1,'2018-03-19 20:55:29',NULL,NULL,'ACTIVE','CONFIRMED'),(4,'IBB20180300004','2018-03-19 20:56:34','PBN','0000004','0000-00-00 00:00:00','','123123123','0000-00-00 00:00:00',2,15,1,'200000',1,1,1,'',1,'2018-03-19 20:56:34',NULL,NULL,'ACTIVE','CONFIRMED'),(6,'IBB20180300006','2018-03-19 20:59:12','PBN','0000005','0000-00-00 00:00:00','','123608','0000-00-00 00:00:00',1,10,1,'200000',1,1,1,'',1,'2018-03-19 20:59:12',NULL,NULL,'ACTIVE','CONFIRMED'),(7,'IBB20180300007','2018-03-19 22:42:04','PBN','0000006','2018-03-19 00:00:00','0000111','999776','0000-00-00 00:00:00',3,35,1,'123456',1,1,1,'',1,'2018-03-19 22:42:04',NULL,NULL,'ACTIVE','CONFIRMED'),(8,'IBB20180300008','2018-03-23 21:11:29','1111111','0000007','2018-03-24 00:00:00','111111','123123123','0000-00-00 00:00:00',4,1000,2,'10000',1,1,1,'',1,'2018-03-23 21:11:29',NULL,NULL,'ACTIVE','CONFIRMED'),(9,'IBB20180300009','2018-03-24 02:06:09','1111111222','0000009','2018-03-07 00:00:00','2222','22222','0000-00-00 00:00:00',1,200,1,'123123123',1,1,1,'1000000',1,'2018-03-24 02:06:09',NULL,NULL,'ACTIVE','CONFIRMED'),(10,'IBB20180300010','2018-03-25 21:57:16','PBN','0000010','2018-03-27 00:00:00','111111','123123123','0000-00-00 00:00:00',4,700,1,'50000',1,1,1,'1000000',1,'2018-03-25 21:57:16',1,'2018-03-25 23:00:41','ACTIVE','CONFIRMED'),(11,'IBB20180300011','2018-03-28 04:45:27','PBN','0000011','2018-03-31 00:00:00','111123','1231231255','2018-03-30 00:00:00',2,500,1,'500000',2,1,1,'5000',1,'2018-03-28 04:45:27',1,'2018-03-28 04:45:47','ACTIVE','CONFIRMED'),(12,'IBB20180300012','2018-03-28 04:46:37','PBN','0000012','2018-03-23 00:00:00','213','12312312','2018-03-29 00:00:00',1,12,1,'1233',1,1,1,'6000',1,'2018-03-28 04:46:37',1,'2018-03-28 04:50:48','ACTIVE','CONFIRMED'),(13,'IBB20180300013','2018-03-28 04:50:39','PBN','0000013','2018-03-31 00:00:00','565465465465','123123123','2018-03-29 00:00:00',1,70,1,'11111111',1,1,1,'111111',1,'2018-03-28 04:50:39',1,'2018-03-28 04:50:54','ACTIVE','CONFIRMED'),(14,'IBB20180300013','2018-03-28 20:12:21','PBN','0000014','2018-03-30 00:00:00','12314','123124','2018-03-26 00:00:00',5,500,1,'5000',2,1,1,'123123',1,'2018-03-28 20:12:21',1,'2018-03-28 20:13:27','ACTIVE','CONFIRMED'),(15,'IBB20180300014','2018-03-28 20:12:35','PBN','0000014','2018-03-30 00:00:00','12314','123124','2018-03-26 00:00:00',5,500,1,'5000',2,1,1,'123123',1,'2018-03-28 20:12:35',1,'2018-03-28 20:13:35','ACTIVE','CONFIRMED'),(16,'IBB20180400002','2018-04-28 20:20:53','PBN','0000001','2018-04-12 00:00:00','4846894665','2111351651','2018-04-25 00:00:00',1,500,1,'200000',1,1,1,'6000',1,'2018-04-28 20:20:53',1,'2018-04-28 20:22:26','ACTIVE','CONFIRMED'),(17,'IBB20180500002','2018-05-28 20:23:47','PBN','0000009','2018-05-11 00:00:00','111123','2111351651','2018-05-30 00:00:00',1,60,1,'7000',1,1,1,'60',1,'2018-05-28 20:23:47',1,'2018-06-28 20:31:52','DELETED','PENDING'),(24,'IBB20180600001','2018-06-28 20:31:19','PBN','0000011','2018-06-20 00:00:00','4846894665','123608','2018-04-25 00:00:00',1,50,1,'10000',1,1,1,'10000',1,'2018-06-28 20:31:19',1,'2018-06-28 20:31:58','DELETED','PENDING'),(25,'IBB20180900001','2018-09-23 22:38:58','12312312','123123123','2018-09-25 00:00:00','123123','123123','2018-09-28 00:00:00',6,17,1,'12500',2,1,1,'12500',1,'2018-09-23 22:38:58',1,'2018-09-23 22:39:11','ACTIVE','CONFIRMED'),(26,'IBB20180900002','2018-09-23 22:39:40','12312312','123123123','2018-09-19 00:00:00','123123','123123','2018-09-18 00:00:00',6,6,1,'12500',1,1,1,'2',1,'2018-09-23 22:39:40',1,'2018-09-23 22:39:49','ACTIVE','CONFIRMED');

UNLOCK TABLES;

/*Table structure for table `trans_pemasukan_barang_jadi` */

DROP TABLE IF EXISTS `trans_pemasukan_barang_jadi`;

CREATE TABLE `trans_pemasukan_barang_jadi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_bukti_penerimaan` varchar(50) NOT NULL,
  `tanggal_bukti_penerimaan` datetime NOT NULL,
  `barang_jadi` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `gudang` int(10) NOT NULL,
  `no_pemakaian_bahan_baku` varchar(200) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED',
  PRIMARY KEY (`id`),
  KEY `no_transaksi` (`no_transaksi`),
  KEY `barang_jadi` (`barang_jadi`),
  KEY `gudang` (`gudang`),
  KEY `barang_jadi_gudang` (`barang_jadi`,`gudang`),
  CONSTRAINT `tpemasukanbjadi_bjadi` FOREIGN KEY (`barang_jadi`) REFERENCES `master_barang_jadi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpemasukanbjadi_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `trans_pemasukan_barang_jadi` */

LOCK TABLES `trans_pemasukan_barang_jadi` WRITE;

insert  into `trans_pemasukan_barang_jadi`(`id`,`no_transaksi`,`tanggal`,`no_bukti_penerimaan`,`tanggal_bukti_penerimaan`,`barang_jadi`,`jumlah`,`gudang`,`no_pemakaian_bahan_baku`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`,`trans_status`) values (1,'IBJ20180300001','2018-03-23 04:26:25','1112556','2018-03-07 00:00:00',1,60,1,'',1,'2018-03-23 04:26:25',NULL,NULL,'ACTIVE','CONFIRMED'),(2,'IBJ20180300002','2018-03-23 21:19:28','11125562','2018-03-24 00:00:00',2,520,2,'',1,'2018-03-23 21:19:28',NULL,NULL,'ACTIVE','CONFIRMED'),(3,'IBJ20180300003','2018-03-24 01:45:46','1112556','2018-03-14 00:00:00',1,1,1,'Array',1,'2018-03-24 01:45:46',NULL,NULL,'ACTIVE','CONFIRMED'),(4,'IBJ20180300004','2018-03-24 01:58:38','11125562','2018-03-12 00:00:00',1,2,1,'OBB20180300001,OBB20180300002',1,'2018-03-24 01:58:38',NULL,NULL,'ACTIVE','CONFIRMED'),(5,'IBJ20180300005','2018-03-26 00:26:24','1112556','2018-03-23 00:00:00',1,10000,1,'OBB20180300001,OBB20180300002',1,'2018-03-26 00:26:24',1,'2018-03-26 00:27:46','ACTIVE','CONFIRMED'),(6,'IBJ20180300006','2018-03-26 00:54:07','11125562','2018-03-28 00:00:00',3,5,1,'0',1,'2018-03-26 00:54:07',1,'2018-03-26 00:54:17','ACTIVE','CONFIRMED'),(7,'IBJ20180300007','2018-03-26 00:54:59','123123','2018-03-21 00:00:00',3,10,1,'OBB20180300003',1,'2018-03-26 00:54:59',NULL,NULL,'ACTIVE','PENDING');

UNLOCK TABLES;

/*Table structure for table `trans_pengeluaran_barang_jadi` */

DROP TABLE IF EXISTS `trans_pengeluaran_barang_jadi`;

CREATE TABLE `trans_pengeluaran_barang_jadi` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_feb` varchar(50) NOT NULL,
  `tanggal_feb` datetime NOT NULL,
  `no_bukti_pengeluaran` varchar(50) NOT NULL,
  `tanggal_bukti_pengeluaran` datetime NOT NULL,
  `pembeli` int(10) NOT NULL,
  `negara_tujuan` int(10) NOT NULL,
  `barang_jadi` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `mata_uang` int(10) NOT NULL,
  `nilai_barang` varchar(50) NOT NULL,
  `gudang` int(10) NOT NULL,
  `harga_satuan` varchar(50) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED',
  PRIMARY KEY (`id`),
  KEY `no_transaksi` (`no_transaksi`),
  KEY `barang_jadi` (`barang_jadi`),
  KEY `mata_uang` (`mata_uang`),
  KEY `gudang` (`gudang`),
  KEY `negara_tujuan` (`negara_tujuan`),
  KEY `pembeli` (`pembeli`),
  KEY `barang_jadi_gudang` (`barang_jadi`,`gudang`),
  CONSTRAINT `tpengeluaranbjadi_bjadi` FOREIGN KEY (`barang_jadi`) REFERENCES `master_barang_jadi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpengeluaranbjadi_customer` FOREIGN KEY (`pembeli`) REFERENCES `master_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpengeluaranbjadi_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpengeluaranbjadi_muang` FOREIGN KEY (`mata_uang`) REFERENCES `master_mata_uang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpengeluaranbjadi_negara` FOREIGN KEY (`negara_tujuan`) REFERENCES `master_negara` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `trans_pengeluaran_barang_jadi` */

LOCK TABLES `trans_pengeluaran_barang_jadi` WRITE;

insert  into `trans_pengeluaran_barang_jadi`(`id`,`no_transaksi`,`tanggal`,`no_feb`,`tanggal_feb`,`no_bukti_pengeluaran`,`tanggal_bukti_pengeluaran`,`pembeli`,`negara_tujuan`,`barang_jadi`,`jumlah`,`mata_uang`,`nilai_barang`,`gudang`,`harga_satuan`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`,`trans_status`) values (1,'OBJ20180300001','2018-03-23 04:27:18','G0056677','2018-03-15 00:00:00','111357','2018-03-27 00:00:00',1,1,1,15,1,'123456',1,'',1,'2018-03-23 04:27:18',NULL,NULL,'ACTIVE','CONFIRMED'),(2,'OBJ20180300002','2018-03-24 02:06:52','G00566773','2018-03-23 00:00:00','33333','2018-03-28 00:00:00',1,1,1,10000,1,'123456',1,'3000000',1,'2018-03-24 02:06:52',NULL,NULL,'ACTIVE','CONFIRMED'),(3,'OBJ20180300003','2018-03-26 00:29:13','G0056677','2018-03-29 00:00:00','33333','2018-03-29 00:00:00',1,1,1,5045,1,'50000',1,'1000000',1,'2018-03-26 00:29:13',1,'2018-03-26 00:29:29','ACTIVE','CONFIRMED');

UNLOCK TABLES;

/*Table structure for table `trans_penyelesaian_barang_waste` */

DROP TABLE IF EXISTS `trans_penyelesaian_barang_waste`;

CREATE TABLE `trans_penyelesaian_barang_waste` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_bc_24` varchar(50) NOT NULL,
  `tanggal_bc_24` datetime NOT NULL,
  `barang_waste` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `nilai` varchar(50) NOT NULL,
  `gudang` int(10) NOT NULL,
  `no_pengeluaran_barang_jadi` varchar(200) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'CONFIRMED',
  PRIMARY KEY (`id`),
  KEY `no_transaksi` (`no_transaksi`),
  KEY `barang_waste` (`barang_waste`),
  KEY `gudang` (`gudang`),
  KEY `barang_waste_gudang` (`barang_waste`,`gudang`),
  CONSTRAINT `tpenyelesaianbwaste_bwaste` FOREIGN KEY (`barang_waste`) REFERENCES `master_barang_waste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpenyelesaianbwaste_gudang` FOREIGN KEY (`gudang`) REFERENCES `master_gudang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `trans_penyelesaian_barang_waste` */

LOCK TABLES `trans_penyelesaian_barang_waste` WRITE;

insert  into `trans_penyelesaian_barang_waste`(`id`,`no_transaksi`,`tanggal`,`no_bc_24`,`tanggal_bc_24`,`barang_waste`,`jumlah`,`nilai`,`gudang`,`no_pengeluaran_barang_jadi`,`created_by`,`created_on`,`modified_by`,`modified_on`,`record_status`,`trans_status`) values (1,'IBJ20180300001','2018-03-25 20:31:41','113123','2018-03-21 00:00:00',1,30,'1000',1,'Array',1,'2018-03-25 20:31:41',NULL,NULL,'ACTIVE','CONFIRMED'),(2,'IBJ20180300002','2018-03-25 20:32:27','113123','2018-03-21 00:00:00',1,30,'1000',1,'OBJ20180300001,OBJ20180300002',1,'2018-03-25 20:32:27',NULL,NULL,'ACTIVE','CONFIRMED'),(4,'IBJ20180300004','2018-03-26 00:32:38','113123','2018-03-22 00:00:00',1,90,'123',1,'OBJ20180300001',1,'2018-03-26 00:32:38',1,'2018-03-26 00:32:58','ACTIVE','CONFIRMED'),(5,'IBJ20180300005','2018-03-26 01:10:30','113123','2018-03-14 00:00:00',1,25,'25000',1,'OBJ20180300002',1,'2018-03-26 01:10:30',1,'2018-03-26 01:12:09','DELETED','PENDING');

UNLOCK TABLES;

/* Procedure structure for procedure `UnsuspendPemasukanBBaku` */

/*!50003 DROP PROCEDURE IF EXISTS  `UnsuspendPemasukanBBaku` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `UnsuspendPemasukanBBaku`(IN no_transaksi VARCHAR(255))
BEGIN
	
	DECLARE bahan_baku INT;
	DECLARE gudang INT;
	DECLARE jumlah INT;
	SET bahan_baku = (SELECT bahan_baku FROM trans_pemasukan_bahan_baku WHERE no_transaksi = no_transaksi AND record_status = 'ACTIVE');
	SET gudang = (SELECT gudang FROM trans_pemasukan_bahan_baku WHERE no_transaksi = no_transaksi AND record_status = 'ACTIVE');
	SET jumlah = (SELECT jumlah FROM trans_pemasukan_bahan_baku WHERE no_transaksi = no_transaksi AND record_status = 'ACTIVE');
	SET @query1 = '
			UPDATE master_stock_bahan_baku
			SET jumlah = (jumlah - ?)
			WHERE bahan_baku = ? AND gudang = ?';
	
	SET @qty = jumlah;	
	SET @bb = bahan_baku;
	SET @gd = gudang;
	
	PREPARE stmt1 FROM @query1;
	EXECUTE stmt1 USING @qty, @bb, @gd;
	DEALLOCATE PREPARE stmt1;
 
	SET @query2 = '
			DELETE FROM trans_inventory_bahan_baku
			WHERE id_bahan_baku = ? AND id_gudang = ? AND comment = ?';
	
	SET @bb = bahan_baku;
	SET @gd = gudang;
	SET @trans = no_transaksi;	
	
	PREPARE stmt2 FROM @query2;
	EXECUTE stmt2 USING @bb, @gd, @trans;
	DEALLOCATE PREPARE stmt2;
 
	SET @query3 = '
			UPDATE trans_pemasukan_bahan_baku
			SET trans_status = ? 
			WHERE no_transaksi = ?';
	
	SET @status = 'PENDING';	
	SET @trans = no_transaksi;	
	
	PREPARE stmt3 FROM @query3;
	EXECUTE stmt3 USING @status, @trans;
	DEALLOCATE PREPARE stmt3;
	
	
	
 END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
