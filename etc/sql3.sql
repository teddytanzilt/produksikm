

ALTER TABLE `trans_inventory_bahan_baku` ADD COLUMN `trans_status` VARCHAR(20) NULL DEFAULT 'COMPLETED' AFTER `record_status`;

ALTER TABLE `trans_inventory_bahan_baku` ADD COLUMN `reff_id` INT(20) NULL AFTER `saldo_akhir`;


ALTER TABLE `trans_inventory_barang_jadi` ADD COLUMN `trans_status` VARCHAR(20) NULL DEFAULT 'COMPLETED' AFTER `record_status`;

ALTER TABLE `trans_inventory_barang_jadi` ADD COLUMN `reff_id` INT(20) NULL AFTER `saldo_akhir`;


ALTER TABLE `trans_inventory_barang_waste` ADD COLUMN `trans_status` VARCHAR(20) NULL DEFAULT 'COMPLETED' AFTER `record_status`;

ALTER TABLE `trans_inventory_barang_waste` ADD COLUMN `reff_id` INT(20) NULL AFTER `saldo_akhir`;


